<?php
/*header('Access-Control-Allow-Origin: https://www.sheepola.com, http://app.sheepola.com');  
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');*/
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
 
Route::group(['middleware' => 'auth.jwt', 'cors'], function () {
    Route::get('logout', 'ApiController@logout'); //http://localhost:8000/api/logout?token=jwt
 
    Route::get('user', 'ApiController@getAuthUser');
 
    Route::get('products', 'ProductController@index');
    Route::get('products/{id}', 'ProductController@show');
    Route::post('products', 'ProductController@store');
    Route::put('products/{id}', 'ProductController@update');
    Route::delete('products/{id}', 'ProductController@destroy');
});*/

/*
Route::get('/example/{api_token}/{name}', function (Request $request) {
    return response()->json([
        'name' => $request->name,
    ]);
})->middleware('api_token', 'cors');
*/


/*Route::group(['middleware' => 'permission'], function() {  

      Route::get('apitest', function (Request $request) {
        return env('API_KEY');
            
    });


});*/

/*
      Route::get('apitest', function (Request $request) {
        return env('API_KEY');
            
    });*/

Route::group(['middleware' => 'api_token'], function () {

   


    // Category
    Route::get('ShowCategoryRoot/{api_token}', 'CategoryController@showroot');
    Route::post('ShowCategorysParentId/{api_token}', 'CategoryController@ShowCategorysId')->name('ShowCategorysParentId');
    Route::post('ShowCategoryId/{api_token}', 'CategoryController@ShowCategorysId')->name('ShowCategoryId');
    Route::post('ShowCategoryId2/{api_token}', 'CategoryController@ShowCategorysid')->name('ShowCategoryId2');
    Route::post('ShowIdName/{api_token}', 'CategoryController@ShowCategorys')->name('ShowIdName');
    Route::post('ShowName/{api_token}', 'CategoryController@ShowCategorys');
    Route::post('ShowCategorysRandom1/{api_token}', 'CategoryController@ShowCategorysRandom')->name('ShowCategorysRandom1');
    Route::post('ShowCategorysRandom2/{api_token}', 'CategoryController@ShowCategorysRandom')->name('ShowCategorysRandom2');
    Route::post('ShowCategorysBottomBanner/{api_token}', 'CategoryController@ShowCategorysBottomBanner');
    Route::post('ShowCouponCateActive/{api_token}', 'CategoryController@ShowCouponCateActive');
    Route::post('ShowCategoryProducts/{api_token}', 'CategoryController@ShowCategoryProducts');
    Route::post('ShowMenuCategorysTopLeft/{api_token}', 'CategoryController@ShowMenuCategorysTopLeft');
    Route::post('ShowMenuCateLeft/{api_token}', 'CategoryController@ShowMenuCateLeft');
    Route::post('ShowCategorysApi/{api_token}', 'CategoryController@ShowCategorysApi');
    Route::post('Category/{api_token}',  'CategoryController@index');
    Route::post('Menucateleft/{api_token}',  'CategoryController@menucateleft');
    Route::post('Menucaterootleft/{api_token}',  'CategoryController@menucaterootleft');
    Route::post('ShowMenuMobile/{api_token}',  'CategoryController@MenuMobile');
    Route::post('ShowCategory/{api_token}',  'CategoryController@ShowCategory');
    Route::post('ShowBannerCategory/{api_token}',  'CategoryController@ShowBannerCategory');
    Route::post('ShowNameCategory/{api_token}',  'CategoryController@ShowNameCategory');
    Route::post('ShowCouponCategory/{api_token}',  'CategoryController@ShowCouponCategory');
    // Category

    Route::get('ShowProductNew/{api_token}', 'MainAppController@ShowProductNew');
    Route::get('ShowProductRecommend/{api_token}', 'MainAppController@ShowProductRecommend');

    // Product
    //Route::get('ShowProduct/{api_token}/{limit?}', 'ProductController@ShowProduct');
    Route::post('ShowProduct/{api_token}', 'ProductController@ShowProduct');
    Route::post('ShowProductId/{api_token}', 'ProductController@ShowProductId');
    Route::post('ResponeProduct/{api_token}', 'ProductController@ResponeProduct');
    Route::get('ShowProductFavourite/{api_token}',  'ProductController@ShowProductFavourite');
    Route::post('ProductFavouritePidUserId/{api_token}',  'ProductController@ShowProductFavouriteID')->name('ProductFavouritePidUserId');
    Route::post('ProductFavouriteUserId/{api_token}',  'ProductController@ShowProductFavouriteID')->name('ProductFavouriteUserId');
    Route::get('ShowProductReview/{api_token}',  'ProductController@ShowProductReview');
    Route::get('CountProductReview/{pid}/{api_token}',  'ProductController@CountProductReview');
    Route::get('ShowProductRankstar/{pid}/{uid}/{api_token}',  'ProductController@ShowProductRankstar');
    Route::get('ProductRating/{pid}/{api_token}',  'ProductController@ShowProductRating');
    Route::get('ProductStarRating/{pid}/{star}/{api_token}',  'ProductController@ShowProductStarRating');
    Route::get('ProductRating/{pid}/{api_token}',  'ProductController@CountProductRating');
    Route::get('ShowProductSku/{sku_id}/{sku_item}/{pid}/{api_token}',  'ProductController@ShowProductSku');
    Route::get('ShowCommentProgress/{pid}/{api_token}',  'ProductController@ShowCommentProgress');
    Route::get('ProductRandom1/{pid}/{limit}/{api_token}',  'ProductController@ShowProductRandom')->name('ProductRandom1');
    Route::get('ProductRandom2/{pid}/{api_token}',  'ProductController@ShowProductRandom')->name('ProductRandom2');
    Route::get('ProductRecommend/{limit}/{api_token}',  'ProductController@ShowProductRandom')->name('ProductRecommend');
    Route::get('ShowProductCategory/{pid}/{categoryid}/{categorysubid}/{api_token}',  'ProductController@ShowProductCategory');
    Route::get('ProductReviewMaxI/{pid}/{api_token}',  'ProductController@ShowProductReviewMaxId');
    Route::post('getProductPaginate/{api_token}',  'ProductController@getProductPaginate');
    //Route::get('showproductfavouriteapp/{userid}/{api_token}',  'ProductController@ShowProductFavouriteApp');
    Route::get('ShowMainSearch/{api_token}',  'ProductController@ShowMainSearch');
    Route::get('ShowProductColumn/{api_token}',  'ProductController@ShowProductColumn');
    Route::get('ShowProductTradeDate/{datestart}/{dateend}/{api_token}',  'ProductController@ShowProductTradeDate');
    Route::get('ShowProductTradeId/{pid}/{api_token}',  'ProductController@ShowProductTradeId');
    Route::get('ShowProductTrade/{api_token}',  'ProductController@ShowProductTrade');
    Route::get('SearchOrderByTaglableDESC/{keyword}/{api_token}',  'ProductController@ShowSearchProduct')->name('SearchOrderByTaglableDESC');
    Route::get('ShowCaseProduct/{api_token}/', 'ProductController@ShowCaseProduct');
    Route::post('ShowBestsellerProduct/{api_token}/', 'ProductController@ShowBestsellerProduct');
    Route::post('ShowTagsbyCategory/{api_token}/', 'ProductController@ShowTagsbyCategory');
    Route::post('ShowProductFlash/{api_token}/', 'ProductController@ShowProductFlash');
    Route::get('ShowProductFlashByget/{api_token}/{limit?}', 'ProductController@ShowProductFlash');
    Route::post('ShowProductBrand/{api_token}/', 'ProductController@ShowBanding');
    Route::post('ShowRandomProductLastView/{api_token}/', 'ProductController@ShowRandomProductLastView');
    Route::post('ShowPromotionProductInArray/{api_token}',  'ProductController@ShowPromotionProductInArray');
    Route::post('ShowDataProductFlashDealAll/{api_token}',  'ProductController@ShowDataProductFlashDealAll');
    Route::post('ShowCountOrderBuyPromotion/{api_token}', 'ProductController@ShowCountOrderBuyPromotion');
    Route::post('ShowProductClearance/{api_token}', 'ProductController@ShowProductClearance');
     Route::post('ShowProductReviewPidUserid/{api_token}',  'ProductController@ShowProductReviewPidUserid');
     Route::get('ShowProductReviewInvUserid/{inv}/{user_id}/{api_token}',  'ProductController@ShowProductReviewInvUserid');
    Route::post('ShowProductCountReviewByUseridInv/{api_token}',  'ProductController@ShowProductCountReviewByUseridInv');
     // Product

    //User
    Route::get('Showuser/{api_token}',  'UserController@ShowUser');
    Route::get('ShowUserId/{userid}/{api_token}',  'UserController@ShowUserId')->name('WhereId');
    Route::get('SelectIdGenderWhereId/{userid}/{api_token}',  'UserController@ShowUserId')->name('SelectIdGenderWhereId');
    Route::get('ShowUserFacbookId/{responseid}/{api_token}',  'UserController@ShowUserFacbookId');
    Route::get('ShowUserLineId/{responseid}/{api_token}',  'UserController@ShowUserLineId');
    Route::get('ShowUserRecommend/{userref}/{api_token}',  'UserController@ShowUserRecommend');
    Route::get('ShowUserRecommendFirstBuy/{value1}/{value2}/{api_token}',  'UserController@ShowUserRecommendFirstBuy');
    Route::get('CheckForgotEmail/{value}/{api_token}',  'UserController@CheckForgotEmail')->name('CheckForgotEmail');
    Route::get('CheckEmail/{value}/{password}/{api_token}',  'UserController@CheckEmail')->name('CheckEmail');
    ///User

    // Order Product
     Route::get('ShowOrderProduct/{api_token}',  'OrderProductController@ShowOrderProduct')->name('ShowOrderProductAll');
     Route::get('ShowOrderProductNotEq567/{api_token}',  'OrderProductController@ShowOrderProduct')->name('ShowOrderProductNotEq567');
     Route::get('ShowOrderProductId1/{id}/{api_token}',  'OrderProductController@ShowOrderProductId')->name('ShowOrderProductId1');
     Route::get('ShowOrderProductId2/{id}/{value}/{api_token}',  'OrderProductController@ShowOrderProductId2')->name('ShowOrderProductId2');
    Route::get('ShowOrderProductId3/{id}/{api_token}',  'OrderProductController@ShowOrderProductId')->name('ShowOrderProductId3');
    Route::get('ShowPendingCash/{userid}/{lastdate}/{api_token}',  'OrderProductController@ShowPendingCash');
    Route::get('ShowPendingCashBack/{userid}/{lastdate}/{api_token}',  'OrderProductController@ShowPendingCashBack');
    Route::get('ShowCountOrderProduct/{userid}/{api_token}',  'OrderProductController@ShowCountOrderProduct');
   
    Route::get('ShowStatusLoadOrder/{userid}/{status}/{api_token}/', 'OrderProductController@ShowStatusLoadOrder');

    Route::get('ShowOrderProductInv/{id}/{value}/{api_token}',  'OrderProductController@ShowOrderProductInv')->name('ShowOrderProductInv');
    Route::get('ShowOrderProductInvUser/{id}/{value}/{api_token}',  'OrderProductController@ShowOrderProductInv')->name('ShowOrderProductInvUser');
     
    // Order Product

     // Promotion
     Route::get('PromotionAll/{api_token}',  'PromotionController@ShowPromotion')->name('PromotionAll');
     Route::get('ShowPromotionNotification1/{api_token}',  'PromotionController@ShowPromotionNotification')->name('ShowPromotionNotification1');
     Route::get('ShowPromotionNotification2/{api_token}',  'PromotionController@ShowPromotionNotification')->name('ShowPromotionNotification2');
     Route::get('ShowPromotionNotification3/{api_token}',  'PromotionController@ShowPromotionNotification')->name('ShowPromotionNotification3');
     Route::get('ShowPromotionNotification4/{api_token}',  'PromotionController@ShowPromotionNotification')->name('ShowPromotionNotification4');
     Route::get('ShowPromotionNotification5/{api_token}',  'PromotionController@ShowPromotionNotification')->name('ShowPromotionNotification5');
     Route::get('ShowPromotionImageFull/{api_token}',  'PromotionController@ShowPromotionImageFull')->name('ShowPromotionImageFull');
     Route::post('ShowPromotionEnd/{api_token}',  'PromotionController@ShowPromotionEnd');
     Route::get('ShowPromotionCreateEndCat/{dateCreate}/{dateEnd}/{PromotionCat}/{api_token}',  'PromotionController@ShowPromotionCreateEndCat');
     Route::get('ShowPromotionCodeId/{dateAdd}/{dateEnd}/{id}/{api_token}',  'PromotionController@ShowPromotionCodeId');
     Route::get('ShowImgProductTypeLinkApp/{api_token}',  'PromotionController@ShowImgProductTypeLinkApp');
     Route::get('CountOrderBuyPromotion/{promoid}/{pid}/{qtysale}/{api_token}',  'PromotionController@CountOrderBuyPromotion');
     Route::get('ShowCheckPromotion/{promoid}/{id}/{api_token}',  'PromotionController@ShowCheckPromotion');
     Route::get('ShowPromotionFlashdeal/{api_token}',  'PromotionController@ShowPromotionFlashdeal');
     Route::get('ShowPromotionTemplate/{api_token}',  'PromotionController@ShowPromotionTemplate');
     Route::get('ShowPromotionCodeDate/{datenow}/{api_token}',  'PromotionController@ShowPromotionCodeDate')->name('GetIdName');
     Route::post('ShowCoupon/{api_token}',  'PromotionController@ShowCoupon');
     Route::post('ShowPromotionSlide/{api_token}',  'PromotionController@ShowPromotionSlide');
     Route::get('ShowPromotionCateStatus/{status}/{api_token}',  'PromotionController@ShowPromotionCateStatus');
     Route::get('ShowPromotionFlashEnd/{api_token}/{limit?}', 'PromotionController@ShowPromotionFlashEnd');
    Route::get('ShowPromotionFlashStartEndByOne/{api_token}', 'PromotionController@ShowPromotionFlashStartEndByOne');
     
      // Promotion


     // Notification
     Route::get('ShowNotification/{api_token}',  'NotificationController@ShowNotification');
     Route::get('ShowNotificationId1/{promotionid}/{userid}/{api_token}',  'NotificationController@ShowNotificationId1');
     Route::get('ShowNotificationId2/{promotionid}/{api_token}',  'NotificationController@ShowNotificationId2');
     Route::get('ShowNoticeNewUserId1/{userid}/{api_token}',  'NotificationController@ShowNoticeNewUserId')->name('ShowNoticeNewUserId1');
     Route::get('ShowNoticeNewUserId2/{userid}/{api_token}',  'NotificationController@ShowNoticeNewUserId')->name('ShowNoticeNewUserId2');
     Route::get('ShowNoticeNewUserId3/{userid}/{api_token}',  'NotificationController@ShowNoticeNewUserId')->name('ShowNoticeNewUserId3');
     Route::get('ShowNoticeNewUserId4/{userid}/{api_token}',  'NotificationController@ShowNoticeNewUserId')->name('ShowNoticeNewUserId4');
     Route::get('ShowtNoticeNew/{api_token}',  'NotificationController@ShowtNoticeNew');
     Route::get('ShowNoticeNewUseridId/{userid}/{id}/{api_token}',  'NotificationController@ShowNoticeNewUseridId');
     Route::get('ShowReadNotice/{id}/{userid}/{api_token}',  'NotificationController@ShowReadNotice');
     Route::get('ShowNotificationWithPromotion/{id}/{api_token}',  'NotificationController@ShowNotificationWithPromotion')->name('UseWithNotification');
     // Notification

     // Order Detail
     Route::get('ShowOrderDetail/{api_token}',  'OrderDetailController@ShowOrderDetail');
     Route::post('ShowCountOrderDetailUseridInv/{api_token}',  'OrderDetailController@ShowCountOrderDetailUseridInv');
     Route::get('ShowOrderDetailInv/{userid}/{inv}/{api_token}',  'OrderDetailController@ShowOrderDetailInv');
     Route::post('ShowOrderDetailJoinOrderProduct/{api_token}',  'OrderDetailController@ShowOrderDetailJoinOrderProduct');
     Route::post('ShowOrderDetailJoinProductReview/{api_token}',  'OrderDetailController@ShowOrderDetailJoinProductReview');
     // Order Detail

     // Address
     Route::get('Showprovince/{api_token}',  'AddressController@ShowProvince');
     Route::get('ShowProvinceNameThai/{value}/{api_token}',  'AddressController@ShowProvinceId')->name('ShowProvinceNameThai');
     Route::get('ShowProvinceNameEnglish/{value}/{api_token}',  'AddressController@ShowProvinceId')->name('ShowProvinceNameEnglish');
     Route::get('ShowAmphurNameThai/{value}/{api_token}',  'AddressController@ShowAmphurId')->name('ShowAmphurNameThai');
     Route::get('ShowAmphurNameEnglish/{value}/{api_token}',  'AddressController@ShowAmphurId')->name('ShowAmphurNameEnglish');
     Route::get('ShowAmphurWhereProvinceid/{value}/{api_token}',  'AddressController@ShowAmphurId')->name('ShowAmphurWhereProvinceid');
     Route::get('ShowTambonWhereAmphur/{value}/{api_token}',  'AddressController@ShowTambonId')->name('ShowTambonWhereAmphur');
     Route::get('ShowTambonWhereId/{value}/{api_token}',  'AddressController@ShowTambonId')->name('ShowTambonWhereId');
     Route::get('ShowTambonLikeNameThai/{value}/{api_token}',  'AddressController@ShowTambonId')->name('ShowTambonLikeNameThai');
     // Address

     // contact user
     Route::get('ShowContactUserId/{id}/{api_token}',  'ContactUserController@ShowContactUserId')->name('ShowContactUserId');
     Route::get('ShowContactId/{id}/{api_token}',  'ContactUserController@ShowContactUserId')->name('ShowContactId');
     Route::get('ShowContactUserId2/{userid}/{id}/{api_token}',  'ContactUserController@ShowContactUserId2');
     // contact user



     // Banner
     Route::get('ShowBannerLink/{api_token}',  'BannerController@ShowBannerLink');
     Route::get('ShowBannerAdsTop/{api_token}',  'BannerController@ShowBannerAdsTop');
     Route::post('ShowBannerAds/{api_token}',  'BannerController@ShowBannerAds');
     Route::get('ShowBannerAdsId1/{id}/{api_token}',  'BannerController@ShowBannerAdsId')->name('ShowBannerAdsId1');
     Route::get('ShowBannerAdsId2/{id}/{api_token}',  'BannerController@ShowBannerAdsId')->name('ShowBannerAdsId2');
     Route::get('ShowBannerAdsId3/{id}/{api_token}',  'BannerController@ShowBannerAdsId')->name('ShowBannerAdsId3');
     Route::get('ShowBannerPosition/{id}/{api_token}',  'BannerController@ShowBannerAdsId')->name('ShowBannerPosition');
     Route::post('ShowActivitySlide/{api_token}',  'BannerController@ShowActivitySlide');
     Route::post('ShowBannerTopSlide/{api_token}',  'BannerController@ShowBannerTopSlide');
     Route::post('ShowConditionSlide/{api_token}',  'BannerController@ShowConditionSlide');
     Route::get('ShowBannerAdsPosition/{id}/{api_token}',  'BannerController@ShowBannerAdsId')->name('ShowBannerAdsPosition');
     Route::post('ShowTopPromotion/{api_token}',  'BannerController@ShowTopPromotion');
     
     // Banner

     // MyCoupon
    Route::get('ShowMyCoupon/{api_token}',  'MyCouponController@ShowMyCoupon');
    Route::get('ShowCountMyCouponId/{userid}/{api_token}',  'MyCouponController@ShowCountMyCouponId');
    Route::get('ShowMyCouponId/{userid}/{date_start}/{date_end}/{api_token}',  'MyCouponController@ShowMyCouponId');
    Route::get('ShowMyCouponId2/{userid}/{codeid}/{api_token}',  'MyCouponController@ShowMyCouponId2')->name('MyCouponId2Where1');
    Route::get('ShowMyCouponId3/{userid}/{codeid}/{api_token}',  'MyCouponController@ShowMyCouponId2')->name('MyCouponId2Where2');

     // MyCoupon


      // Transactions
     Route::get('ShowTransactionsId1/{id}/{api_token}',  'TransactionsController@ShowTransactionsId')->name('ShowTransactionsId1');
     Route::get('ShowTransactionsId2/{id}/{api_token}',  'TransactionsController@ShowTransactionsId')->name('ShowTransactionsId2');
     Route::get('ShowTransactionsId3/{id}/{api_token}',  'TransactionsController@ShowTransactionsId')->name('ShowTransactionsId3');
     Route::get('ShowWithDrawalHistory/{userid}/{lastdate}/{api_token}',  'TransactionsController@ShowWithDrawalHistory');
     Route::get('ShowTotalHistoryUnRead/{userid}/{api_token}',  'TransactionsController@ShowTotalHistoryUnRead');
    Route::get('ShowPaymentGatewayId/{id}/{api_token}',  'TransactionsController@ShowPaymentGatewayId');
     // Transactions


      // setting
     Route::get('ShowSettingApp1/{api_token}',  'SettingController@ShowSettingApp')->name('getSettingApp1');
     Route::get('ShowSettingApp2/{api_token}',  'SettingController@ShowSettingApp')->name('getSettingApp2');
     Route::get('ShowConfirmAppDownload/{api_token}',  'SettingController@ShowConfirmAppDownload')->name('ShowConfirmAppDownload');
     Route::get('ShowConfirmAppDownloadId/{id}/{api_token}',  'SettingController@ShowConfirmAppDownloadId')->name('ShowConfirmAppDownloadId');
     Route::get('ShowAppVersion/{api_token}',  'SettingController@ShowAppVersion');
     Route::get('ShowRegisterBoxEvent/{userid}/{api_token}',  'SettingController@ShowRegisterBoxEvent');
     // setting

     // AdminBank
     Route::get('ShowAdminBank/{api_token}',  'AdminBankController@ShowAdminBank');
     Route::get('ShowAdminBankId/{id}/{api_token}',  'AdminBankController@ShowAdminBankId');
     // AdminBank

     // Content
     Route::get('ShowContentId1/{id}/{api_token}',  'ContentController@ShowContentId')->name('getContentId1');
     Route::get('ShowContentId2/{id}/{api_token}',  'ContentController@ShowContentId')->name('getContentId2');
     Route::get('ShowContentCateId/{id}/{id2}/{api_token}',  'ContentController@ShowContentCateId');
     // Content

      // Business
     Route::get('ShowBusinessBook/{api_token}',  'BusinessController@ShowBusinessBook');
     Route::get('ShowBusinessBookId/{id}/{api_token}',  'BusinessController@ShowBusinessBookId')->name('ShowBusinessBookId');
     Route::get('SelectStatusWhereUserId/{id}/{api_token}',  'BusinessController@ShowBusinessBookId')->name('SelectStatusWhereUserId');
     // Business

       // Mystyle
     Route::post('ShowMyStyle/{api_token}',  'MyStyleController@ShowMyStyle');
     Route::post('ShowMyStyle2/{api_token}',  'MyStyleController@ShowMyStyle2');
     Route::get('ShowMyStyleId/{id}/{api_token}',  'MyStyleController@ShowMyStyleId');
     Route::get('ShowMyStyleRandom/{api_token}',  'MyStyleController@ShowMyStyleRandom');
     Route::get('ShowMyStyleTotal/{api_token}',  'MyStyleController@ShowMyStyleTotal');
     Route::post('ShowMystyleProducts/{api_token}',  'MyStyleController@ShowMystyleProducts');
     Route::post('ShowProductMystyleById/{api_token}',  'MyStyleController@ShowProductMystyleById');
     Route::post('ShowProductMystyleByGroup/{api_token}',  'MyStyleController@ShowProductMystyleByGroup');
      Route::post('ShowCountProductsByMystyle/{api_token}',  'MyStyleController@ShowCountProductsByMystyle');

     

     
     // Mystyle

        // Chat
     Route::get('ShowChatRoom/{api_token}',  'ChatController@ShowChatRoom');
     Route::get('ShowChatDetail/{api_token}',  'ChatController@ShowChatDetail');
     Route::get('ShowChatRoomId/{id}/{api_token}',  'ChatController@ShowChatRoomId');
     Route::get('ShowChatDetailId1/{id}/{api_token}',  'ChatController@ShowChatDetailId')->name('ShowChatDetailId1');
     Route::get('ShowChatDetailId2/{id}/{api_token}',  'ChatController@ShowChatDetailId')->name('ShowChatDetailId2');
     // Chat

});