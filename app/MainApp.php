<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class MainApp extends Model
{
    public function getProductNew($limit=''){

        $datas =   DB::select(DB::raw('SELECT  name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent FROM product WHERE show_hidden = \'1\' GROUP BY sku_id,sku_item ORDER BY id DESC LIMIT '.$limit));

        return $datas;
    }

    public function getProductRecommend($limit=''){

        $datas =   DB::select(DB::raw('SELECT name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent,details FROM `product` WHERE `show_hidden` = \'1\' GROUP BY sku_id,sku_item ORDER BY RAND() LIMIT '.$limit));

        return $datas;
    }

    public function getCategorysMainApp($limit='',$parentid){

        $datas = DB::select(DB::raw('SELECT id,parentid,name FROM `categorys` WHERE `show_hidden` = \'1\' AND `parentid` = \''.$parentid.'\' ORDER BY RAND() LIMIT '.$limit));
        return  $datas;

    }

    public function getProductCategoryMainApp($limit='',$categoryid=''){

        $datas =   DB::select(DB::raw('SELECT name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent,details FROM `product` WHERE `show_hidden` = \'1\' AND `categoryid` = \''.$categoryid.'\' GROUP BY sku_id,sku_item ORDER BY RAND() LIMIT '.$limit));

        return $datas;
    }

    public function getBannerAdsCategory($limit='',$categoryid){

        $datas =   DB::select(DB::raw('SELECT url_ios,link,banner_img FROM `banner_ads` WHERE position IN (\'32\',\'33\',\'34\') AND `show_hidden` = \'1\' AND `category` = \''.$categoryid.'\' ORDER BY RAND() LIMIT '.$limit));

        return $datas;
    }

    public function getSettingApp(){

        $datas =   DB::select(DB::raw('SELECT app_index_category_product,app_index_flashdeals,app_index_mystyle,app_index_coupon FROM `setting_app` WHERE `id` = \'1\''));
        return $datas;
    }
}
