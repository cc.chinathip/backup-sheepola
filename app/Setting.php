<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Setting extends Model
{
    //

    public function getSettingApp(){

    	$RouteName = Route::currentRouteName();

    		if($RouteName == 'getSettingApp1'){

    			$datas =  DB::table('setting_app')->where('id','1')->get();

    		}else if($RouteName == 'getSettingApp2'){

    			$datas =  DB::table('setting_app')->select('flashdealstimeout')->where('id','1')->get();
    		}

    	
		return $datas;

	}

	public function getConfirmAppDownload(){

    	$datas =  DB::table('confirm_app_download')->get();

    	
		return $datas;

	}

	public function getConfirmAppDownloadId($id){

    	$datas =  DB::table('confirm_app_download')->where('user_id',$id)->get();

    	
		return $datas;

	}

	public function getAppVersion(){

    	$datas =  DB::table('appversion')->limit('1')->orderBy('id', 'desc')->get();

    	
		return $datas;

	}

    public function getRegisterBoxEvent($userid=''){

          

            $setting_app = DB::select(DB::raw('SELECT eventboxid,total_buy_register_ref,date_end_register_ref,date_open_register_ref,open_register_ref FROM `setting_app` WHERE `id` = \'1\' '));

            foreach ($setting_app as $setting_app_value_key => $setting_app_value_value) {
                # code...
                        $getregisterboxevent = DB::select(DB::raw('SELECT id FROM `transactions` WHERE `user_id` = \''.$userid.'\' AND `eventbox` = \''.$setting_app_value_value->eventboxid.'\' '));

                    if($setting_app_value_value->open_register_ref == 1){

                        if($setting_app_value_value->date_open_register_ref <= date('Y-m-d') && $setting_app_value_value->date_end_register_ref >= date('Y-m-d')){

                            $boxeventregistershare = array(
                                'text_bar' => $getregisterboxevent->total.'/'.$setting_app_value_value->total_buy_register_ref,
                                'text_percen' => (100 / $setting_app_value_value->total_buy_register_ref) * $getregisterboxevent->total
                            );

                            echo json_encode($boxeventregistershare);

                        }else{
                            echo "false";
                        }

                    }else{
                        echo "false";
                    }

            }  
        
    }

	

	
}
