<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route


class Content extends Model
{
    //

    public function getContent(){

    	$datas =  DB::table('content')->get();

		return $datas;

	}

	public function getContentCate(){

    	$datas =  DB::table('content_cate')->get();

		return $datas;

	}

	public function getContentId($id){


		$RouteName = Route::currentRouteName();

			if($RouteName == 'getContentId1'){

    			$datas =  DB::table('content')->where('id',$id)->get();

    		}else if($RouteName == 'getContentId2'){

    			$datas =  DB::table('content')->where('cate_content',$id)->get();
    		}

    	

		return $datas;

	}

	public function getContentCateId($id,$id2){


		$RouteName = Route::currentRouteName();

		$datas =  DB::table('content_cate')->where('id','<>' ,$id)->where('id','<>' ,$id2)->get();

    	
		return $datas;

	}




	
}
