<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Product extends Model
{
   
   public function priceproduct_arr($product='',$detail=''){

        $prict_product = array();

        $date = date('Y-m-d H:i:s');

        $activity =  DB::table('product_activity_date')->where('status','1')->where('date_start','<=', $date)->where('date_end','>=', $date)->orderBy('id','DESC')->first();

        if(!empty($activity)) {
             $product_price =  DB::table('product_activity_item')->select('price','cash_back')->where('status','1')->where('product_id',$product->id)->where('product_activity_date_id',$activity->id)->first();


            $count =  DB::table('product_activity_item')->select(DB::raw('COUNT(id) as total'))->where('status','1')->where('product_id',$product->id)->where('product_activity_date_id',$activity->id)->first();
            if ($count->total > 0) {
                $prict_product[0] = $product_price->price;
                $prict_product[5] = $product->snat_price;
                $prict_product[1] = '<ins><span>฿'.number_format($product_price->price).'</span></ins><del><span>฿'.number_format($product->snat_price).'</span></del>';
                $prict_product[2] = $product_price->price * $product_price->cash_back / 100;
            } else {
                if(!isset($product->snat_price) || $product->snat_price == '0'){
                    $prict_product[0] = $product->nat_price;
                    $prict_product[5] = $product->snat_price;
                    $prict_product[1] = '<ins><span>฿'.number_format($product->nat_price).'</span></ins>';
                    $prict_product[2] = $product->nat_price * $product->cashback_percent / 100;
                }else{
                    if(!isset($product->nat_price)  || $product->nat_price == '0'){
                        $prict_product[0] = $product->snat_price;
                        $prict_product[5] = $product->nat_price;
                        $prict_product[1] = '<ins><span>฿'.number_format($product->snat_price).'</span></ins>';
                        $prict_product[2] = $product->snat_price * $product->cashback_percent / 100;
                    }else{
                        if(isset($product->nat_price) == isset($product->snat_price)){
                            $prict_product[0] = $product->snat_price;
                            $prict_product[5] = $product->nat_price;
                            $prict_product[1] = '<ins><span>฿'.number_format($product->snat_price).'</span></ins>';
                            $prict_product[2] = $product->snat_price * $product->cashback_percent / 100;
                        }else{
                            $prict_product[0] = $product->nat_price;
                            $prict_product[5] = $product->snat_price;
                            $prict_product[1] = "<del><span>฿".number_format($product->snat_price).'</span></del> <ins><span>฿'.number_format($product->nat_price).'</span></ins>';
                            if(calc2($product->nat_price,$product->snat_price) != 0){
                                if($detail == ''){
                                    $prict_product[3] = calc2($product->nat_price,$product->snat_price);
                                }else{
                                    $prict_product[3] = str_replace('-','',calc2($product->nat_price,$product->snat_price));
                                    $prict_product[4] = calc22($product->nat_price,$product->snat_price);
                                }
                            }
                            $prict_product[2] = $product->nat_price * $product->cashback_percent / 100;
                        }
                    }
                }
            }
        } else {
            if($product->snat_price == '' || $product->snat_price == '0'){
                $prict_product[0] = $product->nat_price;
                $prict_product[5] = $product->snat_price;
                $prict_product[1] = '<ins><span>฿'.number_format($product->nat_price).'</span></ins>';
                $prict_product[2] = $product->nat_price * $product->cashback_percent / 100;
            }else{
                if($product->nat_price == '' || $product->nat_price == '0'){
                    $prict_product[0] = $product->snat_price;
                    $prict_product[5] = $product->nat_price;
                    $prict_product[1] = '<ins><span>฿'.number_format($product->snat_price).'</span></ins>';
                    $prict_product[2] = $product->snat_price * $product->cashback_percent / 100;
                }else{

                    if($product->nat_price == $product->snat_price){

                        $prict_product[0] = $product->snat_price;
                        $prict_product[5] = $product->nat_price;
                        $prict_product[1] = '<ins><span>฿'.number_format($product->snat_price).'</span></ins>';
                        $prict_product[2] = $product->snat_price * $product->cashback_percent / 100;

                    }else{

                        $prict_product[0] = $product->nat_price;
                        $prict_product[5] = $product->snat_price;

                        $prict_product[1] = "<del><span>฿".number_format($product->snat_price).'</span></del> <ins><span>฿'.number_format($product->nat_price).'</span></ins>';

                        if(calc2($product->nat_price,$product->snat_price) != 0){
                            if($detail == ''){
                                $prict_product[3] = calc2($product->nat_price,$product->snat_price);
                            }else{
                                $prict_product[3] = str_replace('-','',calc2($product->nat_price,$product->snat_price));
                                $prict_product[4] = calc22($product->nat_price,$product->snat_price);
                            }
                        }

                        $prict_product[2] = $product->nat_price * $product->cashback_percent / 100;

                    }
                }
            }
        }
        return $prict_product;
    }

    public function getProductFlashByPromtionId($PromtionId,$limit){

        $datas =  DB::table('product')->where('promotion_id',$PromtionId)->where('show_hidden','1')->inRandomOrder()->limit($limit)->get();
        return $datas;
    }

    public function getProductFlashByPromtionId2($PromtionId){

        $datas =  DB::table('product')->where('promotion_id',$PromtionId)->where('show_hidden','1')->get();
        return $datas;
    }


  
    public function getRandomProductLastView() {

            $productlastview =  DB::table('product')->select('id','name_product','categoryid','category_subid','name_product_sale','nat_price','snat_price','sku_id','sku_item')
                     ->where('show_hidden','1')->limit('1')->get();

            return $productlastview;
    }

    public function getProductRecommendedsSelectId($id,$sku_id,$sku_item,$where_category) {

            $productrecommendeds =   DB::select(DB::raw('SELECT  id  FROM product WHERE `show_hidden` = \'1\' AND `id` != \''.$id.'\' AND `sku_id` <> \''.$sku_id.'\' AND `sku_item` <> \''.$sku_item.'\''.$where_category.' GROUP BY  sku_id,sku_item  LIMIT 100'));
                     
            return $productrecommendeds;
    }

    public function getProductRecommendedsUnique($productrecommended_unique_i) {

    $productrecommendeds =   DB::select(DB::raw('SELECT  id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand FROM product WHERE `show_hidden` = \'1\' AND `id` = \''.$productrecommended_unique_i.'\' LIMIT 1'));

        return $productrecommendeds;
    }

    public function getProductRandomLastView() {

        $productrand =   DB::select(DB::raw('SELECT  id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand FROM product WHERE `show_hidden` = "1"  ORDER BY RAND() limit 24 '));

        return $productrand;
    }


    public function getProduct($data){

    $whereData = [];
    $whereData[] = ['show_hidden', '1'];

    if(!empty($data['limit'])) {
        $limit = $data['limit'];
    } else {
        $limit = 100;
    }

    if(!empty($data['type'])) {
        if($data['type'] == 'gift') {
            $whereData[] = ['gift', '<>', ''];
        } 
    }

    if(!empty($data['pricelow']) && $data['pricelow'] != '' || !empty($data['priceheight']) && $data['priceheight'] != ''){
            if($data['pricelow'] != '' && $data['priceheight'] != ''){
                if($data['pricelow'] == $data['priceheight']) {
                    $whereData[] = ['nat_price', '<>', $data['pricelow']];
                } elseif ($data['pricelow'] < $data['priceheight']) {
                    $whereData[] = ['nat_price', '>=', $data['pricelow']];
                    $whereData[] = ['nat_price', '<=', $data['priceheight']];
                } else if ($data['pricelow'] > $data['priceheight']) {
                    $whereData[] = ['nat_price', '>=', $data['priceheight']];
                    $whereData[] = ['nat_price', '<=', $data['pricelow']];
                }
            } else if($data['pricelow'] != ''){
                $whereData[] = ['nat_price', '>=', $data['pricelow']];
            } else if($data['priceheight'] != ''){
                $whereData[] = ['nat_price', '<=', $data['priceheight']];
            }
        }

        if(!empty($data['cat']) && $data['cat'] != '' || !empty($data['parent']) && $data['parent'] != ''){
            if(!empty($data['cat']) && $data['cat'] != '' && !empty($data['parent']) && $data['parent'] != ''){
                if($data['cat'] == $data['parent']) {
                    $whereData[] = ['categoryid', $data['parent']];
                } else {
                    $whereData[] = ['categoryid', $data['parent']];
                    $whereData[] = ['category_subid', $data['cat']];
                }
            } else if($data['parent'] != ''){
                $whereData[] = ['categoryid', $data['parent']];
            } else if($data['cat'] != ''){
                $whereData[] = ['category_subid', $data['cat']];
            }
        }

        if(!empty($data['star']) && $data['star'] != ''){
            $star_set = explode(',',$data['star']);
            $star_sets = array();
            foreach ($star_set as $value) {
                if($value != ''){
                    $star_sets[] = $value;
                }
            }
            $intval = implode(',', array_map('intval', $star_sets));
            $whereInData = [$intval];
        } else {
            $whereInData = ['0', '1', '2', '3', '4', '5'];
        }

        if(!empty($data['pid']) && $data['pid'] != ''){
            $whereInData = [$data['pid']];
        }
        
        if(!empty($data['order']) && $data['order'] != ''){
            switch ($data['order']) {
                case 'desc':
                    $orderData = [
                            ['nat_price', 'DESC'],
                            ['special', 'DESC'],
                        ];
                break;
                case 'asc':
                    $orderData = [
                            ['nat_price', 'ASC'],
                            ['special', 'DESC'],
                        ];
                break;
                case 'az':
                    $orderData = ['name_product', 'ASC'];
                break;
                case 'za':
                    $orderData = ['name_product', 'DESC'];
                break;
                case 'new':
                    $orderData = ['id', 'DESC'];
                break;
            }
        }  else {
            $orderData = ['id', 'DESC'];
        }


    $datas =  DB::table('product')->select('id','cashback_percent','categoryid','category_subid','aff_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','promotion_id','details','gift','brand',DB::raw('(CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order'))->where($whereData)->whereIn('rankstar', $whereInData)->limit($limit)->orderBy('id', 'DESC')->get();

        if(!empty($datas)) {
            return $datas;
        } else {
            return false;
        }     

    }
   /* public function getProductClearance($data){
    $product_clearance = $this->db->raw('SELECT * FROM `product` WHERE `promotion_id` = \''.$promotion->PromtionId.'\' AND `show_hidden` = \'1\'');*/

    public function getProductClearance($data){

    $whereData = [];
    $whereData[] = ['show_hidden', '1'];

    if(!empty($data['limit'])) {
        $limit = $data['limit'];
    } else {
        $limit = 100;
    }

    if(!empty($data['pricelow']) && $data['pricelow'] != '' || !empty($data['priceheight']) && $data['priceheight'] != ''){
            if($data['pricelow'] != '' && $data['priceheight'] != ''){
                if($data['pricelow'] == $data['priceheight']) {
                    $whereData[] = ['nat_price', '<>', $data['pricelow']];
                } elseif ($data['pricelow'] < $data['priceheight']) {
                    $whereData[] = ['nat_price', '>=', $data['pricelow']];
                    $whereData[] = ['nat_price', '<=', $data['priceheight']];
                } else if ($data['pricelow'] > $data['priceheight']) {
                    $whereData[] = ['nat_price', '>=', $data['priceheight']];
                    $whereData[] = ['nat_price', '<=', $data['pricelow']];
                }
            } else if($data['pricelow'] != ''){
                $whereData[] = ['nat_price', '>=', $data['pricelow']];
            } else if($data['priceheight'] != ''){
                $whereData[] = ['nat_price', '<=', $data['priceheight']];
            }


        }

        $orderData = ['id', 'DESC'];

        $date = date('Y-m-d');
        $time = date('H:i:s');

        $promotion_data =  DB::table('promotion')->select('PromtionId')->where('ProductType', '6')->where('PromotionStart', '<=', $date)->where('PromotionEnd', '>=', $date)->orderBy('id', 'DESC')->first();

        
        if (!empty($promotion_data->PromtionId)) {

            $whereData[] = ['promotion_id', $promotion_data->PromtionId];

            $datas =  DB::table('product')->select('id','cashback_percent','categoryid','category_subid','aff_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','promotion_id','details','gift','brand',DB::raw('(CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order'))->where($whereData)->limit($limit)->orderBy('id', 'DESC')->get();

        } else {
           $datas = ''; 
        }

        if(!empty($datas)) {
            return $datas;
        } else {
            return false;
        }     

    }


    public function getProductId($pid){

        $data = DB::table('product')->where('id', $pid)->first();

        return $data;


    }
    public function getProductIdUserId($pid){


        $datas =  DB::table('product')->where('id', $pid)->first();
        return $datas;
    }
    public function getProductFavourite(){

        $datas =  DB::table('product_favourite')->get();
        return $datas;
    }
    public function getProductFavouriteId($pid='',$id_user){

        $RouteName = Route::currentRouteName();

        if($RouteName == 'ProductFavouritePidUserId'){

            $datas =  DB::table('product_favourite')->where('pid', $pid)->where('id_user', $id_user)->get();
            return json_encode($datas);

        }else if($RouteName == 'ProductFavouriteUserId'){

            $datas =  DB::table('product_favourite')->where('id_user', $id_user)->get();
            return json_encode($datas);

        }
    }
    public function getReviewConfig(){

        $datas =  DB::table('review_config')->first();
        return $datas;
    }

    public function getProductReviewByTag($pid='',$uid='',$oid=''){

        $datas =  DB::table('product_review')->select('image','message','edit','tag')->where('pid', $pid)->where('user_id', $uid)->where('oid', $oid)->where('status', '1')->groupBy('pid')->first();

        return $datas;
    }


    public function getProductCountReviewByUseridInv($uid='',$oid=''){

        $datas =  DB::table('product_review')->select(DB::raw('COUNT(id) as total_review'))->where('user_id', $uid)->where('oid', $oid)->get();
        return $datas;
    }


    public function getProductReview(){

        $datas =  DB::table('product_review')->get();
        return $datas;
    }

     public function getProductReviewPidUserid($explode_array,$user_id,$inv){


        $datas =  DB::table('product_review')->whereIn('pid', $explode_array)->where('user_id', $user_id)->where('oid', $inv)->where('status', '1')->groupBy('pid')->get();

        return $datas;
    }
    /*  public function getProductReviewInvUserid($inv,$user_id){

        $datas =  DB::table('product_review')->where('inv', $inv)->where('user_id', $user_id)->get();
        return $datas;
    }*/
    public function getCountProductReview($pid){

        $datas =  DB::table('product_review')->select(DB::raw('COUNT(id)'))->where('pid', $pid)->groupBy('user_id')->get();
        return $datas;
    }
    public function getProductRankstar($pid , $uid){

        $datas =  DB::table('rankstar')->where('pid', $pid)->where('uid', $uid)->first();
        return $datas;
    }

     public function getRankstarByReview($pid , $uid){

        $datas =  DB::table('rankstar')->select('rankstar','id')->where('pid', $pid)->where('uid', $uid)->orderBy('id', 'DESC')->first();

        return $datas;
    }


    public function getProductRating($pid){

        $datas =  DB::table('rankstar')->select(DB::raw('AVG(rankstar) AS rankstar'))->where('pid', $pid)->where('rankstar','<>', '0' )->first();
        return round($datas[0]->rankstar);
    }
    public function getCountProductRating($pid){

        $datas =  DB::table('rankstar')->select(DB::raw('COUNT(rankstar) AS rankstar'))->where('pid', $pid)->where('rankstar','<>', '0' )->get();
        return round($datas[0]->rankstar);
    }
    public function getProductStarRating($pid,$star){

        $datas =  DB::table('rankstar')->select(DB::raw('COUNT(rankstar) AS rankstar'))->where('pid', $pid)->where('rankstar', $star)->get();
        return round($datas[0]->rankstar);
    }
    public function getProductSku($sku_id , $sku_item , $pid){

        $datas =  DB::table('product')->where('show_hidden', '1')->where('sku_id', $sku_id)->where('sku_item', $sku_item)
        ->where('id','<>', $pid)->get();
        return $datas;
    }
    public function getCommentProgress($pid){

        $total_RatingCount = $this->getCountProductRating($pid);
        $ranking5 = $this->getProductStarRating('5',$pid);
        $ranking4 = $this->getProductStarRating('4',$pid);
        $ranking3 = $this->getProductStarRating('3',$pid);
        $ranking2 = $this->getProductStarRating('2',$pid);
        $ranking1 = $this->getProductStarRating('1',$pid);

        $commentprogress = array(
            'ranking5' => $ranking5,
            'Totalranking5' => $ranking5 / $total_RatingCount * 100,
            'ranking4' => $ranking4,
            'Totalranking4' => $ranking4 / $total_RatingCount * 100,
            'ranking3' => $ranking3,
            'Totalranking3' => $ranking3 / $total_RatingCount * 100,
            'ranking2' => $ranking2,
            'Totalranking2' => $ranking2 / $total_RatingCount * 100,
            'ranking1' => $ranking1,
            'Totalranking1' => $ranking1 / $total_RatingCount * 100
        );
        return json_encode($commentprogress);
    }
    public function getProductRandom($pid='',$limit=''){

        $RouteName = Route::currentRouteName();

        if($RouteName == 'ProductRandom1'){

            $datas =  DB::table('product')->where('id','<>', $pid)->groupBy('sku_id')->groupBy('sku_item')->limit($limit)->inRandomOrder()->get();

        }else if($RouteName == 'ProductRandom2'){

            $datas =  DB::table('product')->where('id','<>', $pid)->select('sku_id')->groupBy('sku_id')->select('sku_item')->groupBy('sku_item')->limit($limit)->inRandomOrder()->get();

        }else if($RouteName == 'ProductRecommend'){

            $datas =   DB::select(DB::raw('SELECT name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent,details FROM `product` WHERE `show_hidden` = \'1\' GROUP BY sku_id,sku_item ORDER BY RAND() LIMIT '.$limit.''));

        }


        return $datas;
    }
    public function getProductCategory($pid,$categoryid,$categorysubid){

        $datas =  DB::table('product')->where('id','<>', $pid)->where('categoryid', $categoryid)->where('category_subid', $categorysubid)->select('sku_id')->groupBy('sku_id')->select('sku_item')->groupBy('sku_item')->limit(6)->inRandomOrder()->get();
        return $datas;
    }
    public function getProductReviewMaxId($pid){

        $datas =   DB::select(DB::raw('SELECT * FROM product_review WHERE id IN (SELECT MAX(id) FROM product_review WHERE `pid` = \''.$pid.'\' AND `status` = \'1\' 
            GROUP BY user_id) GROUP BY user_id'));
        return $datas;
    }
    /*function getProductFavouriteApp($userid){

        $datas =  DB::table('product_favourite_app')->where('id_user', $userid)->get();
        return $datas;
    }*/


    public function getMainSearch()
    {


        $main_sc_product_data['sc_product_data'] = DB::select(DB::raw('SELECT id,imageshow_cover,name_product,nat_price,snat_price,taglable,name_product_sale FROM `product` WHERE 
            `show_hidden` = 1 ORDER BY RAND() LIMIT 2'));


        $textdata = '';
        for ($i = 0; $i < count($main_sc_product_data['sc_product_data']) ; $i++) {

            $textdata .= $main_sc_product_data['sc_product_data'][$i]->taglable . ",";
        }

        $unique = array_unique(explode(',', $textdata));

        return json_encode($main_sc_product_data);
    }

    public function getProductColumn(){

        $datas =   DB::select(DB::raw('SELECT  name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent FROM product WHERE show_hidden = \'1\' GROUP BY sku_id,sku_item ORDER BY id DESC LIMIT 10'));

        return $datas;

    }

    public function getProductNew($limit='10'){

        $datas =   DB::select(DB::raw('SELECT  name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent FROM product WHERE show_hidden = \'1\' GROUP BY sku_id,sku_item ORDER BY id DESC LIMIT '.$limit));

        return $datas;

    }

    public function getProductRecommend($limit='10'){

        $datas =   DB::select(DB::raw('SELECT  name_product_sale,name_product,id,imageshow_cover,snat_price,nat_price,cashback_percent,aff_percent FROM product WHERE show_hidden = \'1\' GROUP BY sku_id,sku_item ORDER BY RAND() LIMIT '.$limit));

        return $datas;

    }

    public function getProductTrade(){

        $datas =  DB::table('product_trade')->get();

        return $datas;

    }


    public function getProductTradeDate($datestart,$dateend){


        $datas =  DB::table('product_trade')->where('date_start','>=', $datestart)->where('date_end','<=', $dateend)->where('status_trade','<=', '1')->limit('5')->get();

        return $datas;

    }

    public function getProductTradeId($pid){

        $datas =  DB::table('product_trade')->where('pid', $pid)->get();

        return $datas;

    }

    public function getTagsbyCategory($categoryid,$limit){


        $datas =  DB::table('product')->select('taglable')->where('show_hidden', '1')->where('categoryid', $categoryid)->where('taglable','<>','')->groupBy('taglable')->inRandomOrder()->limit($limit)->get();

        return $datas;
    }


    public function getSearchProduct($keyword){

        $name = urldecode($keyword);

        $RouteName = Route::currentRouteName();

        if($RouteName == 'SearchOrderByTaglableDESC'){

            $datas =   DB::select(DB::raw('SELECT  id,imageshow_cover,name_product,nat_price,snat_price,taglable,name_product_sale FROM product WHERE `show_hidden` = \'1\' AND `qty_sale` != \'0\' AND `taglable` LIKE \'%'.$name.'%\' GROUP BY taglable ORDER BY taglable DESC '));

        }else if($RouteName == 'SearchOrderByTaglableRandom'){

            $datas =   DB::select(DB::raw('SELECT  id,imageshow_cover,name_product,nat_price,snat_price,taglable,name_product_sale FROM product WHERE `show_hidden` = \'1\' AND `qty_sale` != \'0\' AND `taglable` LIKE \'%'.$name.'%\' GROUP BY taglable ORDER BY RAND() '));

        }


        return $datas;
    }
    public function getCaseProduct($where='',$orderby='',$limit=''){

     $datas = DB::select(DB::raw("SELECT id,cashback_percent,categoryid,category_subid,aff_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,qty,qty_sale,promotion_id,details,gift,brand,
      (CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order  FROM product  ".$where."  ".$orderby." ".$limit." "));

     return $datas;
 }

   public function getProductProductPagination($data){

    $whereData = [];
    $whereData = [['show_hidden', '1'],];

    if(!empty($data['pricelow']) && $data['pricelow'] != '' || !empty($data['priceheight']) && $data['priceheight'] != ''){
            if($data['pricelow'] != '' && $data['priceheight'] != ''){
                if($data['pricelow'] == $data['priceheight']) {
                    $whereData = [
                        ['nat_price', '<>', $data['pricelow']]
                    ];
                } elseif ($data['pricelow'] < $data['priceheight']) {
                    $whereData = [
                        ['nat_price', '>=', $data['pricelow']],
                        ['nat_price', '<=', $data['priceheight']],
                    ];
                } else if ($data['pricelow'] > $data['priceheight']) {
                    $whereData = [
                        ['nat_price', '>=', $data['priceheight']],
                        ['nat_price', '<=', $data['pricelow']],
                    ];
                }
            } else if($data['pricelow'] != ''){
                $whereData = [
                        ['nat_price', '>=', $data['pricelow']],
                    ];
            } else if($data['priceheight'] != ''){
                $whereData = [
                        ['nat_price', '<=', $data['priceheight']],
                    ];
            }
        }

        if(!empty($data['star']) && $data['star'] != ''){
            $star_set = explode(',',$data['star']);
            $star_sets = array();
            foreach ($star_set as $value) {
                if($value != ''){
                    $star_sets[] = $value;
                }
            }
            $intval = implode(',', array_map('intval', $star_sets));
            $whereInData = [$intval];
        } else {
            $whereInData = [0, 1, 2, 3, 4, 5];
        }
        
        if(!empty($data['order']) && $data['order'] != ''){
            switch ($data['order']) {
                case 'desc':
                    $orderData = [
                            ['nat_price', 'DESC'],
                            ['special', 'DESC'],
                        ];
                break;
                case 'asc':
                    $orderData = [
                            ['nat_price', 'ASC'],
                            ['special', 'DESC'],
                        ];
                break;
                case 'az':
                    $orderData = ['name_product', 'ASC'];
                break;
                case 'za':
                    $orderData = ['name_product', 'DESC'];
                break;
                case 'new':
                    $orderData = ['id', 'DESC'];
                break;
            }
        }  else {
            $orderData = ['id', 'DESC'];
        }


         if(!empty($data['q']) || !empty($data['tag'])){
                if (!empty($data['tag'])) {
                    if(!empty($data['cat']) && $data['cat'] != '' || !empty($data['parent']) && $data['parent'] != ''){
                        if(!empty($data['cat']) && $data['cat'] != '' && !empty($data['parent']) && $data['parent'] != ''){
                            if($data['cat'] == $data['parent']) {
                                $whereData = [
                                    ['categoryid', $data['parent']],
                                ];
                            } else {
                                $whereData = [
                                    ['categoryid', $data['parent']],
                                    ['category_subid', $data['cat']],
                                ];
                            }
                        } else if($data['parent'] != ''){
                            $whereData = [
                                    ['categoryid', $data['parent']],
                                ];
                        } else if($data['cat'] != ''){
                            $whereData = [
                                    ['category_subid', $data['cat']],
                                ];
                        }
                    }

                    $tags = $data['tag'];

                    $datas =  DB::table('product')->select('id','cashback_percent','categoryid','category_subid','aff_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','promotion_id','details','gift','brand',DB::raw('(CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order'))->where('show_hidden', '1')->where('taglable' , 'like' , '%' . $tags . '%')->whereIn('rankstar', $whereInData)->orderBy('id', 'DESC')->paginate($data['limit']);

            } else {

                $qst = implode(' ', $this->get_segment_array($data['q']));

                $query = trim($qst);
                if (mb_strlen($query)===0){
                    return false; 
                }
                $query = substr($query, 0, 200);

                $scoreFullTitle = 2;
                $scoreTitleKeyword = 5;
                $scoreFullSummary = 5;
                $scoreSummaryKeyword = 5;
                $scoreFullDocument = 1;
                $scoreDocumentKeyword = 1;
                $scoreCategoryKeyword = 2;
                $scoreUrlKeyword = 7;
                $scorecolor = 15;

                $keywords = $this->filterSearchKeys($query);
                $titleSQL = array();
                $sumSQL = array();
                $categorySQL = array();
                $urlSQL = array();

                if (count($keywords) > 1){
                    $titleSQL[] = "if (name_product LIKE '%".$keywords[0]."%',{$scoreFullTitle},0)";
                    $sumSQL[] = "if (name_product_sale LIKE '%".$keywords[0]."%',{$scoreFullSummary},0)";
                }

                foreach($keywords as $key){
                    if(strpos($key,"สี") !== false){
                                        // echo "string";
                        $titleSQL[] = "if (name_product LIKE '%".$key."%',{$scorecolor},0)";
                        $sumSQL[] = "if (name_product_sale LIKE '%".$key."%',{$scorecolor},0)";
                        $urlSQL[] = "if (taglable LIKE '%".$key."%',{$scorecolor},0)";
                    }else{
                        $titleSQL[] = "if (name_product LIKE '%".$key."%',{$scoreTitleKeyword},0)";
                        $sumSQL[] = "if (name_product_sale LIKE '%".$key."%',{$scoreSummaryKeyword},0)";
                        $urlSQL[] = "if (taglable LIKE '%".$key."%',{$scoreUrlKeyword},0)";
                    }

                    $urlSQL[] = "if (brand LIKE '%".$key."%',{$scoreUrlKeyword},0)";
                    $categorySQL[] = "if ((SELECT count(categorys.id) FROM categorys WHERE categorys.name = '".$key."') > 0,{$scoreCategoryKeyword},0)";
                }

              if (empty($titleSQL)){
                    $titleSQL[] = 0;
                }
                if (empty($sumSQL)){
                    $sumSQL[] = 0;
                }
                if (empty($docSQL)){
                    $docSQL[] = 0;
                 }
                if (empty($urlSQL)){
                    $urlSQL[] = 0;
                }
                if (empty($tagSQL)){
                    $tagSQL[] = 0;
                }

                $where = '`show_hidden` = \'1\'';

                if(!empty($data['order']) && $data['order'] != ''){
                    switch ($data['order']) {
                        case 'desc':
                            $order = 'nat_price DESC, special DESC';
                        break;
                        case 'asc':
                            $order = 'nat_price ASC, special DESC';
                        break;
                        case 'az':
                            $order = 'name_product ASC';
                        break;
                        case 'za':
                            $order = 'name_product DESC';
                        break;
                        case 'new':
                            $order = 'id DESC';
                        break;
                    }
                }  else {
                    $order = 'id DESC';
                }


            $dataSQL = '('.implode(' + ', $titleSQL).') + ('.implode(' + ', $sumSQL).' )+('.implode(' + ', $docSQL).') + ('.implode(' + ', $categorySQL).') + ('.implode(' + ', $urlSQL).') AS relevance';



             $datas =  DB::table('product as p')->select('id','cashback_percent','categoryid','category_subid','aff_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','promotion_id','details','gift','brand',DB::raw('(CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order, (SELECT pd.pr_price FROM promotion_detail pd WHERE pd.id = p.id AND pd.show_hidden = \'1\' LIMIT 1) AS special'),DB::raw($dataSQL))->where($whereData)->whereIn('rankstar', $whereInData)->orderBy('relevance', 'DESC')->paginate($data['limit']);

            }
        } else {
            if(!empty($data['cat']) && $data['cat'] != '' || !empty($data['parent']) && $data['parent'] != ''){
                if(!empty($data['cat']) && $data['cat'] != '' && !empty($data['parent']) && $data['parent'] != ''){
                    if($data['cat'] == $data['parent']) {
                        $whereData = [
                            ['categoryid', $data['parent']],
                        ];
                    } else {
                        $whereData = [
                            ['categoryid', $data['parent']],
                            ['category_subid', $data['cat']],
                        ];
                    }
                } else if($data['parent'] != ''){
                    $whereData = [
                            ['categoryid', $data['parent']],
                        ];
                } else if($data['cat'] != ''){
                    $whereData = [
                            ['category_subid', $data['cat']],
                        ];
                }
            }

            $datas =  DB::table('product')->select('id','cashback_percent','categoryid','category_subid','aff_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','promotion_id','details','gift','brand',DB::raw('(CASE WHEN (nat_price != 0) THEN nat_price ELSE (CASE WHEN (snat_price != 0) THEN snat_price ELSE nat_price END)END) AS price_order'))->where($whereData)->whereIn('rankstar', $whereInData)->orderBy('id', 'DESC')->paginate($data['limit']);
        }

        return $datas;
   }

   public function get_segment_array($input_string) {
        $string = $input_string;
        $segmented_result = [];

        // ลบเครื่องหมายคำพูด, ตัวแบ่งประโยค //
        $string = str_replace(array('\'', '‘', '’', '“', '”', '"', '-', '/', '(', ')', '{', '}', '...', '..', '…', '', ',', ':', '|', '\\'), '', $string);
        // เปลี่ยน newline ให้กลายเป็น Space เพื่อที่ใช้สำหรับ Trim
        $string = str_replace(array("\r", "\r\n", "\n"), ' ', $string);

        // กำจัดซ้ำ //
        $string = $this->clear_duplicated($string);

        // แยกประโยคจากช่องว่าง (~เผื่อไว้สำหรับภาษาอังกฤษ) //
        $string_exploded = explode(' ', $string);

        // Reverse Array สำหรับการใช้ Dictionary แบบ Reverse //
        foreach ($string_exploded as $input_string_exploded_row) {
            $current_string_reverse_array = array_reverse($this->uni_strsplit(trim($input_string_exploded_row)));

            $current_array_result = $this->_segment_by_dictionary_reverse($current_string_reverse_array);
            foreach ($current_array_result as $each_result) {
                if (trim($each_result) != '')
                    $segmented_result[] = trim($each_result);
            }
        }
        // จัดการคำที่ตัดที่ยาวผิดปกติ (~อาจจะเป็นเพราะว่าพิมผิด) โดยการตัดตาม Dict แบบธรรมดา//
        $tmp_result = array();
        foreach ($segmented_result as $result_row) {
            if (mb_strlen($result_row) > 10) {

                $current_string_array = $this->uni_strsplit(trim($result_row));
                $current_array_result = $this->_segment_by_dictionary($current_string_array);

                foreach ($current_array_result as $current_result_row) {
                    $tmp_result[] = trim($current_result_row);
                }
            } else {
                $tmp_result[] = $result_row;
            }
        }
        $segmented_result = $tmp_result;

        return $segmented_result;
    }

    public function clear_duplicated($string) {
        //หดรูปตัวอักษรซ้ำๆ//
        $input_string_split = $this->uni_strsplit($string);
        $previous_char = '';
        $previous_string = '';
        $dup_list_array = array();
        $dup_list_array_replace = array();
        foreach ($input_string_split as $current_char) {

            if ($previous_char == $current_char) {
                $previous_char = $current_char;
                $previous_string .= $current_char;
            } else {
                if (mb_strlen($previous_string) > 3) {
                    $dup_list_array[] = $previous_string;
                    $dup_list_array_replace[] = $current_char;
                    $string = str_replace($previous_string, $previous_char, $string);
                }
                $previous_char = $current_char;
                $previous_string = $current_char;
            }
        }
        if (mb_strlen($previous_string) > 3) {
            $dup_list_array[] = $previous_string;
            $dup_list_array_replace = $current_char;
        }
        return str_replace($dup_list_array, $dup_list_array_replace, $string);
    }

    public function uni_strsplit($string, $split_length=1) {
        preg_match_all('`.`u', $string, $arr);
        $arr = array_chunk($arr[0], $split_length);
        $arr = array_map('implode', $arr);
        return $arr;
    }

    public function _segment_by_dictionary($input_array) {

        $result_array = array();
        $tmp_string = '';
        $dictionary_array = array();

        $pointer = 0;
        $length_of_string = count($input_array)-1;

        while ($pointer <= $length_of_string) {

            $tmp_string .= $input_array[$pointer];

            if (isset($this->uni_strsplit[crc32($tmp_string)])) { // ถ้าเจอใน Dict //
                $dup_array = array();
                $dup_array[] = array(
                    'title' => $tmp_string,
                    'to_mark' => $pointer + 1,
                );
                $count_more = 0;
                $more_tmp = $tmp_string;
                //echo $more_tmp.'<br/>';


                for ($i = $pointer + 1; $i <= $length_of_string; $i++) {
                    $more_tmp .= $input_array[$i];
                    //echo $more_tmp.'<br/>';
                    //echo $more_tmp.'<br/>';
                    if (isset($this->uni_strsplit[crc32($more_tmp)])) {
                        $dup_array[] = array(
                            'title' => $more_tmp,
                            'to_mark' => $i + 1,
                        );
                        //print_r($dup_array);
                    }

                    $count_more++;
                }

                if (count($dup_array) > 0) {
                    $result_array[] = $dup_array[count($dup_array) - 1]['title'];

                    $pointer = $dup_array[count($dup_array) - 1]['to_mark'];

                    //echo $to_mark;
                } else {
                    
                }
                //echo '-------------------<br/>';
                $dup_array = array();
                $tmp_string = '';
                continue;
            }

            $pointer++;
        }

        if ($tmp_string != '') { //  ส่วนที่เหลือ ถ้าไม่เจอใน Dict
            $result_array[] = $tmp_string;
        }

        if (count($result_array) == 0) {
            return array(implode($input_array));
        }
        return $result_array;
    }

    public function _segment_by_dictionary_reverse($input_array) {

        $result_array = array();
        $tmp_string = '';
        $dictionary_array = array();

        $pointer = 0;
        $length_of_string = count($input_array)-1;

        while ($pointer <= $length_of_string) {

            $tmp_string = $input_array[$pointer] . $tmp_string;

            if (isset($dictionary_array[crc32($tmp_string)])) { // ถ้าเจอใน Dict //
                $dup_array = array();
                $dup_array[] = array(
                    'title' => $tmp_string,
                    'to_mark' => $pointer + 1,
                );
                $count_more = 0;
                $more_tmp = $tmp_string;
                //echo $more_tmp.'<br/>';


                for ($i = $pointer + 1; $i <= $length_of_string; $i++) {
                    $more_tmp = $input_array[$i] . $more_tmp;
                    //echo $more_tmp.'<br/>';
                    //echo $more_tmp.'<br/>';
                    if (isset($dictionary_array[crc32($more_tmp)])) {
                        $dup_array[] = array(
                            'title' => $more_tmp,
                            'to_mark' => $i + 1,
                        );
                        //print_r($dup_array);
                    }

                    $count_more++;
                }

                if (count($dup_array) > 0) {
                    $result_array[] = $dup_array[count($dup_array) - 1]['title'];

                    $pointer = $dup_array[count($dup_array) - 1]['to_mark'];

                    //echo $to_mark;
                } else {
                    
                }
                //echo '-------------------<br/>';
                $dup_array = array();
                $tmp_string = '';
                continue;
            }

            $pointer++;
        }

        if ($tmp_string != '') { //  ส่วนที่เหลือ ถ้าไม่เจอใน Dict
            $result_array[] = $tmp_string;
        }

        if (count($result_array) == 0) {
            return array(implode(array_reverse($input_array)));
        }
        return array_reverse($result_array);
    }

    public function filterSearchKeys($query){
            $query = trim(preg_replace("/(\s+)+/", " ", $query));
            $words = array();
            $list = array("in","it","a","the","of","or","I","you","he","me","us","they","she","to","but","that","this","those","then");
            $c = 0;
            foreach(explode(" ", $query) as $key){
                if (in_array($key, $list)){
                    continue;
                }
                $words[] = $key;
                if ($c >= 15){
                    break;
                }
                $c++;
            }
            return $words;
     }

         
    
    public function getBandings($limit)
    {

         $datas =  DB::table('product_brand')->where('brand_logo', '<>', '')->where('show_hidden','1')->groupBy('brand_name')->inRandomOrder()->limit($limit)->get();
         
        return $datas;
    }

    public function getPromotionProductInArray($explode_array,$limit){

        //return $PromotionProduct;
        //$explode_array = (explode(",",$PromotionProduct));
        if(!empty($limit)){

            $datas =  DB::table('product')->where('show_hidden','1')->whereIn('id',$explode_array)->inRandomOrder()->limit($limit)->get();

        }else{

             $datas =  DB::table('product')->where('show_hidden','1')->whereIn('id',$explode_array)->inRandomOrder()->get();
        }
        
        return $datas;

    
        
    }


public function getBestsellerProduct(){

            $date_before = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-15 day" ) );
            $date_now = date("Y-m-d");

            $order_detail = DB::table('order_detail')->select('order_detail.pid', 'product.sku_item', DB::raw('SUM(order_detail.qty) as sum_qty'))->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->join('product', 'product.id', '=', 'order_detail.pid')->where('order_product.status', '5')->whereBetween('order_detail.date_order', [$date_before, $date_now])->groupBy('order_detail.pid')->limit('5')->get();


            return $order_detail;
            
    }

    public function getProductByCategoryid($id,$limit){

        $datas =  DB::table('product')->select('id','cashback_percent','imageshow_cover','name_product','name_product_sale','nat_price','snat_price','qty','qty_sale','aff_percent','promotion_id')->where('categoryid',$id)->where('show_hidden','1')->where('qty_sale', '>' ,'0')->inRandomOrder()->limit($limit)->get();
        return $datas;
    }
  

}
