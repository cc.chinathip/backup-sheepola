<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Route; // call Route
use DB;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function products()
    {
        return $this->hasMany(Product::class); // One to Many
    }
    public function getUser(){

        $datas =  DB::table('user_acc')->get();
        return $datas;
    }
    public function getUserId($userid){

        $RouteName = Route::currentRouteName();

            if($RouteName == 'WhereId'){

                $datas =  DB::table('user_acc')->where('id', $userid)->get();

            }else if($RouteName == 'UseWithNotification'){

                $datas =   DB::select(DB::raw('SELECT id,gender FROM `user_acc` WHERE `id` = \''.$userid.'\''));

            }

        return $datas;
    }
    public function getUserLineId($responseid){

        $datas =  DB::table('user_acc')->where('line_id', $responseid)->get();
        return $datas;
    }
    public function getUserFacbookId($responseid){

        $datas =  DB::table('user_acc')->where('facebook_id', $responseid)->get();
        return $datas;
    }
    
    public function getUserRecommendlist($userref){

        $datas =  DB::table('user_acc')->where('user_ref', $userref)->get();
        return $datas;
    }
    public function getUserRecommend($value1,$value2=''){


            $datas =  DB::table('user_acc')->where('user_ref', $val1)->where('fbuy',$val2)->get();

        return $datas;
    }

    public function getValueEmail($value1,$value2 = ''){

        $RouteName = Route::currentRouteName();

            if($RouteName == 'checkforgotemail'){

               $datas =  DB::table('user_acc')->where('email', $value1)->get();

           }else if($RouteName == 'checkemail'){

               $datas =  DB::table('user_acc')->where('email', $value1)->where('password_nat', $value2)->get();
           }

        return $datas;
    }

    



}