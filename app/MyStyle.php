<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class MyStyle extends Model
{
    //

	public function getMyStyle(){

		$datas =  DB::table('mystyle')->where('style_status','1')->orderBy('id','DESC')->get();


		return $datas;
	}


	public function getProductMystyleByGroup($mystyleid,$limit='')
	{
		
	
		$datas = DB::select(DB::raw('SELECT pm.date_create,pm.mystyle_id,pm.product_id,pm.product_mystyle_status,pd.* FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE pd.show_hidden = \'1\' AND mystyle_id = '.$mystyleid.'  GROUP BY pd.sku_id,pd.sku_item ORDER BY pd.id DESC  LIMIT 4 '));

		return $datas;
	}


	public function getMyStyleId($id){

		$datas =  DB::table('mystyle')->where('id',$id)->first();

		return $datas;
	}


	public function getProductsByMystyle($mystyle_id='')
	{
		

		$datas = DB::select(DB::raw('SELECT pm.date_create,pm.mystyle_id,pm.product_id,pm.product_mystyle_status,pd.id,pd.cashback_percent,pd.imageshow_cover,pd.name_product,pd.name_product_sale,pd.nat_price,pd.snat_price,pd.aff_percent,pd.qty,pd.qty_sale,pd.promotion_id,pd.gift FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE pd.show_hidden = \'1\' AND mystyle_id = '.$mystyle_id.' GROUP BY pd.sku_id,pd.sku_item ORDER BY pd.id DESC LIMIT 3'));



		return $datas;
	}


	public function CountProductsMystyle($mystyle_id='')
	{
		

		$product_total = DB::select(DB::raw('SELECT COUNT(id) AS total FROM `product_mystyle` WHERE `mystyle_id` = \''.$mystyle_id.'\' AND product_mystyle_status = \'1\' '));

		return $product_total[0]->total;
	}




	public function getProductMystyleById($mystyleid)
	{
		
		$datas = DB::select(DB::raw('SELECT pm.date_create,pm.mystyle_id,pm.product_id,pm.product_mystyle_status,pd.* FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE pd.show_hidden = \'1\' AND pm.mystyle_id = '.$mystyleid.'  GROUP BY pd.sku_id,pd.sku_item ORDER BY pd.id DESC  '));

		return $datas;
	}


	public function CountProductsByMystyle($mystyle_id='')
	{
	
		$datas = DB::select(DB::raw('SELECT pm.date_create,pm.mystyle_id,pm.product_id,pm.product_mystyle_status,pd.* FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE pd.show_hidden = \'1\' AND pm.mystyle_id = '.$mystyle_id.'   GROUP BY pd.sku_id,pd.sku_item ORDER BY pd.id DESC  '));



		return COUNT($datas);
	}



	public function getMystyleProducts($limit){


		$resp['mystyle'] =  DB::table('mystyle')->where('style_status','1')->orderBy('id','DESC')->limit($limit)->get();

		foreach ($resp['mystyle'] as $vm) {
			$resp['products'][$vm->id] = $this->getProductsByMystyle($vm->id);


			$resp['count_products'][$vm->id] = $this->CountProductsByMystyle($vm->id);
		}
		//print_r($vm->id);
		return json_encode($resp);
	}


	public function getMystyleProductsALLById($id_mystyle,$limit=''){

		$product_mystyle = DB::select(DB::raw('SELECT pd.* FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE mystyle_id = '.$id_mystyle.' AND product_mystyle_status = \'1\' AND pd.show_hidden = \'1\' ORDER BY RAND() '.$limit.' '));


		return $product_mystyle;
	}



	public function getMyStyleRandom(){

		/*$get_mystyle_list = $this->db->table('mystyle')->value('id,stylename')->where('`style_status` = \'1\'')->order_by('RAND()')->find_all();*/
		$get_mystyle_list =  DB::table('mystyle')->select('id','stylename')->where('style_status','1')->inRandomOrder()->get();

		$i = 0;
		foreach ($get_mystyle_list as $key_mystyle_list => $value_mystyle_list) {

			$id_mystyle = $value_mystyle_list->id;
			$namemy = $value_mystyle_list->stylename;


			$product_mystyle = DB::select(DB::raw("SELECT pm.mystyle_id,pd.imageshow_cover FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE 
				mystyle_id = ".$id_mystyle." AND pd.show_hidden = '1' AND product_mystyle_status = '1' ORDER BY RAND() LIMIT 4"));



			if (is_array($product_mystyle)) {


				$product_total = DB::select(DB::raw("SELECT COUNT(id) AS total FROM `product_mystyle`  WHERE mystyle_id = ".$id_mystyle."  AND product_mystyle_status = '1' "));


				foreach($product_total as $key_product_total => $value_product_total)
				{

					$product_mystyle[0]->myname = $namemy;
					$product_mystyle[0]->total = $value_product_total->total;

					$result['product_mystyle'][$i] = $product_mystyle;

				} 

			}else{
				$result['product_mystyle'][$i] = 'not_product';
			}


			$i++;
		}

	
		return   json_encode($result);

	}
	

	public function getMyStyleTotal(){

		//$main_product['mystyle'] = $this->db->raw('SELECT stylename,id FROM `mystyle` WHERE `style_status` = \'1\' ORDER BY RAND() LIMIT 6');

		$main_product['mystyle'] = DB::select(DB::raw("SELECT stylename,id FROM `mystyle` WHERE `style_status` = '1' ORDER BY RAND() LIMIT 6"));

		foreach ($main_product['mystyle'] as $v) {
			
			/*$main_product['product_mystyle'][$v->id] = $this->db->raw('SELECT pm.mystyle_id,pd.imageshow_cover FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE mystyle_id = '.$v->id.' AND pd.show_hidden = \'1\' AND product_mystyle_status = \'1\' GROUP BY pd.sku_id,pd.sku_item ORDER BY RAND() LIMIT 3');*/

			$main_product['product_mystyle'][$v->id] = DB::select(DB::raw("SELECT pm.mystyle_id,pd.imageshow_cover FROM `product_mystyle` as pm JOIN product as pd ON pd.id = pm.product_id WHERE mystyle_id = '.$v->id.' AND pd.show_hidden = '1' AND product_mystyle_status = '1' GROUP BY pd.sku_id,pd.sku_item ORDER BY RAND() LIMIT 3"));

			/*$product_total = $this->db->raw('SELECT COUNT(id) as total FROM `product_mystyle` WHERE `mystyle_id` = \''.$v->id.'\' AND product_mystyle_status = \'1\'');
*/
			$product_total = DB::select(DB::raw("SELECT COUNT(id) as total FROM `product_mystyle` WHERE `mystyle_id` = '".$v->id."' AND product_mystyle_status = '1'"));

			$main_product['product_mystyle_count'][$v->id] = $product_total[0]->total;

			return $main_product;
			
		}
	}


	
}
