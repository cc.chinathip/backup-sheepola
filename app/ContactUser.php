<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class ContactUser extends Model
{
    //
    public function getContactUserId($id){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowContactUserId'){

			$datas =  DB::table('contact_user')->where('userid',$id)->get();

		}else if($RouteName == 'ShowContactId'){

			$datas =  DB::table('contact_user')->where('id',$id)->get();
		}

		

		return $datas;
	}

	public function getContactUserId2($userid,$id){

		
		$datas =  DB::table('contact_user')->where('userid',$userid)->where('id',$id)->get();

		return $datas;
	}

}
