<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route
use App\User;

class Notification extends Model
{
    //

    public function getNotification(){


		$datas =  DB::table('notification')->get();
			
		return $datas;
	}

	 public function getNotificationId1($promotionid,$userid){

    	
		$datas =  DB::table('notification')->where('promotion_id',$promotionid)->where('user_id',$userid)->get();

    	
    	return $datas;
	}

	public function getNotificationId2($promotionid){

    	
		$datas =  DB::table('notification')->where('promotion_id',$promotionid)->get();

    	
    	return $datas;
	}

	public function getNoticeNew(){

    	
		$datas =  DB::table('notice_new')->get();

    	return $datas;
	}

	public function getNoticeNewUserId($userid){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowNoticeNewUserId1'){
    	
			$datas =  DB::table('notice_new')->where('id_user',$userid)->orderBy('date_time', 'desc')->limit(30)->get();

		}else if($RouteName == 'ShowNoticeNewUserId2'){

			$datas =  DB::table('notice_new')->where('id_user',$userid)->orWhere('id_promo','<>' ,'0')->orderBy('date_time', 'desc')->limit(30)->get();

		}else if($RouteName == 'ShowNoticeNewUserId3'){

			$datas =  DB::table('notice_new')->where('id_user',$userid)->where('is_type' ,'1')->orderBy('date_time', 'desc')->limit(30)->get();

		}else if($RouteName == 'ShowNoticeNewUserId4'){

			$datas =  DB::table('notice_new')->where('id_user',$userid)->where('is_type' ,'0')->orderBy('date_time', 'desc')->limit(30)->get();

		}

    	return $datas;
	}


	public function getNoticeNewUseridId($userid,$id){

		$datas =  DB::table('notice_new')->where('id_user',$userid)->where('id' ,$id)->get();

    	return $datas;
	}

	public function getReadNotice($id,$uid){

		$datas =  DB::table('notice_new')->where('id',$id)->get();

		
		if($datas->user_read == ''){

			$result = $uid;

		}else{
			$result = $notice_new->user_read.','.$uid;
		}

		$result = DB::insert($result);

		

    	return $result;
	}
	
/*public function read_notice($id='',$uid='')
{
	$notice_new = $this->database->table('notice_new')->no_cache()->where('`id` = \''.$id.'\'')->find_one();
	if($notice_new->user_read == ''){
		$setuserread = $uid;
	}else{
		$setuserread = $notice_new->user_read.','.$uid;
	}
	$notice_new->set('user_read',$setuserread);
	echo $notice_new->save();
}
*/

public function getNotificationWithPromotion($id=''){
		$count = 0;

		$GetUserModel = new User();

		$user_acc = $GetUserModel->getUserId($id);

		$GetPromotionModel = new Promotion();

		$promotion = $GetPromotionModel->getPromotionNotification();

		foreach ($promotion as $v) {
			$_get = true;
			if ($v->gender != null) {
				if ($gender != $v->gender) {
					$_get = false;
				}
			}

			if ($_get == true) {
				
				$notification =   DB::select(DB::raw('SELECT id FROM `notification` WHERE `is_read` != \'1\' AND `promotion_id` = \''.$v->id.'\' AND `user_id` = \''.$user_acc[0]->id.'\' ORDER BY id DESC'));

				if (is_array($notification)) {
					$count += 1;
				}
			}
		}

		$notice_new =   DB::select(DB::raw('SELECT user_read FROM `notice_new` WHERE `id_user` = \''.$user_acc[0]->id.'\' ORDER BY date_time DESC LIMIT 30'));
		
		foreach ($notice_new as $v) {
			if ($v->user_read == null) {
				$count += 1;
			}
		}
		//echo json_encode($count);

		return json_encode($count);

		
	}

    
}
