<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Chat extends Model
{
    //

	public function getChatRoom(){

		$datas =  DB::table('chatroom')->get();

		return $datas;
	}

	public function getChatDetail(){

		$datas =  DB::table('chatdetail')->get();

		return $datas;
	}

	public function getChatRoomId($id){

		$datas =  DB::table('chatroom')->where('id_user',$id)->get();

		return $datas;
	}

	public function getChatDetailId($id){


		$RouteName = Route::currentRouteName();
		
		if($RouteName == 'ShowChatDetailId1'){

			$datas =  DB::table('chatdetail')->where('idroom',$id)->where('id_admin','<>' ,'0')->get();

		}else if($RouteName == 'ShowChatDetailId2'){

			$datas =  DB::table('chatdetail')->where('idroom',$id)->limit('20')->orderBy('id','desc')->get();

		}

		return $datas;
	}

	

	

  
}
