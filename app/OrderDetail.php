<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class OrderDetail extends Model
{
    //
    public function getOrderDetail(){

		$datas =  DB::table('order_detail')->get();
		return $datas;
	}

	public function getOrderDetailInv($userid,$inv){

		$datas =  DB::table('order_detail')->where('uid',$userid)->where('oid',$inv)->get();
		return $datas;
	}

	public function getCountOrderDetailUseridInv($userid,$inv){

		$order_detail =  DB::table('order_detail')->select(DB::raw('COUNT(id) as total_order_detail,oid'))->where('uid',$userid)->where('oid',$inv)->first();

		//return $order_detail->total_order_detail;

		$product_review =  DB::table('product_review')->select(DB::raw('COUNT(id) as total_review'))->where('user_id', $userid)->where('oid', $inv)->first();

		if($order_detail->total_order_detail == $product_review->total_review){

			$total_equal = 1;

		}else{

			$total_equal = 0;
		}

		$total = array(
			'total_equal' => $total_equal,
			'inv' => $order_detail->oid
		);
		
		return $total;
	}


	public function getOrderDetailJoinOrderProduct($userid,$inv){

		
		$datas =  DB::table('order_detail')->select('order_detail.oid','order_detail.uid','order_detail.qty','order_detail.pid', 'product.sku_item', 'product.nat_price','product.snat_price','product.imageshow_cover','product.name_product','product.name_product_sale','order_detail.priceperpcs')->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->join('product', 'product.id', '=', 'order_detail.pid')->where('order_product.status', '6')->where('order_product.user_id', $userid)->where('order_product.oid', $inv)->get();


	
		return $datas;
	}

	public function getOrderDetailJoinProductReview($userid,$inv){

		$datas =  $order_detail = DB::table('order_detail')->select('order_detail.qty','order_detail.pid', 'product.sku_item', 'product.nat_price','product.snat_price','product.imageshow_cover','product.name_product','product.name_product_sale','order_detail.priceperpcs')->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->join('product', 'product.id', '=', 'order_detail.pid')->join('product_review', 'product_review.pid', '=', 'order_detail.pid')->where('order_product.status', '6')->where('order_product.user_id', $userid)->where('order_product.oid', $inv)->get();

		return $datas;
	}


	

	
}
