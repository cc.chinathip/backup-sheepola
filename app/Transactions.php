<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Transactions extends Model
{
    //
        public function getTransactionsId($id){

         $RouteName = Route::currentRouteName();

         if($RouteName == 'ShowTransactionsId1'){

           $datas =  DB::table('transactions')->where('inv_id',$id)->where('status_pay','4')->get();

         }else if($RouteName == 'ShowTransactionsId2'){

           $datas =  DB::table('transactions')->where('user_id',$id)->where('status', '<>' ,'0')->where('cashback', '1')->limit('20')->orderBy('id', 'desc')->get();

         }else if($RouteName == 'ShowTransactionsId3'){

           $datas =  DB::table('transactions')->where('user_id',$id)->where('user_read','unread')->get();


         }


         return $datas;

       }

       public function getPendingCashBack($userid,$lastdate=''){

        /*$datas = $this->database->table('transactions')->no_cache()->where('`user_id` LIKE \''.$uid.'\' AND `cashback` = \'1\' AND (`status` = \'1\' OR `status` = \'2\')')->order_by('date_time DESC')->find_all();*/


        $datas =  DB::table('transactions')->where('user_id', 'LIKE' ,$userid)->where('status', '1')->orWhere('status', '2')->where('cashback','1')->orderBy('date_time','desc')->get();


        if($lastdate == 'true'){

          return $datas[0]->date_time;

        }else{

          $pendingcashtotal = 0;
          foreach ($datas as $key => $value) {
            if($value->status == 1){
              $pendingcashtotal += $value->price_total;
            }elseif($value->status == 2){
              $pendingcashtotal -= $value->price_total;
            }

          }
          return number_format($pendingcashtotal);
        }
      }


      public function getWithDrawalHistory($userid,$lastdate=''){

          //$datas = $this->database->table('withdrawal_history')->no_cache()->where('`id_user` = \''.$uid.'\'')->order_by('date_time_wd DESC')->find();


        $datas =  DB::table('withdrawal_history')->where('id_user', $userid)->orderBy('date_time_wd','desc')->get();

        foreach ($datas as $key => $value) {

          if($lastdate == 'true'){

            return $datas[0]->date_time_wd;

          }else{
            if(empty($datas[0]->amount)){
              return number_format(0,2);
            }else{
              return number_format($datas[0]->amount,2);
            }
          }

        }


      }

      public function getTotalHistoryUnRead($userid){

       
        $datas = DB::select(DB::raw('SELECT COUNT(id) total FROM `transactions` WHERE `user_id` = \''.$userid.'\' AND `user_read` = \'unread\' '));

        return $datas;

      }

      public function getPaymentGatewayId($id){

       
        $datas =  DB::table('payment_gateway')->where('id',$id)->get();

        return $datas;

      }





}
