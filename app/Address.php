<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Address extends Model
{
    //

	public function getProvince(){

		
		$datas =  DB::table('province')->orderBy('name_in_thai', 'asc')->get();

		return $datas;
	}

	public function getAmphur(){

		
		$datas =  DB::table('amphur')->orderBy('name_in_thai', 'asc')->get();

		return $datas;
	}


	public function getTambonId($value){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowTambonWhereAmphur'){

			$datas =  DB::table('tambon')->where('amphur_id', $value)->orderBy('name_in_thai', 'asc')->get();

		}else if($RouteName == 'ShowTambonWhereId'){

			$datas =  DB::table('tambon')->where('id', $value)->get();

		}else if($RouteName == 'ShowTambonLikeNameThai'){

			$datas =  DB::table('tambon')->where('name_in_thai', 'LIKE', "%$value%")->get();

		}

		return $datas;
	}


	public function getAmphurId($value){

		
		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowAmphurNameThai'){

			$datas =  DB::table('amphur')->where('name_in_thai', 'LIKE', "%$value%")->get();

		}else if($RouteName == 'ShowAmphurNameEnglish'){

			$datas =  DB::table('amphur')->where('name_in_english', 'LIKE', "%$value%")->get();

		}else if($RouteName == 'ShowAmphurWhereProvinceid'){

			$datas =  DB::table('amphur')->where('province_id', $value)->orderBy('name_in_thai','asc')->get();
		}


		return $datas;
	}

	public function getProvinceId($value){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowProvinceNameThai'){

			$datas =  DB::table('province')->where('name_in_thai', 'LIKE', "%$value%")->get();

		}else if($RouteName == 'ShowProvinceNameEnglish'){

			$datas =  DB::table('province')->where('name_in_english', 'LIKE', "%$value%")->get();
		}
		
		return $datas;
	}



}
