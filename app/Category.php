<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route
use App\Promotion;
use App\Product;

class Category extends Model
{
     function getCategoryMobile(){
        $cate = [];
     
        $CateRand =  DB::table('categorys')->where('show_hidden','1')->where('parentid','0')->orderBy('name')->get();

        foreach ($CateRand as $caterand) {
                $catename=$caterand->name;
                $Catesub =  DB::table('categorys')->where('show_hidden','1')->where('parentid',$caterand->id)->orderBy('name')->get();

                $i=0;
                $parent_array = [];
                foreach ($Catesub as $r) {
                 
                $catename=$r->name;

                    $parent_array[] = array(
                            'url' => env('SHEEP_URL').'product/?parent='.$r->parentid.'&cat='.$r->id, 
                            'name' => $catename, 
                            );
                }

                $cate[] = array(
                    'id' => $caterand->id,
                    'classtitle' => 'icon icon-cate-'.$caterand->id.' clr-grad-1',
                    'class' => 'cd-nav cd-nav-'.$caterand->id.' hidden',
                    'color' => $caterand->color,
                    'urlall' => env('SHEEP_URL').'product/?parent='.$caterand->id.'&cat='.$caterand->id,
                    'nameall' => '',
                    'image' => env('SHEEP_URL').'static/newiconapp/cate_w/'.$caterand->id.'.png',
                    'name' => $catename,
                    'parent' => $parent_array
                    );
        }

        return $cate;
    }

    public function getCategoryAll(){

            $cate = [];
            // $Catesub=[];
            $CateRand = DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','bgcolor','color')->where('show_hidden','1' )->where('parentid','0')->orderBy('name')->get();

         
    
        foreach ($CateRand as $caterand) {
           
                $catename=$caterand->name;

                $Catesub = DB::table('categorys')->select('id','parentid','name')->where('show_hidden','1' )->where('parentid',$caterand->id)->orderBy('name')->first();

                if (!empty($Catesub)) {
                    # code...
             
                  
                    $catename=$Catesub->name;

                    $parent_array[] = array(
                            'url' => 'product/?parent='.$Catesub->parentid.'&cat='.$Catesub->id, 
                            'name' => $catename, 
                            );
                    
                    //$cate .='<li><a href="'.BASE_URL.'product/?parent='.$r->parentid.'&cat='.$r->id.'"><span class="fa fa-angle-right" aria-hidden="true"></span> '.$catename.'</a></li>';
   }
                $cate[] = array(
                    'id' => $caterand->id,
                    'classtitle' => 'icon icon-cate-'.$caterand->id.' clr-grad-1',
                    'class' => 'cd-nav cd-nav-'.$caterand->id.' hidden',
                    'color' => $caterand->color,
                    'urlall' => 'product/?parent='.$caterand->id.'&cat='.$caterand->id,
                    'nameall' => '',
                    'image' => 'static/newiconapp/cate_w/'.$caterand->id.'.png',
                    'name' => $catename,
                    'parent' => $parent_array
                    );

    }
         return $cate;
    }

public function Menucateleft(){

    $GetPromotionModel = new Promotion();
    $GetProductModel = new Product();

    $cate = [];
        
    $CateRand = DB::table('categorys')->where('show_hidden','1')->where('parentid','0')->orderBy('name')->get();

    foreach ($CateRand as $caterand) {
            $parent_array = [];
        
                $catename=$caterand->name;
                $Catesub = DB::table('categorys')->where('show_hidden','1')-> where('parentid' ,$caterand->id)->orderBy('name')->get();
        


            for ($i=0; $i < count($Catesub) ; $i++){

              
                    $catenamesub=$Catesub[$i]->name;

                $parent_array[] = array(
                            'url' => 'https://www.sheepola.com/product/c/'.$Catesub[$i]->parentid.'/'.url_clean($catename).'/'.$Catesub[$i]->id.'/'.url_clean($catenamesub).'.html', 
                            'name' => $catenamesub, 
                            );

            }

            $product_ = DB::table('product')->where('categoryid',$caterand->id)->where('show_hidden','1')->where('qty_sale','>','0')->orderBy('id', 'desc')->limit('2')->get();

            $product_array = [];
            foreach ($product_ as $product) {
                // $promotion_price = $this->mainclass->checkinpromotion($this->db,$product->id,$product->promotion_id);
              
                 $promotion_price = $GetPromotionModel->getCheckPromotion($product->id, $product->promotion_id);

              

                if(!empty($promotion_price) && $promotion_price > 0){
                    $product->nat_price = $promotion_price->pr_price;
                    $product->qty_sale = $product->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                    if($product->qty_sale <= 0){
                        $product->qty_sale = 0;
                    }
                    if($promotion_price->product_image != ''){
                        $product->imageshow_cover = $promotion_price->product_image;
                    }
                    if(strip_tags($promotion_price->detail_product_mobile) != ''){
                        $product->details = $promotion_price->detail_product_mobile;
                    }
                    if(strip_tags($promotion_price->detail_product) != ''){
                        $product->details_web = $promotion_price->detail_product;
                    }
                    if($promotion_price->cashback != '-1'){
                        $product->cashback_percent = $promotion_price->cashback;
                    }
                    if($promotion_price->cashback_aff != '-1'){
                        $product->aff_percent = $promotion_price->cashback_aff;
                    }

                }

               $prict_product = $GetProductModel->priceproduct_arr($product);

                if($product->name_product_sale != ''){
                    $name_product = strip_tags($product->name_product_sale);
                }else{
                    $name_product = strip_tags($product->name_product);
                }

                if(isset($prict_product[3]) && $prict_product[3]!=''){
                    $saleoff = $prict_product[3];
                }else{
                    $saleoff = '';
                }

                if(!empty($product->imageshow_cover)) {  
                    $image = env('CDN_URL').'static/imgs/products/small/'.$product->imageshow_cover;
                } else {
                    $image = env('SHEEP_URL').'static/images/lazy.gif';
                }

                $product_array[] = array(
                    'name' => $name_product,
                    'saleoff' => $saleoff,
                    'url' => 'https://www.sheepola.com/product/detail/'.$product->id.'/'.url_clean($product->name_product).'.html',
                    'thumb_href' => $image,
                    'thumb_src' => $image,
                    'price' => $prict_product[0],
                    'price_old' => $prict_product[5],
                    'cashback' => floor($prict_product[2]),
                );

            }

            $cate[] = array(
                    'id' => $caterand->id,
                    'class' => 'icon-bold icon-cate-cate-'.$caterand->id.' clr-grad-1',
                    'url' => 'https://www.sheepola.com/product/c/'.$caterand->id.'/'.url_clean($catename).'.html',
                    'name' => $catename,
                    'urlall' => 'https://www.sheepola.com/product/c/'.$caterand->id.'/'.url_clean($catename).'.html',
                    'nameall' => '',
                    'parent' => $parent_array,
                    'product' => $product_array,
                    );

        }

    return json_encode($cate);

}

public function menucaterootleft(){

        if(isset($_POST['parent']) && !empty($_POST['parent'])){
                $id = $_POST['parent'];
        }else{
                $id = 0;

        }

        $category_sub = '';

        $cate = [];
        $parent_array = [];
        $all_array = '';
     
                    $CateRand = DB::table('categorys')->where('show_hidden' ,'1')->where('parentid' ,$id)->orderBy('name')->get();


        foreach ($CateRand as $v_subcate) {
         
                $catename=$v_subcate->name;

              if($v_subcate->parentid == 0){
                $v_subcate->parentid = $v_subcate->id;
            }

                $url = 'product/c/'.$v_subcate->parentid.'/'.url_clean($catename).'.html';
                $name = $catename;

                    $cate_sub_= DB::table('categorys')->where('show_hidden','1')->where('parentid' ,$v_subcate->parentid)->orderBy('name')->get();
        
                foreach ($cate_sub_ as $v_subcate_) {

                  
                    $catename_=$v_subcate_->name;

                    if($v_subcate_->parentid==0){
                        $v_subcate_->parentid=$v_subcate_->id;
                    }

                    $parent_array[] = array(
                        'url' => 'product/c/'.$v_subcate_->parentid.'/'.url_clean($catename).'/'.$v_subcate_->id.'/'.url_clean($catename_).'.html', 
                        'name' => $catename_, 
                        );
                }
                $url_all = 'product';
                $name_all = '';
    
            
            $cate[] = array(
                    'url' => $url,
                    'name' => $name,
                    'parent' => $parent_array
                );
        }
            array_push($cate,array(
                    'url' => $url_all,
                    'name' => $name_all,
                    'parent' => []
        ));

        return json_encode($cate);
    }

    function getCategoryByID($data){
        $whereData = [];

       if(!empty($data['parent'])){
            $whereData[] = ['id', $data['parent']];
        } else {
            $whereData[] = ['id', 0];
        }
     
        $cate =  DB::table('categorys')->where($whereData)->first();

        return $cate;
    }

    function getBannerCategoryByID($data){
        $whereData = [];

       if(!empty($data['parent'])){
            $whereData[] = ['id', $data['parent']];
        } else {
            $whereData[] = ['id', 0];
        }
     
        $cate =  DB::table('categorys')->where($whereData)->first();

        if(!empty($cate->image)) {
            return env('CDN_URL').'static/imgs/product/categorys/'.$cate->image;
        } else {
            return false;
        }
    }

    function getNameCategoryByID($data){
        $whereData = [];

       if(!empty($data['parent'])){
            $whereData[] = ['id', $data['parent']];
        } else {
            $whereData[] = ['id', 0];
        }
     
        $cate =  DB::table('categorys')->where($whereData)->first();

        if(!empty($cate->name)) {
            return $cate->name;
        } else {
            return '';
        }
    }

    function getCouponCategoryByID($data){
        //$coupon1 = $this->db->table('promocode')->where('AND (`date_add` <= \''.date('Y-m-d').'\' AND `date_end` >= \''.date('Y-m-d').')->order_by('date_add ASC')->find_all();

        $date_today = date('Y-m-d');

         $whereData = [];

         $whereData[] = ['status_admin', 1];
         $whereData[] = ['status', 1];
         $whereData[] = ['used', 0];
         $whereData[] = ['show_in_coupon', 1];
         $whereData[] = ['category_coupon', 2];
         $whereData[] = ['date_add', '<=' , $date_today];
         $whereData[] = ['date_end', '>=' , $date_today];

       if(!empty($data['parent'])){
            $whereData[] = ['category_product', $data['parent']];
        } else {
            $whereData[] = ['category_product', 0];
        }

        $coupon1 = DB::table('promocode')->orderBy('date_add', 'ASC')->get();

        if ($coupon1 && !empty($coupon1)) {
            $i = 0;
            foreach ($coupon1 as $v) {
            $i++;
            $check_mycoupon_keep = $this->check_mycoupon_keep($v->id, $this->db);
            $keep = $check_mycoupon_keep;
            $code_ = 'code_'.$i;
            $use_coupon = $v->category_coupon;
            $price_coupon = $v->price;
            $code_coupon = $v->code;
            $pricelow_coupon = $v->pricelow;
            $enddate_coupon = date("d/m/Y", strtotime($v->date_end));

            if ($v->category_coupon == 1) {
                $condition = 'ใช้ได้กับทุกสินค้า';
            }else{
                $condition = 'เฉพาะหมวด'.$this->cou_cate_active($v->category_product,$this->db);
            }

            if ($check_mycoupon_keep == 'depleted') {
                $card = 'card-gray';
                $action = '';
                $bg_card_top = 'background:#ddd;border:none;border-bottom:none;';
                $bg_card_top_code = 'background:#ddd;';
                $color_code = 'color: #48484C;';
                $button_name = 'หมดแล้ว';
                $temp_color = "color:#48484C;background:#ddd;border:none;";
            }else{
                $card = '';
                $action = 'onclick="copyToClipboard(`#code_'.$i.'`)"';
                $bg_card_top = 'background:#fff;';
                $color_code = 'color: ##4c2d84;';
                $bg_card_top_code = 'background:#fff;';
                $button_name = 'ดูเพิ่มเติม';
                $temp_color = "color: ##4c2d84;border: 1px solid #ccc;margin: -5px 0px 0px 0px;border-radius: 7px;";
            }

            if($code_coupon != null) {
                $coupon[] = array(
                    'keep' => $keep,
                    'id' => $code_,
                    'use_coupon' => $use_coupon,
                    'price_coupon' => $price_coupon,
                    'code_coupon' => $code_coupon,
                    'pricelow_coupon' => $pricelow_coupon,
                    'enddate_coupon' => $enddate_coupon,
                    'condition' => $condition,
                    'url' => env('SHEEP_URL').'coupon',
                    'button_text' => $button_name,
                    );
                }
            }
            
            $count_coupon = count($coupon1);
            if($count_coupon < 16) {

                //$coupon2 = $this->db->table('promocode')->where('`status_admin` = \'1\' AND `status` = \'1\' AND (`date_add` <= \''.date('Y-m-d').'\' AND `date_end` >= \''.date('Y-m-d').'\') AND used = \'0\' AND show_in_coupon = \'1\' AND `category_coupon` = \'1\'')->order_by('date_add ASC')->limit(16 - (int)$count_coupon)->find();

                $whereData2 = [];
                $whereData2[] = ['status_admin', 1];
                $whereData2[] = ['status', 1];
                $whereData2[] = ['used', 0];
                $whereData2[] = ['show_in_coupon', 1];
                $whereData2[] = ['category_coupon', 2];
                $whereData2[] = ['date_add', '<=' , $date_today];
                $whereData2[] = ['date_end', '>=' , $date_today];

                $coupon2 = DB::table('promocode')->where($whereData2)->orderBy('date_add', 'ASC')->limit(16 - (int)$count_coupon)->get();

                $i = 0;

                foreach ($coupon2 as $v) {
                $i++;
                $check_mycoupon_keep = $this->check_mycoupon_keep($v->id, $this->db);
                $keep = $check_mycoupon_keep;
                $code_ = 'code_'.$i;
                $use_coupon = $v->category_coupon;
                $price_coupon = $v->price;
                $code_coupon = $v->code;
                $pricelow_coupon = $v->pricelow;
                $enddate_coupon = date("d/m/Y", strtotime($v->date_end));
                
                if ($v->category_coupon == 1) {
                    $condition = 'ใช้ได้กับทุกสินค้า';
                }else{
                    $condition = 'เฉพาะหมวด'.$this->cou_cate_active($v->category_product,$this->db);
                }
                
                if ($check_mycoupon_keep == 'depleted') {
                    $card = 'card-gray';
                    $action = '';
                    $bg_card_top = 'background:#ddd;border:none;border-bottom:none;';
                    $bg_card_top_code = 'background:#ddd;';
                    $color_code = 'color: #48484C;';
                    $button_name = 'หมดแล้ว';
                    $temp_color = "color:#48484C;background:#ddd;border:none;";
                } else {
                    $card = '';
                    $action = 'onclick="copyToClipboard(`#code_'.$i.'`)"';
                    $bg_card_top = 'background:#fff;';
                    $color_code = 'color: ##4c2d84;';
                    $bg_card_top_code = 'background:#fff;';
                    $button_name = 'ดูเพิ่มเติม';
                    $temp_color = "color: ##4c2d84;border: 1px solid #ccc;margin: -5px 0px 0px 0px;border-radius: 7px;";
                }

                    if($code_coupon != null) {
                            $coupon[] = array(
                            'keep' => $keep,
                            'id' => $code_,
                            'use_coupon' => $use_coupon,
                            'price_coupon' => $price_coupon,
                            'code_coupon' => $code_coupon,
                            'pricelow_coupon' => $pricelow_coupon,
                            'enddate_coupon' => $enddate_coupon,
                            'condition' => $condition,
                            'url' => env('SHEEP_URL').'coupon',
                            'button_text' => $button_name,
                            );
                        }
                    }
                }
            }else{
                    //$coupon3 = $this->db->table('promocode')->where('`status_admin` = \'1\' AND `status` = \'1\' AND (`date_add` <= \''.date('Y-m-d').'\' AND `date_end` >= \''.date('Y-m-d').'\') AND used = \'0\' AND show_in_coupon = \'1\' AND `category_coupon` = \'1\'')->order_by('date_add ASC')->limit(10)->find_all();

                    $whereData3 = [];
                    $whereData3[] = ['status_admin', 1];
                    $whereData3[] = ['status', 1];
                    $whereData3[] = ['used', 0];
                    $whereData3[] = ['show_in_coupon', 1];
                    $whereData3[] = ['category_coupon', 2];
                    $whereData3[] = ['date_add', '<=' , $date_today];
                    $whereData3[] = ['date_end', '>=' , $date_today];

                    $coupon3 = DB::table('promocode')->where($whereData3)->orderBy('date_add', 'ASC')->limit(10)->get();

                    $i = 0;

                    foreach ($coupon3 as $v) {
                        $i++;
                        $check_mycoupon_keep = $this->check_mycoupon_keep($v->id, $this->db);
                        $keep = $check_mycoupon_keep;
                        $code_ = 'code_'.$i;
                        $use_coupon = $v->category_coupon;
                        $price_coupon = $v->price;
                        $code_coupon = $v->code;
                        $pricelow_coupon = $v->pricelow;
                        $enddate_coupon = date("d/m/Y", strtotime($v->date_end));
                        if ($v->category_coupon == 1) {
                            $condition = 'ใช้ได้กับทุกสินค้า';
                        }else{
                            $condition = 'เฉพาะหมวด'.$this->cou_cate_active($v->category_product,$this->db);
                        }

                        if ($check_mycoupon_keep == 'depleted') {
                            $card = 'card-gray';
                            $action = '';
                            $bg_card_top = 'background:#ddd;border:none;border-bottom:none;';
                            $bg_card_top_code = 'background:#ddd;';
                            $color_code = 'color: #48484C;';
                            $button_name = 'หมดแล้ว';
                            $temp_color = "color:#48484C;background:#ddd;border:none;";

                        }else{
                            $card = '';
                            $action = 'onclick="copyToClipboard(`#code_'.$i.'`)"';
                            $bg_card_top = 'background:#fff;';
                            $color_code = 'color: ##4c2d84;';
                            $bg_card_top_code = 'background:#fff;';
                            $button_name = 'ดูเพิ่มเติม';
                            $temp_color = "color: ##4c2d84;border: 1px solid #ccc;margin: -5px 0px 0px 0px;border-radius: 7px;";
                        }

                        if($code_coupon != null) {
                            $coupon[] = array(
                                'keep' => $keep,
                                'id' => $code_,
                                'use_coupon' => $use_coupon,
                                'price_coupon' => $price_coupon,
                                'code_coupon' => $code_coupon,
                                'pricelow_coupon' => $pricelow_coupon,
                                'enddate_coupon' => $enddate_coupon,
                                'condition' => $condition,
                                'url' => env('SHEEP_URL').'coupon',
                                'button_text' => $button_name,
                            );
                        }
                    }
                }
         if(!empty($coupon)) {
            return $coupon;
         } else {
            return false;
         }
    }

    public function check_mycoupon_keep($id,$db='') {
        //$mycoupon = $db->table('mycoupon')->where('`code_id` = \''.$id.'\' AND user_id = \''.$_SESSION['user_acc']['id'].'\'')->find_one();
        //$coupon = $db->table('promocode')->where('`id` = \''.$id.'\'')->find_one();
        $coupon = DB::table('promocode')->where('id', $id)->first();

        if ($coupon->total_limit != 0 && $coupon->remain_give_limit == 0) {
            return 'depleted'; // หมด
        }else{
            return 'succ';  // เก็บแล้ว
        }
    }

    public function cou_cate_active($id,$db='') {
        //$category = $db->table('categorys')->value('name')->where('`id` = \''.$id.'\'')->find_one();
        $category = DB::table('categorys')->select('name')->where('id', $id)->first();

        return $category->name;
    }

    public function getRootCategory(){
        $category_data = [];

        $datas = DB::select(DB::raw("SELECT c1.id, c1.name, (SELECT GROUP_CONCAT(c2.name ORDER BY c2.id SEPARATOR '&nbsp;&nbsp;&gt;') FROM categorys c2 WHERE c2.id = c1.parentid GROUP BY c1.parentid) AS pathroot, c1.parentid FROM categorys c1 WHERE parentid <> '0' ORDER BY coalesce(c1.parentid, c1.id), parentid, c1.id"));
          
        foreach ($datas as $data) {
            $category_data[] = array(
                'id' => $data->id,
                'name' => $data->name,
                'path' => $data->pathroot,
                'parentid' => $data->parentid,
                'value' => $data->id . ',' . $data->parentid,
            ); 
        }

        return $category_data;
    }

    
    function getCategorysApi(){
    
        $datas =  DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','bgcolor','color')->where('show_hidden', '1')->where('parentid', '0')->orderBy('name','asc')->get();
        return $datas;
    }


    public function getCategoryProducts($limit=''){
     
       $datas =  DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','hex','bgcolor','hex_sub','color','detail')->where('show_hidden', '1')->where('parentid', '0')->inRandomOrder()->limit($limit)->get();

       
        return $datas;
        
    }


   public function getCategorys(){

        $RouteName = Route::currentRouteName();

        $datas =  DB::table('categorys')->select('name')->get();
        return $datas;
    
    }

    public function getCouponCateActive($id=''){

        $datas =  DB::table('categorys')->select('name')->where('id',$id)->first();

        return $datas->name;
    }

     public function getCategorysRandom($show_hidden,$parentid,$limit){

        $RouteName = Route::currentRouteName();

        if($RouteName == 'ShowCategorysRandom1'){

            $datas =  DB::table('categorys')->select('name','name_en','id','hex_sub','detail')->where('show_hidden',$show_hidden)->where('parentid',$parentid)->inRandomOrder()->limit($limit)->get();


        }else if($RouteName == 'ShowCategorysRandom2'){

            $datas =  DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','hex','bgcolor','hex_sub','color','detail')->where('show_hidden',$show_hidden)->where('parentid', $parentid)->inRandomOrder()->limit($limit)->get();
        }
    
        return json_encode($datas);
    }

     public function getCategorysBottomBanner($show_hidden,$parentid,$limit){

      
        $datas =  DB::table('categorys')->select('id','name','name_short','name_en','parentid')->where('show_hidden',$show_hidden)->where('parentid', $parentid)->orderBy('name','ASC')->limit($limit)->get();

        return json_encode($datas);
    }


   public function getCategorysId($id,$ShowHidden=''){

      
        $RouteName = Route::currentRouteName();
        
        if($RouteName == 'ShowCategorysParentId'){

            $datas =  DB::table('categorys')->where('parentid', $id)->where('show_hidden', $ShowHidden)->get();

        }else if($RouteName == 'ShowCategoryId2'){

            $datas =  DB::table('categorys')->select('id','name','parentid')->where('parentid', $id)->where('show_hidden', $ShowHidden)->orderBy('name', 'asc')->get();

        }else if($RouteName == 'ShowIdName'){

             $datas =  DB::table('categorys')->select('id','name')->where('show_hidden', $ShowHidden)->orderBy('name', 'asc')->get();

        }else if($RouteName == 'ShowCategoryId'){

            $datas =  DB::table('categorys')->where('id', $id)->where('show_hidden', $ShowHidden)->get();

        }

        return $datas;
    }
 
   public function getCategorys_main_sub($categorysid){

     $datas =  DB::table('categorys')->select('id','parentid','name')->where('show_hidden','1')->where('parentid',$categorysid)->orderBy('name', 'asc')->get();
     return  $datas;

   }

    public function getMenuCategorysTopLeft(){

        $categorys_main =  DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','bgcolor')->where('show_hidden','1')->orderBy('name', 'asc')->get();

        return $categorys_main;

    } 

    public function getMenuCateLeft(){
        //$cate = [];
        $CateRand =  DB::table('categorys')->select('id','name','name_en','imageindex','imageindex_en','bgcolor')->where('show_hidden','1')->where('parentid','0')->orderBy('name','asc')->get();

        return $CateRand;
    
    }
     
}