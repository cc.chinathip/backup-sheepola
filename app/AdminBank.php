<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class AdminBank extends Model
{
    //
    public function getAdminBank(){

    	$datas =  DB::table('adminbank')->get();

    	
		return $datas;

	}

	public function getAdminBankId($id){

    	$datas =  DB::table('adminbank')->where('id',$id)->get();

    	
		return $datas;

	}


	
}
