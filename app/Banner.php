<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route


class Banner extends Model
{
    //
    public function getBannerAds($limit){

		$datas =  DB::table('banner_ads')->limit($limit)->get();

		return $datas;
	}

public function getTopPromotion($position,$limit=''){


		$datas =  DB::table('banner_ads')->where('position',$position)->where('show_hidden','1')->inRandomOrder()->limit($limit)->get();
		return json_encode($datas);

}

public function getConditionSlide(){
		$resp[0] = array(
			'link' => 'https://www.sheepola.com/help/index/10/การคืนเงินและการคืนสินค้า.html',
			'image' => 'https://www.sheepola.com/static/icon/shield-p.png',
			'name' => 'รับประกันความพอใจ',
			'subname' => 'คืนเงินภายใน 7 วัน'
		);

		$resp[1] = array(
			'link' => 'https://www.sheepola.com/content/index/5/วิธีจัดส่งสินค้า.html',
			'image' => 'https://www.sheepola.com/static/icon/free-delivery-p.png',
			'name' => 'จัดส่งฟรี',
			'subname' => 'ซื้อครบ 159 บาทขึ้นไป'
		);

		$resp[2] = array(
			'link' => 'https://www.sheepola.com/content/index/4/วิธีชำระเงิน.html',
			'image' => 'https://www.sheepola.com/static/icon/pay-p.png',
			'name' => 'เก็บเงินปลายทาง',
			'subname' => 'ซื้อครบ 299 บาทขึ้นไป'
		);

		$resp[3] = array(
			'link' => 'https://www.sheepola.com/promotion/detail/65/5/เงินคืนคืออะไร.html',
			'image' => 'https://www.sheepola.com/static/icon/c32x32.png',
			'name' => 'รับเงินคืน',
			'subname' => 'สูงสุด 50% เมื่อซื้อสินค้า'
		);

		return json_encode($resp);
}

public function getBannerTopSlide($position,$limit){


		$datas =  DB::table('banner_ads')->where('position', $position)->where('show_hidden','1')->orderby('sort_no','ASC')
		->limit($limit)->get();

		return $datas;

}

public function getBannerTopSlide39(){

		$datas =   DB::table('banner_ads')->select('banner_img','link')->where('position', '39')->where('show_hidden','1')->get();
		return $datas;

}

public function getBannerTopSlide40(){

		$datas =   DB::table('banner_ads')->select('banner_img')->where('position', '40')->where('show_hidden','1')->get();
		return $datas;
}


public function getActivitySlide(){
    
    $datas =  DB::table('activity_ads')->where('status', '1')->get();

    return $datas;

}

	public function getBannerAdsId($id){

		$RouteName = Route::currentRouteName();
		
		if($RouteName == 'ShowBannerAdsId1'){

			$datas =  DB::table('banner_ads')->where('id',$id)->where('show_hidden','1')->get();

		}else if($RouteName == 'ShowBannerAdsId2'){

			$datas =  DB::table('banner_ads')->where('id',$id)->where('show_hidden','1')->limit('1')->get();

		}else if($RouteName == 'ShowBannerAdsId3'){

			$datas =  DB::table('banner_ads')->where('id',$id)->where('show_hidden','1')->limit('5')->orderby('id','desc')->get();

		}else if($RouteName == 'ShowBannerPosition'){

			$datas =  DB::table('banner_position')->where('id',$id)->where('show_hidden','1')->orderby('id','desc')->get();

		}else if($RouteName == 'ShowBannerAdsPosition'){

			$datas =  DB::table('banner_ads')->where('position',$id)->where('show_hidden','1')->get();
		}

		return $datas;

	}


	public function getBannerLink(){

		$GetModel = new Category();
	
		$main_product['categorys'] = $GetModel->getCategorysRand();

		foreach ($main_product['categorys'] as $v) {

			$datas =   DB::select(DB::raw('SELECT url_ios,link,banner_img FROM `banner_ads` WHERE position IN (\'32\',\'33\',\'34\') AND `show_hidden` = \'1\' AND `category` = \''.$v->id.'\' ORDER BY RAND() LIMIT 3'));

	        return $datas;
	     }


	}


	public function getBannerAdsTop(){

		$datas = DB::select(DB::raw('SELECT id,url_ios,link,banner_img FROM `banner_ads` WHERE `show_hidden` = "1" AND `position` = "30" ORDER BY id DESC LIMIT 5'));

		return $datas;

	}
	
	
}
