<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class OrderProduct extends Model
{
    //
	public function getOrderProduct(){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowOrderProductAll'){

			$datas =  DB::table('order_product')->get();

		}else if($RouteName == 'ShowOrderProductNotEq567'){

			$datas =  DB::table('order_product')->where('status', '<>' ,'5')
			->where('status', '<>' ,'6')->where('status', '<>' ,'7')->get();

		}

		return $datas;
	}
	public function getOrderProductId($id ='',$value =''){


		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowOrderProductId1'){

			$datas =  DB::table('order_product')->where('user_id',$id)->where('status', '<>' ,'5')
			->where('status', '<>' ,'6')->where('status', '<>' ,'7')->get();

		}else if($RouteName == 'ShowOrderProductId2'){

			$datas =  DB::table('order_product')->where('user_id',$id)->where('status', $value)->get();


		}else if($RouteName == 'ShowOrderProductId3'){

			$datas =  DB::table('order_product')->where('status',$id)->get();


		}else if($RouteName == 'ShowOrderProductInvUser'){

			$datas =  DB::table('order_product')->where('user_id',$id)->where('oid', $value)->get();


		}else if($RouteName == 'ShowOrderProductInv'){

			$datas =  DB::table('order_product')->where('oid', $value)->get();


		}

		return $datas;
	}

	public function getCountOrderProduct($userid =''){

	
		$datas = DB::select(DB::raw('SELECT COUNT(id) total FROM `order_product` WHERE `user_id` = '.$userid.' AND `status` < \'5\' '));

		return $datas;

	}


	
	public function LoadOrder($status='', $ShowStatus='') {

		if ($status == "04") {
			
				$status ='status != 5 AND status !=6 AND status !=7';

		} else {

			if ($status == '5') {

				
				$status ="status = ".$status." ";

			} else if ($status == '6') {

				
				$status ="status = ".$status." OR status = 5 AND status_shipping = 1";

			} else {

	
				$status ="status = ".$status." ";
			}
		}

		return $status;

	}

	public function getStatusLoadOrder($userid, $status , $date='') {

		$status = $this->LoadOrder($status);

		if($date ==''){

			$datas = DB::select(DB::raw("SELECT * FROM `order_product` WHERE `user_id` = ".$userid." AND  5   ORDER BY id DESC LIMIT 0,5"));

		}else{

			$datas = DB::select(DB::raw("SELECT * FROM `order_product` WHERE `user_id` = ".$userid." AND  ".$status." date_order <= ".$date." ORDER BY id DESC  LIMIT 0,5"));
		}

		return $datas;
		
	}

	public function getPendingCash($userid,$lastdate=''){


		$datas =  DB::table('order_product')->where('user_id', $userid)->where('status', '5')->where('status_account', '1')->where('status', '<>' ,'7')->orderBy('date_order','desc')->get();

		if($lastdate == 'true'){

			return $datas[0]->date_order;

		}else{
			$pendingcashtotal = 0;

			if($datas){
				foreach ($datas as $key => $value) {

					$pendingcash = json_decode($value->json_order,true);
					if($pendingcash['total_price_cash_back'] != ''){
						$pendingcashtotal += floatval(str_replace(',','',$pendingcash['total_price_cash_back']));
					}
				}
			}
			return number_format($pendingcashtotal,2);
		}
	}


	




}
