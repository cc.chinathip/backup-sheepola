<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class MyCoupon extends Model
{
    //

    public function getMyCoupon(){

			$datas =  DB::table('mycoupon')->get();
			return $datas;

	}


    public function getMyCouponId($userid,$date_start,$date_end){

    	$RouteName = Route::currentRouteName();

			$datas =  DB::table('mycoupon')->where('user_id',$userid)->where('status','0')->where('date_start','<=',$date_start)
			->where('date_end','>=',$date_end)->get();
			return $datas;

	}

	public function getMyCouponId2($userid,$codeid){

			$RouteName = Route::currentRouteName();

			if($RouteName == 'MyCouponId2Where1'){

				$datas =  DB::table('mycoupon')->where('user_id',$userid)->where('code_id','<=',$codeid)->where('status','0')
			->limit('1')->get();

			}else if($RouteName == 'MyCouponId2Where2'){

				$datas =  DB::table('mycoupon')->where('user_id',$userid)->where('code_id','<=',$codeid)
				->get();
			}
		
			return $datas;

	}

	public function CountMyCouponId($userid){

    		
    		$date = date('Y-m-d');

			$datas = DB::select(DB::raw('SELECT COUNT(id) total FROM `mycoupon` WHERE `user_id` = \''.$userid.'\' AND `status` = \'0\' AND `date_start` <= \''.$date.'\' AND `date_end` >= \''.$date.'\''));

			return $datas;



	}

	

	

	
}
