<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Business extends Model
{
    //

    public function getBusinessBook(){

        $datas =  DB::table('business_book')->get();
        return $datas;
    }

    public function getBusinessBookId($id){

    	$RouteName = Route::currentRouteName();
		
		if($RouteName == 'ShowBusinessBookId'){

       		 $datas =  DB::table('business_book')->where('id_user',$id)->get();

       	}else if($RouteName == 'SelectStatusWhereUserId'){

       		$datas =  DB::table('business_book')->select('status')->where('id_user',$id)->get();



       	}
        return $datas;
    }

   
    
}
