<?php
namespace App\Http\Controllers;
use App\Promotion;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PromotionController extends Controller
{
    //
    public function ShowPromotion(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotion();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionCateStatus(Request $request){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionCateStatus($request->route('status'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionid(Request $request){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionid($request->route('id'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowPromotionSlide(){
        if( Cache::has( 'ShowPromotionSlide' ) ) {
            return Cache::get( 'ShowPromotionSlide' );
        }else{

            $GetModel = new Promotion();
            $data = $GetModel->getPromotionSlide();
            if (!$data) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, getPromotionSlide  cannot be found'
                ], 400);
            }
            Cache::put( 'ShowPromotionSlide', $data, 90 );
            return $data;

        }
    }

    public function ShowPromotionTemplate(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionTemplate();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getPromotionTemplate  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function GetCategorysCopon($datas) {

        $GetCategorysModel = new Category();
        foreach ($datas as $v) {

                    if ($v->category_coupon == 1) {
                        $condition = 'ใช้ได้กับทุกสินค้า';
                    }else{
                        $condition = 'เฉพาะหมวด'.$GetCategorysModel->getCouponCateActive($v->category_product);
                    }
                   
                    $resp['coupon'][] = array(
                        'id' => $v->id,
                        'code' => $v->code,
                        'date_end' => date_format(date_create($v->date_end), 'd/m/Y'),
                        'condition' => $condition,
                        'price' => $v->price,
                        'pricelow' => $v->pricelow,
                    );

                
      }
             return json_encode($resp);
            

    }
    public function ShowCoupon(){

        if(isset($_POST['limit'])){

            $limit = $_POST['limit'];

        }else{

            $limit = '5';
        }

        if( Cache::has( 'ShowCoupon'.$limit ) ) {
            return Cache::get( 'ShowCoupon'.$limit );
        }else{

            $GetModel = new Promotion();
            $datas = $GetModel->getCoupon($limit);
            
            if (count($datas) > 0) {
                $datareturn = $this->GetCategorysCopon($datas);
                Cache::put( 'ShowCoupon'.$limit, $datareturn, 90 );
                return $datareturn;
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, getBestsellerProduct  cannot be found'
                ], 400);
            }

            return $datas;

        }
    }

    public function ShowPromotionCodeDate(Request $request){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionCodeDate($request->route('datenow'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getPromotionCodeDate  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionNotification(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionNotification();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionImageFull(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionImageFull();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

     public function ShowPromotionEnd(){

        if(isset($_POST['status']) AND isset($_POST['status_admin']) AND isset($_POST['ProductType']) AND isset($_POST['dateEnd'])){

            $status = $_POST['status'];
            $status_admin = $_POST['status_admin'];
            $ProductType = $_POST['ProductType'];
            $dateEnd = $_POST['dateEnd'];

        }else if(!isset($_POST['status']) AND !isset($_POST['status_admin']) AND !isset($_POST['ProductType']) AND isset($_POST['dateEnd'])){

            $status = '1';
            $status_admin = '1';
            $ProductType = '5';
            $dateEnd = $_POST['dateEnd'];
        }

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionEnd($status,$status_admin,$ProductType,$dateEnd);
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

  
    public function ShowPromotionCreateEndCat(Request $request){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionCreateEndCat($request->route('dateCreate'),$request->route('dateEnd'),$request->route('PromotionCat'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

     public function ShowImgProductTypeLinkApp(){

        $GetModel = new Promotion();
        $data = $GetModel->getImgProductTypeLinkApp();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function CountOrderBuyPromotion(Request $request){

        $GetModel = new Promotion();
        $data = $GetModel->getCountOrderBuyPromotion($request->route('promoid'),$request->route('pid'),$request->route('qtysale'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, promotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowCheckPromotion(Request $requests){

        $GetModel = new Promotion();
        $data = $GetModel->getCheckPromotion($request->route('promoid'),$request->route('id'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getCheckPromotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionFlashdeal(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionFlashdeal();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getPromotionFlashdeal  cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowPromotionFlashStartEndByOne(){

        $GetModel = new Promotion();
        $data = $GetModel->getPromotionFlashStartEndByOne();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getPromotionFlashdeal  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPromotionFlashEnd(Request $request){

        if(!empty($request->route('limit'))){
            $limit = $request->route('limit');
        }else{
            $limit = 30;
        }

        $GetPromotionModel = new Promotion();

        $data = $GetPromotionModel->getPromotionFlashEnd($limit);

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getPromotionFlashEnd  cannot be found'
            ], 400);
        } else {
            return response()->json($data);
        }
    }


   


}
