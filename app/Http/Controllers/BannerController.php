<?php

namespace App\Http\Controllers;
use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
class BannerController extends Controller
{
    
    public function GetDataBannerTopSlide($data){

        foreach ($data as $v) {
            if(ismobile()){
                //$filename = BASE_CDN.'static/imgs/promotion/banner/mobile/'.$v->banner_img;

                

                $filename = env('CDN_URL').'static/imgs/promotion/banner/mobile'.$v->banner_img;
                if (file_exists($filename)) {
                    //$banner_img = BASE_CDN.'static/imgs/promotion/banner/mobile/'.$v->banner_img;
                    $banner_img = env('CDN_URL').'static/imgs/promotion/banner/mobile'.$v->banner_img;
                }else{
                    //$banner_img = BASE_CDN.'static/imgs/promotion/banner/'.$v->banner_img;
                    $banner_img = env('CDN_URL').'static/imgs/promotion/banner/'.$v->banner_img;
                }
            }else{
                //$banner_img = BASE_CDN.'static/imgs/promotion/banner/'.$v->banner_img;
                $banner_img = env('CDN_URL').'static/imgs/promotion/banner/'. $v->banner_img;
            }

            $resp['banner_slide'][] = array(
                'banner_img' => $banner_img,
                'link' => $v->link
            );

        }
        $GetModelBanner = new Banner();
        $resp['banner_39'] = $GetModelBanner->getBannerTopSlide39();
        $resp['banner_40'] = $GetModelBanner->getBannerTopSlide40();
     

       return json_encode($resp);
    }

     public function ShowBannerTopSlide(){

        if(isset($_POST['position']) AND isset($_POST['limit'])){
            $position = $_POST['position'];
            $limit = $_POST['limit'];
        } else {
           $position = '38';
           $limit = '8';

        }

        if( Cache::has( 'ShowBannerTopSlide'.$position.$limit ) ) {
            return Cache::get( 'ShowBannerTopSlide'.$position.$limit );
        }else{
            $GetModel = new Banner();

            $data = $GetModel->getBannerTopSlide($position,$limit);

            if (count($data) > 0) {
                $datareturn = $this->GetDataBannerTopSlide($data);
                Cache::put( 'ShowBannerTopSlide'.$position.$limit, $datareturn, 90 );
                return $datareturn;

            }else{

                return response()->json([
                    'success' => false,
                    'message' => 'getBannerTopSlide user cannot be found'
                ], 400);
            }
        }

       
    }

    public function ShowConditionSlide(){

        if( Cache::has( 'ShowConditionSlide' ) ) {
            return Cache::get( 'ShowConditionSlide' );
        }else{
            $GetModel = new Banner();

            $data = $GetModel->getConditionSlide();

            if (!$data) {
                return response()->json([
                    'success' => false,
                    'message' => 'getBannerLink user cannot be found'
                ], 400);
            }
            Cache::put( 'ShowConditionSlide', $data, 90 );
            return $data;
        }
    }

    public function ShowBannerLink(){
        $GetModel = new Banner();

        $data = $GetModel->getBannerLink();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getBannerLink user cannot be found'
            ], 400);
        }

        return $data;
    }

    public function GetDataActivitySlide($data){

    
        foreach ($data as $v) {

            if (empty($v->sub_name)) {
                $sub_name = 'dis-promo';
                $sub_name_app = 'none';
            }else{
                $sub_name = '';
                $sub_name_app = '';
            }

            $resp[] = array(
                'id' => $v->id,
                'link' => $v->link,
                'link_app' => $v->link_app,
                'name' => $v->name,
                'sub_name' => '<span class="sub-promo '.$sub_name.'">'.$v->sub_name.'</span>',
                'sub_name_app' => $sub_name_app,
                'image' => env('CDN_URL').'static/imgs/activityads/'.$v->image
            );
        }

        return json_encode($resp);
    }

    public function ShowActivitySlide(){
     
        if( Cache::has( 'ShowActivitySlide' ) ) {
            return Cache::get( 'ShowActivitySlide' );
        }else{

            $GetModel = new Banner();
            $data = $GetModel->getActivitySlide();

            if (count($data) > 0) {
                $GetDataActivitySlide = $this->GetDataActivitySlide($data);
                Cache::put( 'ShowActivitySlide', $GetDataActivitySlide, 190 );
                return $GetDataActivitySlide;
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, getActivitySlide  cannot be found'
                ], 400);
            }

            return $data;
        }
    }

    public function ShowBannerAdsTop(){
        $GetModel = new Banner();

        $data = $GetModel->getBannerAdsTop();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getBannerAdsTop user cannot be found'
            ], 400);
        }

        return $data;
    }



    public function ShowBannerAds(){

        if(!empty($_POST['limit'])){

            $limit = $_POST['limit'];
            
        }else{

            $limit = '5';
             
        }

        $GetModel = new Banner();

        $data = $GetModel->getBannerAds($limit);

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'contact user cannot be found'
            ], 400);
        }

        return $data;
    }

     public function ShowTopPromotion(){

        if(!empty($_POST['limit']) AND !empty($_POST['position'])){

            $position = $_POST['position'];
            $limit = $_POST['limit'];
            

        }else{

             $position = '31';
             $limit = '8';
             
        }

        if( Cache::has( 'ShowMystyleProducts'.$position.$limit ) ) {
            return Cache::get( 'ShowMystyleProducts'.$position.$limit );
        }else{

            $GetModel = new Banner();

            $data = $GetModel->getTopPromotion($position,$limit);

            if (!$data) {
                return response()->json([
                    'success' => false,
                    'message' => 'contact user cannot be found'
                ], 400);
            }
            Cache::put( 'ShowMystyleProducts'.$position.$limit, $data, 90 );
            return $data;
        }
    }

    public function ShowBannerAdsId(Request $request)
    {
        $GetModel = new Banner();

        $data = $GetModel->getBannerAdsId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'contact user cannot be found'
            ], 400);
        }

        return $data;
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
