<?php

namespace App\Http\Controllers;
use App\Product;
use App\MainApp;
use App\Promotion;
use App\Category;
use App\MyStyle;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Facades\Cache;
class MainAppController extends Controller
{
    //protected $user;
    /*public function __construct()
    {
        $this->user() = JWTAuth::parseToken()->authenticate();
    }*/
    protected function user() {
        return  JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        return $this->user()
        ->products()
        ->get(['id' , 'name', 'price', 'quantity'])
        ->toArray();

    }

    public function ShowProductNew(){

        if( Cache::has( 'ShowProductNew' ) ) {
            return Cache::get( 'ShowProductNew' );
        }else{

            $GetModel = new MainApp();
            $GetMoelMyStyle = new MyStyle();
            $GetPromotionModel = new Promotion();
            $GetCategoryModel = new Category();
            $GetProductModel = new Product();

            $resp = array();

            // $data = $GetModel->getProductNew(10);

            // if (count($data) > 0) {
            //     return response()->json($this->GetProductPrice($data));
            // } else {
            //     return response()->json([
            //         'success' => false,
            //         'message' => 'Sorry, getBestsellerProduct  cannot be found'
            //     ], 400);
            // }
            // 
            // $Categorys = $GetModel->getCategorysMainApp(2,0);

            // $resp['CategorysProduct'] = response()->json($Categorys);

            // foreach ($Categorys as $c) {
            //     $resp['ProductsCategory'][$c->id] = response()->json($GetModel->getProductCategoryMainApp(6,$c->id));
            //     $resp['BannerAdsCategory'][$c->id] = response()->json($GetModel->getBannerAdsCategory(3,$c->id));
            // }
            $SettingApp = $GetModel->getSettingApp();

            $resp['SettingApp'] = response()->json($SettingApp);


            $ProductNew = $GetModel->getProductNew(10);

            if (count($ProductNew) > 0) {
                $resp['ProductNew'] = response()->json($this->GetProductPrice($ProductNew));

            } else {
                $resp['ProductNew'] = response()->json([
                    'success' => false,
                    'message' => 'Sorry, getBestsellerProduct  cannot be found'
                ], 400);
            }

            $ProductRecommend = $GetModel->getProductRecommend(20);

            if (count($ProductRecommend) > 0) {
                $resp['ProductRecommend'] = response()->json($this->GetProductPrice($ProductRecommend));

            } else {
                $resp['ProductRecommend'] = response()->json([
                    'success' => false,
                    'message' => 'Sorry, getBestsellerProduct  cannot be found'
                ], 400);
            }

            if ($SettingApp[0]->app_index_flashdeals == 1) {
                $ProductFlash = $GetPromotionModel->getPromotionFlash();

                $resp['PromotionFlash'] = response()->json($ProductFlash);

                if (count($ProductFlash) > 0) {
                    $resp['ProductFlash'] = $this->GetProductPromotionFlash($ProductFlash,10);
                } else {
                    $resp['ProductFlash'] = response()->json([
                        'success' => false,
                        'message' => 'Sorry, GetProductPromotionFlash  cannot be found'
                    ], 400);
                }
            }
        
            if ($SettingApp[0]->app_index_mystyle == 1) {
                $ProductMyStyle = $GetMoelMyStyle->getMystyleProducts(6);

                if (!$ProductMyStyle) {
                    $resp['ProductMyStyle'] = response()->json([
                        'success' => false,
                        'message' => 'Sorry, mystyle  cannot be found'
                    ], 400);
                }else{
                    $resp['ProductMyStyle'] = $ProductMyStyle;
                }
            }

            Cache::put( 'ShowProductNew', $resp, 190 );
            return $resp;
        }
    }

    public function ShowProductRecommend(){

        $GetModel = new MainApp();

        $data = $GetModel->getProductRecommend(20);

        if (count($data) > 0) {
            return response()->json($this->GetProductPrice($data));

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getBestsellerProduct  cannot be found'
            ], 400);
        }
    }

    public function GetProductPromotionFlash($promotion,$limit) {

        //return $limit;

        $GetProductModel = new Product();
        $GetPromotionModel = new Promotion();

        if(isset($promotion)){
            $productflash = $GetProductModel->getProductFlashByPromtionId($promotion[0]->PromtionId,$limit);
        }
        
        //return $productflash;
        foreach ($productflash as $vp) {
        $date = date('Y-m-d h:i:s');
            
       
            if ($promotion[0]->PromotionStart.' '.$promotion[0]->time_start <= $date && $promotion[0]->PromotionEnd.' '.$promotion[0]->time_end >= $date){
              
                if($vp->promotion_id != '0'){
                    
                    $promotion_price = $GetPromotionModel->getCheckPromotion($vp->promotion_id,$vp->id);

                    if(isset($promotion_price)){

                        if ($promotion_price->fake_price > 0) {
                            $vp->snat_price = $promotion_price->fake_price;
                        }
                        $vp->nat_price = $promotion_price->pr_price;
                        $vp->qty_sale = $vp->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;

                        if($vp->qty_sale <= 0){
                            $vp->qty_sale = 0;
                        }

                 
                        $count_order =  $GetPromotionModel->getCountOrderBuyPromotion($vp->promotion_id,$promotion_price->qty_limit,$vp->id);

                        if (!$count_order) {
                            $vp->pro_mod = false;
                        }else{
                            $vp->pro_mod = true;
                        }
                        
                        
                        if($promotion_price->product_image != ''){
                            $vp->imageshow_cover = $promotion_price->product_image;
                        }
                        if(strip_tags($promotion_price->detail_product_mobile) != ''){
                            $vp->details = $promotion_price->detail_product_mobile;
                        }
                        if(strip_tags($promotion_price->detail_product) != ''){
                            $vp->details_web = $promotion_price->detail_product;
                        }
                        if($promotion_price->cashback != '-1'){
                            $vp->cashback_percent = $promotion_price->cashback;
                        }
                        if($promotion_price->cashback_aff != '-1'){
                            $vp->aff_percent = $promotion_price->cashback_aff;
                        }
                    }
                }else{
                        $promotion_price = false;
                }
            }

            $prict_product = $GetProductModel->priceproduct_arr($vp);

                   
            if(!empty($vp->imageshow_cover)) {  
                $image = 'https://sheepola.sgp1.cdn.digitaloceanspaces.com/static/imgs/product/thumbnail/'.$vp->imageshow_cover;
            } else {
                $image = 'https://www.sheepola.com/static/images/lazy.gif';
            }

            if(!empty($vp->name_product_sale)){
                $product_sale = $vp->name_product_sale;
            } else {
                $product_sale = $vp->name_product;
            }

            $resp[] = array(
                'id' => $vp->id,
                'url' => 'product/detail/'.$vp->id.'/'.url_clean($vp->name_product).'.html',
                'image' => $image,
                'product_sale' => $product_sale,
                'price' => $prict_product,
                'pro_mod' => $vp->pro_mod
            );

        }

        return response()->json($resp);
    } 

    public function ShowProductFlash(){
       
         if(!empty($_POST['limit'])){

            $limit = $_POST['limit'];

        }else{

            $limit = 5;
        }

        $GetPromotionModel = new Promotion();
       
        $data = $GetPromotionModel->getPromotionFlash();

        if (count($data) > 0) {
            return $this->GetProductPromotionFlash($data,$limit);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, GetProductPromotionFlash  cannot be found'
            ], 400);
        }

       
    }

    public function GetProductPrice($products) {

        $GetModel = new Product();
        $GetPromotionModel = new Promotion();

        $no = 1;

        foreach ($products as $vp) {

            if(!empty($vp->pid)) {
                $product = $GetModel->getProductId($vp->pid);
            } else {
                $product = $GetModel->getProductId($vp->id);
            }

            $promotion_price = $GetPromotionModel->getCheckPromotion($product->promotion_id, $product->id);

            if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                $product->nat_price = $promotion_price->pr_price;

                $product->qty_sale = $product->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                if($product->qty_sale <= 0){
                    $product->qty_sale = 0;
                }

                if($promotion_price->product_image != ''){
                    $product->imageshow_cover = $promotion_price->product_image;
                }

                if(strip_tags($promotion_price->detail_product_mobile) != ''){
                    $product->details = $promotion_price->detail_product_mobile;
                }

                if(strip_tags($promotion_price->detail_product) != ''){
                    $product->details_web = $promotion_price->detail_product;
                }

                if($promotion_price->cashback != '-1'){
                    $product->cashback_percent = $promotion_price->cashback;
                }

                if($promotion_price->cashback_aff != '-1'){
                    $product->aff_percent = $promotion_price->cashback_aff;
                }
            }
    
            $prict_product = $GetModel->priceproduct_arr($product);


            if(!empty($prict_product[3])){ 
                $saleoff = $prict_product[3];
            } else {
                $saleoff = '';
            }
                   
            if(!empty($product->imageshow_cover)) {  
                $image = 'https://sheepola.sgp1.cdn.digitaloceanspaces.com/static/imgs/product/thumbnail/'.$product->imageshow_cover;
            } else {
                $image = 'https://www.sheepola.com/static/images/lazy.gif';
            }

            if(!empty($product->name_product_sale)){
                $product_sale = $product->name_product_sale;
            } else {
                $product_sale = $product->name_product;
            }

            $product_data[] = array(
                'id' => $product->id,
                'saleoff' => $saleoff,
                'url' => 'product/detail/'.$product->id.'/'.url_clean($product->name_product).'.html',
                'image' => $image,
                'no' => $no,
                'product_sale' => $product_sale,
                'price' => $prict_product,
                'price_old' => $prict_product,
                'cashback' => floor($prict_product[2]),
            );

            $no++;
        }

        return response()->json($product_data);
    }
}