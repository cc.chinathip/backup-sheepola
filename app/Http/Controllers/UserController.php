<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
     public function ShowUser(){

        $GetModel = new User();
        $data = $GetModel->getUser();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowUserId(Request $request){

        $GetModel = new User();
        $data = $GetModel->getUserId($request->route('userid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowUserFacbookId(Request $request){

        $GetModel = new User();
        $data = $GetModel->getUserFacbookId($request->route('responseid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowUserLineId(Request $request){

        $GetModel = new User();
        $data = $GetModel->getUserLineId($request->route('responseid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowUserRecommendlist(Request $request){

        $GetModel = new User();
        $data = $GetModel->getUserRecommendlist($request->route('userref'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
      public function ShowUserRecommendFirstBuy(Request $request){

        $GetModel = new User();
        $data = $GetModel->getUserRecommend($request->route('value1'),$request->route('value2'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function CheckForgotEmail(Request $request){

        $GetModel = new User();
        $data = $GetModel->getValueEmail($request->route('value1'),$request->route('value2'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }
        return $data;
    }

    public function CheckEmail(Request $request){

        $GetModel = new User();
        $data = $GetModel->getValueEmail($request->route('value1'),$request->route('value2'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user  cannot be found'
            ], 400);
        }
        return $data;
    }


    
    
}
