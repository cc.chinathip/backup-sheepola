<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ShowSettingApp()
    {
        $GetModel = new Setting();

        $data = $GetModel->getSettingApp();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'setting cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowRegisterBoxEvent(Request $request)
    {
        $GetModel = new Setting();

        $data = $GetModel->getRegisterBoxEvent($request->route('userid'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getRegisterBoxEvent cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowConfirmAppDownload()
    {
        $GetModel = new Setting();

        $data = $GetModel->getConfirmAppDownload();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'setting cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowConfirmAppDownloadId(Request $request)
    {
        $GetModel = new Setting();

        $data = $GetModel->getConfirmAppDownloadId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'setting cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowAppVersion()
    {
        $GetModel = new Setting();

        $data = $GetModel->getAppVersion();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'setting cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
