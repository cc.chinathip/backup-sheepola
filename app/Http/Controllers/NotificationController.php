<?php

namespace App\Http\Controllers;
use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //

   	public function ShowNotification(){

        $GetMoel = new Notification();
        $data = $GetMoel->getNotification();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowNotificationWithPromotion(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getNotificationWithPromotion($request->route('id'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getNotificationWithPromotion  cannot be found'
            ], 400);
        }

        return $data;
    }

    
    public function ShowNotificationId1(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getNotificationId1($request->route('promotionid'),$request->route('userid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowNotificationId2(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getNotificationId2($request->route('promotionid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowNoticeNewUserId(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getNoticeNewUserId($request->route('userid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }



    public function ShowtNoticeNew(){

        $GetMoel = new Notification();
        $data = $GetMoel->getNoticeNew();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowNoticeNewUseridId(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getNoticeNewUseridId($request->route('userid'),$request->route('id'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }


     public function ShowReadNotice(Request $request){

        $GetMoel = new Notification();
        $data = $GetMoel->getReadNotice($request->route('id'),$request->route('userid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        

        return $data;
    }

    /*public function ShowReadNotice(){

        $GetMoel = new Notification();
        $data = $GetMoel->getReadNotice();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, notification  cannot be found'
            ], 400);
        }

        return $data;
    }*/




    
}
