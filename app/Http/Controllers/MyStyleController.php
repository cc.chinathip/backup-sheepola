<?php

namespace App\Http\Controllers;

use App\MyStyle;
use App\Product;
use App\Promotion;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class MyStyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    public function ShowMystyleProducts(){

        if(!empty($_POST['limit'])){

            $limit = $_POST['limit'];

        }else{

            $limit = 6;
        }

        if( Cache::has( 'ShowMystyleProducts'.$limit ) ) {
            return Cache::get( 'ShowMystyleProducts'.$limit );
        }else{
            $GetModel = new MyStyle();
            $data = $GetModel->getMystyleProducts($limit);
            if (!$data) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, mystyle  cannot be found'
                ], 400);
            }
            Cache::put( 'ShowMystyleProducts'.$limit, $data, 90 );
            return $data;
        }
    }

    public function GetDataProductMystyleById($data){

        $GetProductModel = new Product();
        $GetPromotionModel = new Promotion();

        if($data){
           foreach ($data as $productalls) {

               if($productalls->promotion_id != '0'){
                $promotion_price = $GetPromotionModel->getCheckPromotion($productalls->promotion_id,$productalls->id);
                if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                    $productalls->nat_price = $promotion_price->pr_price;
                    $productalls->qty_sale = $productalls->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                    if($productalls->qty_sale <= 0){
                        $productalls->qty_sale = 0;
                    }

                    if($promotion_price->product_image != ''){
                        $productalls->imageshow_cover = $promotion_price->product_image;
                    }

                    if(strip_tags($promotion_price->detail_product_mobile) != ''){
                        $productalls->details = $promotion_price->detail_product_mobile;
                    }

                    if(strip_tags($promotion_price->detail_product) != ''){
                        $productalls->details_web = $promotion_price->detail_product;
                    }

                    if($promotion_price->cashback != '-1'){
                        $productalls->cashback_percent = $promotion_price->cashback;
                    }

                    if($promotion_price->cashback_aff != '-1'){
                        $productalls->aff_percent = $promotion_price->cashback_aff;
                    }
                }
            }else{
                $promotion_price = false;
            }

            $prict_product = $GetProductModel->priceproduct_arr($productalls);


            if(!empty($productalls->imageshow_cover)) {  
                $image = env('CDN_URL').'static/imgs/products/medium/'.$productalls->imageshow_cover;
            } else {
                $image = env('SHEEP_URL').'static/images/lazy.gif';
            }



            if(!empty($productalls->name_product_sale)){
                $product_sale = $productalls->name_product_sale;

            } else {
                $product_sale = $productalls->name_product;
            }

            if($prict_product[0] == $prict_product[5]) {
                $price_sorting = $prict_product[5];
            } else {
                $price_sorting = $prict_product[0];
            }

            if(isset($prict_product[3]) && $prict_product[3] !=''){ 
                $saleoff = $prict_product[3];
                $sale_sorting = $prict_product[3];
            } else {
                $saleoff = '';
                $sale_sorting = 0;
            }

             $productmystyle_data[] = array(
                'id' => $productalls->id,
                'saleoff' => $saleoff,
                'url' => env('SHEEP_URL').'product/detail/'.$productalls->id.'/'.url_clean($productalls->name_product).'.html',
                'image' => $image,
                'product_sale' => $product_sale,
                'price' => $prict_product[0],
                'price_old' => $prict_product[5],
                'cashback' => floor($prict_product[2]),
                'price_sorting' => $price_sorting,
                'sale_sorting' => $sale_sorting,
                'desc' => str_replace('&nbsp;','',trim(substr_utf8(strip_tags($productalls->details),0,200))) . '...',
            );

            

        }
        return response()->json($productmystyle_data);

    }

    

}
public function ShowProductMystyleById(){

    //$data = $this->getProductMystyleAll();

    if(!empty($_POST['mystyleid']) && !empty($_POST['limit'])){

        $mystyleid = $_POST['mystyleid'];
        $limit = $_POST['limit'];

    }else{

        $mystyleid = $_POST['mystyleid'];
        $limit = 20;
    }

    $GetModel = new MyStyle();
    $data = $GetModel->getProductMystyleById($mystyleid);

    if (count($data)>0) {

     return $this->GetDataProductMystyleById($data);
 }else{
    return response()->json([
        'success' => false,
        'message' => 'Sorry, getProductMystyleById  cannot be found'
    ], 400);
}


}

public function ShowProductMystyleByGroup(){


    if(!empty($_POST['mystyleid']) && !empty($_POST['limit'])){

        $mystyleid = $_POST['mystyleid'];
        $limit = $_POST['limit'];

    }else{

        $mystyleid = $_POST['mystyleid'];
        $limit = 20;
    }

    
    $GetModel = new MyStyle();
    $data = $GetModel->getProductMystyleByGroup($mystyleid,$limit);
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, getProductMystyleByGroup  cannot be found'
        ], 400);
    }

    return $data;
}


public function ShowMyStyleRandom(){

    $GetModel = new MyStyle();
    $data = $GetModel->getMyStyleRandom();
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, mystyle  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowCountProductsByMystyle(){


    if(!empty($_POST['mystyleid'])){

        $mystyleid = $_POST['mystyleid'];
        //$limit = $_POST['limit'];

    }


    $GetModel = new MyStyle();
    $data = $GetModel->CountProductsByMystyle($mystyleid);
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, CountProductsByMystyle  cannot be found'
        ], 400);
    }

    return $data;
}


public function GetDataMystyle($data_MyStyle,$limit){

    $GetModel = new MyStyle();
  
    foreach ($data_MyStyle as $data_MyStyleId_row) {

        if (!empty($data_MyStyleId_row->stylename)) {

                   $stylename = $data_MyStyleId_row->stylename;
        }else{
                   $stylename = '';
        }

         $product_mystyle = $GetModel->getProductMystyleByGroup($data_MyStyleId_row->id,$limit);

        foreach ($product_mystyle as  $key =>$product_mystyle_row) {

                     $image[$key] = env('CDN_URL').'static/imgs/product/thumbnail/'.$product_mystyle_row->imageshow_cover;
                     $groupMystyle = ($image);

                     $id[$key] = $product_mystyle_row->id;
                     $groupMystyle2 = implode(',', $id);

                     //$groupMystyle2 = rsort($groupMystyle2);

        }
         //print_r($groupMystyle2);

         $CountProductsMystyle = $GetModel->CountProductsByMystyle($data_MyStyleId_row->id);
    
            $product_mystyle_data[] = array(

                'stylename' => $stylename,
                'url' => env('SHEEP_URL').'product/mystyle/'.$data_MyStyleId_row->id.'/'.($stylename).'.html',
                'CountProductsMystyle' => $CountProductsMystyle,
                'image' => $groupMystyle[0],
                'image2' => $groupMystyle[1],
                'image3' => $groupMystyle[2],
                'image4' => $groupMystyle[3],
                'id' => $groupMystyle2,
                

                
            );

    }
    
    return response()->json($product_mystyle_data);

}

public function ShowMyStyle(){

    $GetModel = new MyStyle();
    $data = $GetModel->getMyStyle();
       

    if(!empty($_POST['limit'])){

       
        $limit = $_POST['limit'];

    }else{

        
        $limit = 4;
    }

    $data = $GetModel->getMyStyle();

    //return  $data;

    if (count($data)>0) {

        return $this->GetDataMystyle($data,$limit);
     
    }else{
        return response()->json([
            'success' => false,
            'message' => 'Sorry, getMyStyle  cannot be found'
        ], 400);
    }


}

public function ShowMyStyle2(){

    $GetModel = new MyStyle();
    $data = $GetModel->getMyStyle();
       
    
    if (count($data)>0) {

        return $data = $GetModel->getMyStyle();
     
    }else{
        return response()->json([
            'success' => false,
            'message' => 'Sorry, ShowMyStyle2  cannot be found'
        ], 400);
    }


}








public function ShowMyStyleId(Request $request){

    $GetModel = new MyStyle();
    $data = $GetModel->getMyStyleId($request->route('id'));

    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, mystyle  cannot be found'
        ], 400);
    } 
    return response()->json($data);
    
}

public function ShowMyStyleTotal(){

    $GetModel = new MyStyle();
    $data = $GetModel->getMyStyleTotal();
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, mystyle  cannot be found'
        ], 400);
    }

    return $data;
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MyStyle  $myStyle
     * @return \Illuminate\Http\Response
     */
    public function show(MyStyle $myStyle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MyStyle  $myStyle
     * @return \Illuminate\Http\Response
     */
    public function edit(MyStyle $myStyle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MyStyle  $myStyle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MyStyle $myStyle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MyStyle  $myStyle
     * @return \Illuminate\Http\Response
     */
    public function destroy(MyStyle $myStyle)
    {
        //
    }
}
