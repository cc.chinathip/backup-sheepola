<?php

namespace App\Http\Controllers;
use App\Product;
use App\Promotion;
use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Facades\Cache;
class ProductController extends Controller
{
    //protected $user;
    /*public function __construct()
    {
        $this->user() = JWTAuth::parseToken()->authenticate();
    }*/
    protected function user() {
        return  JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        return $this->user()
        ->products()
        ->get(['id' , 'name', 'price', 'quantity'])
        ->toArray();

    }
    public function substr_utf8( $str, $start_p , $len_p){
        return preg_replace( '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start_p.'}'.
            '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len_p.'}).*#s',
            '$1' , $str );
    }
    public function show($id)
    {
        $product = $this->user()->products()->find($id);
        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product with id ' . $id . ' cannot be found'
            ], 400);
        }
        return $product;
    }

    public function ShowProduct(){

        $GetModel = new Product();
        $GetPromotionModel = new Promotion();

        $data = $GetModel->getProduct($_POST);

        if (count($data) > 0) {

        foreach ($data as $vo) {

            $promotion_price = $GetPromotionModel->getCheckPromotion($vo->promotion_id, $vo->id);

            if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                $vo->nat_price = $promotion_price->pr_price;

                $vo->qty_sale = $vo->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                if($vo->qty_sale <= 0){
                    $vo->qty_sale = 0;
                }

                if($promotion_price->product_image != ''){
                    $vo->imageshow_cover = $promotion_price->product_image;
                }

                if(strip_tags($promotion_price->detail_product_mobile) != ''){
                    $vo->details = $promotion_price->detail_product_mobile;
                }

                if(strip_tags($promotion_price->detail_product) != ''){
                    $vo->details_web = $promotion_price->detail_product;
                }

                if($promotion_price->cashback != '-1'){
                    $vo->cashback_percent = $promotion_price->cashback;
                }

                if($promotion_price->cashback_aff != '-1'){
                    $vo->aff_percent = $promotion_price->cashback_aff;
                }
            }

            $prict_product = $GetModel->priceproduct_arr($vo);

            if(isset($prict_product[3]) && $prict_product[3] !=''){ 
                $saleoff = $prict_product[3];
                $sale_sorting = $prict_product[3];
            } else {
                $saleoff = '';
                $sale_sorting = 0;
            }

            if(!empty($vo->imageshow_cover)) {  
                $image = env('CDN_URL').'static/imgs/products/medium/'.$vo->imageshow_cover;
            } else {
                $image = env('SHEEP_URL').'static/images/lazy.gif';
            }

            if(!empty($vo->name_product_sale)){
                $product_sale = $vo->name_product_sale;
            } else {
                $product_sale = $vo->name_product;
            }

            if($prict_product[0] == $prict_product[5]) {
                $price_sorting = $prict_product[5];
            } else {
                $price_sorting = $prict_product[0];
            }

            $product_data[] = array(
                'saleoff' => $saleoff,
                'url' => env('SHEEP_URL').'product/detail/'.$vo->id.'/'.url_clean($vo->name_product).'.html',
                'image' => $image,
                'product_sale' => $product_sale,
                'price' => $prict_product[0],
                'price_old' => $prict_product[5],
                'cashback' => floor($prict_product[2]),
                'price_sorting' => $price_sorting,
                'sale_sorting' => $sale_sorting,
                'desc' => str_replace('&nbsp;','',trim($this->substr_utf8(strip_tags($vo->details),0,200))) . '...',
            );
        }
        return response()->json($product_data);

    } else {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, getBestsellerProduct  cannot be found'
        ], 400);
    } 
}

public function ShowProductClearance(){

        $GetModel = new Product();
        $GetPromotionModel = new Promotion();

        $data = $GetModel->getProductClearance($_POST);

        if (!empty($data)) {

        foreach ($data as $vo) {
          

            $promotion_price = $GetPromotionModel->getCheckPromotion($vo->promotion_id, $vo->id);

            if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                $vo->nat_price = $promotion_price->pr_price;

                $vo->qty_sale = $vo->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                if($vo->qty_sale <= 0){
                    $vo->qty_sale = 0;
                }

                if($promotion_price->product_image != ''){
                    $vo->imageshow_cover = $promotion_price->product_image;
                }

                if(strip_tags($promotion_price->detail_product_mobile) != ''){
                    $vo->details = $promotion_price->detail_product_mobile;
                }

                if(strip_tags($promotion_price->detail_product) != ''){
                    $vo->details_web = $promotion_price->detail_product;
                }

                if($promotion_price->cashback != '-1'){
                    $vo->cashback_percent = $promotion_price->cashback;
                }

                if($promotion_price->cashback_aff != '-1'){
                    $vo->aff_percent = $promotion_price->cashback_aff;
                }
            }

            $prict_product = $GetModel->priceproduct_arr($vo);

            if(isset($prict_product[3]) && $prict_product[3] !=''){ 
                $saleoff = $prict_product[3];
                $sale_sorting = $prict_product[3];
            } else {
                $saleoff = '';
                $sale_sorting = 0;
            }

            if(!empty($vo->imageshow_cover)) {  
                $image = env('CDN_URL').'static/imgs/products/medium/'.$vo->imageshow_cover;
            } else {
                $image = env('SHEEP_URL').'static/images/lazy.gif';
            }

            if(!empty($vo->name_product_sale)){
                $product_sale = $vo->name_product_sale;
            } else {
                $product_sale = $vo->name_product;
            }

            if($prict_product[0] == $prict_product[5]) {
                $price_sorting = $prict_product[5];
            } else {
                $price_sorting = $prict_product[0];
            }

            $product_data[] = array(
                'saleoff' => $saleoff,
                'url' => env('SHEEP_URL').'product/detail/'.$vo->id.'/'.url_clean($vo->name_product).'.html',
                'image' => $image,
                'product_sale' => $product_sale,
                'price' => $prict_product[0],
                'price_old' => $prict_product[5],
                'cashback' => floor($prict_product[2]),
                'price_sorting' => $price_sorting,
                'sale_sorting' => $sale_sorting,
                'desc' => str_replace('&nbsp;','',trim($this->substr_utf8(strip_tags($vo->details),0,200))) . '...',
            );
        }
        return response()->json($product_data);

    } else {
        return response()->json(array());
    } 
}

public function GetProductPromotionFlash($promotion,$limit) {

        //return $limit;

    $GetProductModel = new Product();
    $GetPromotionModel = new Promotion();

    if(isset($promotion)){
        $productflash = $GetProductModel->getProductFlashByPromtionId($promotion[0]->PromtionId,$limit);
    }

    foreach ($productflash as $vp) {
        $date = date('Y-m-d h:i:s');

          
        if ($promotion[0]->PromotionStart.' '.$promotion[0]->time_start <= $date && $promotion[0]->PromotionEnd.' '.$promotion[0]->time_end >= $date){

            if($vp->promotion_id != '0'){

                $promotion_price = $GetPromotionModel->getCheckPromotion($vp->promotion_id,$vp->id);

                if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){

                    if ($promotion_price->fake_price > 0) {
                        $vp->snat_price = $promotion_price->fake_price;
                    }
                    $vp->nat_price = $promotion_price->pr_price;
                    $vp->qty_sale = $vp->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;

                    if($vp->qty_sale <= 0){
                        $vp->qty_sale = 0;
                    }


                    $count_order =  $GetPromotionModel->getCountOrderBuyPromotion($vp->promotion_id,$promotion_price->qty_limit,$vp->id);

                    if (!$count_order) {
                        $vp->pro_mod = false;
                    }else{
                        $vp->pro_mod = true;
                    }


                    if($promotion_price->product_image != ''){
                        $vp->imageshow_cover = $promotion_price->product_image;
                    }
                    if(strip_tags($promotion_price->detail_product_mobile) != ''){
                        $vp->details = $promotion_price->detail_product_mobile;
                    }
                    if(strip_tags($promotion_price->detail_product) != ''){
                        $vp->details_web = $promotion_price->detail_product;
                    }
                    if($promotion_price->cashback != '-1'){
                        $vp->cashback_percent = $promotion_price->cashback;
                    }
                    if($promotion_price->cashback_aff != '-1'){
                        $vp->aff_percent = $promotion_price->cashback_aff;
                    }
                }
            }else{
                $promotion_price = false;
            }
        }
        $prict_product = $GetProductModel->priceproduct_arr($vp);


        if(!empty($prict_product[3])){ 
            $saleoff = $prict_product[3];
        } else {
            $saleoff = '';
        }

        if(!empty($vp->imageshow_cover)) {  
            $image = env('CDN_URL').'static/imgs/products/medium/'.$vp->imageshow_cover;
        } else {
            $image = env('SHEEP_URL').'static/images/lazy.gif';
        }


        if(!empty($vp->name_product_sale)){
            $product_sale = $vp->name_product_sale;
        } else {
            $product_sale = $vp->name_product;
        }

        $price_strip_tag = strip_tags($prict_product[1]);


        $price_explode = explode('฿',$price_strip_tag);

        if (!isset($price_explode[2])) {
            $price_explode[2] = $price_explode[1];
            $price_explode[1] = '';

        }

        $resp[] = array(
            'saleoff' => $saleoff,
            'url' => env('SHEEP_URL').'product/detail/'.$vp->id.'/'.url_clean($vp->name_product).'.html',
            'image' => $image,
            'product_sale' => $product_sale,
            'price' => $price_explode[2],
            'price_old' =>  $price_explode[1],
            'cashback' => floor($prict_product[2]),
        );

    }

    return response()->json($resp);
} 

public function ShowProductFlash(Request $request){

    if(!empty($request->post('limit')) AND $request->isMethod('post')){

        $limit = $request->post('limit');

    }else if(!empty($request->route('limit')) AND $request->isMethod('get')){

        $limit = $request->route('limit');

    }
    else{

        $limit = 30;
    }

    if( Cache::has( 'ShowProductFlash'.$limit ) ) {
        return Cache::get( 'ShowProductFlash'.$limit );
    }else{

/*
        if($request->route('limit')){

            $limit = $request->route('limit');

        }else{

            $limit = 30;
        }*/

        //return $limit;
        $GetPromotionModel = new Promotion();

        $data = $GetPromotionModel->getPromotionFlash();

        if (count($data) > 0) {
            $datares = $this->GetProductPromotionFlash($data,$limit);
            Cache::put( 'ShowProductFlash'.$limit, $datares, 90 );
            return $datares;
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, GetProductPromotionFlash  cannot be found'
            ], 400);
        }

}
    }



    public function ShowBanding(){

        if(!empty($_POST['limit'])){
            $limit = $_POST['limit'];
        }else{
            $limit = 12;
        }

        if( Cache::has( 'ShowBanding'.$limit ) ) {
            return Cache::get( 'ShowBanding'.$limit );
        }else{

            $GetModel = new Product();

            $data = $GetModel->getBandings($limit);

            if (!$data) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, product  cannot be found'
                ], 400);
            } else {
                $datareturn = response()->json($data);
                Cache::put( 'ShowBanding'.$limit, $datareturn, 90 );
                return $datareturn;
            }

        
        }
    }

    public function ShowCaseProduct(Request $request){

        $GetModel = new Product();
        $data = $GetModel->getCaseProduct($request->route('where'),$request->route('limit'),$request->route('orderby'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }


    public function GetProductPrice($order_detail) {

        $GetModel = new Product();
        $GetPromotionModel = new Promotion();

        $run = 1;
        $i = 1;
        $no = 1;

        foreach ($order_detail as $vo) {

            if(!empty($vo->pid)) {
                $product = $GetModel->getProductId($vo->pid);
            } else {
                $product = $GetModel->getProductId($vo->id);
            }

            $promotion_price = $GetPromotionModel->getCheckPromotion($product->promotion_id, $product->id);

            if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                $product->nat_price = $promotion_price->pr_price;

                $product->qty_sale = $product->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                if($product->qty_sale <= 0){
                    $product->qty_sale = 0;
                }

                if($promotion_price->product_image != ''){
                    $product->imageshow_cover = $promotion_price->product_image;
                }

                if(strip_tags($promotion_price->detail_product_mobile) != ''){
                    $product->details = $promotion_price->detail_product_mobile;
                }

                if(strip_tags($promotion_price->detail_product) != ''){
                    $product->details_web = $promotion_price->detail_product;
                }

                if($promotion_price->cashback != '-1'){
                    $product->cashback_percent = $promotion_price->cashback;
                }

                if($promotion_price->cashback_aff != '-1'){
                    $product->aff_percent = $promotion_price->cashback_aff;
                }
            }

            $prict_product = $GetModel->priceproduct_arr($product);


            if(!empty($prict_product[3])){ 
                $saleoff = $prict_product[3];
            } else {
                $saleoff = '';
            }



            if(!empty($product->imageshow_cover)) {  
                $image = env('CDN_URL').'static/imgs/products/medium/'.$product->imageshow_cover;
            } else {
                $image = env('SHEEP_URL').'static/images/lazy.gif';
            }



            if(!empty($product->name_product_sale)){
                $product_sale = $product->name_product_sale;
            } else {
                $product_sale = $product->name_product;
            }

            $product_data[] = array(
                'saleoff' => $saleoff,
                'url' => env('SHEEP_URL').'product/detail/'.$product->id.'/'.url_clean($product->name_product).'.html',
                'image' => $image,
                'no' => $no,
                'product_sale' => $product_sale,
                'price' => $prict_product,
                'price_old' => $prict_product,
                'cashback' => floor($prict_product[2]),
            );

            $no++;

            if ($run == 3) {
                $run = 1;
                $i++;
            }else{
                $run++;
            } 
        }

        return response()->json($product_data);
    }

    public function ShowBestsellerProduct(){

        if( Cache::has( 'ShowBestsellerProduct' ) ) {
            return Cache::get( 'ShowBestsellerProduct' );
        }else{

        $GetModel = new Product();
        $GetPromotionModel = new Promotion();

        $datas = $GetModel->getBestsellerProduct();

        if (count($datas) > 0) {
            //return $this->GetProductPrice($data);

            $run = 1;
            $i = 1;
            $no = 1;

            foreach ($datas as $vo) {

                if(!empty($vo->pid)) {
                    $product = $GetModel->getProductId($vo->pid);
                } else {
                    $product = $GetModel->getProductId($vo->id);
                }

                $promotion_price = $GetPromotionModel->getCheckPromotion($product->promotion_id, $product->id);

                if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                    $product->nat_price = $promotion_price->pr_price;

                    $product->qty_sale = $product->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                    if($product->qty_sale <= 0){
                        $product->qty_sale = 0;
                    }

                    if($promotion_price->product_image != ''){
                        $product->imageshow_cover = $promotion_price->product_image;
                    }

                    if(strip_tags($promotion_price->detail_product_mobile) != ''){
                        $product->details = $promotion_price->detail_product_mobile;
                    }

                    if(strip_tags($promotion_price->detail_product) != ''){
                        $product->details_web = $promotion_price->detail_product;
                    }

                    if($promotion_price->cashback != '-1'){
                        $product->cashback_percent = $promotion_price->cashback;
                    }

                    if($promotion_price->cashback_aff != '-1'){
                        $product->aff_percent = $promotion_price->cashback_aff;
                    }
                }

                $prict_product = $GetModel->priceproduct_arr($product);


                if(!empty($prict_product[3])){ 
                    $saleoff = $prict_product[3];
                } else {
                    $saleoff = '';
                }





                if(!empty($product->imageshow_cover)) {  
                    $image = env('CDN_URL').'static/imgs/products/medium/'.$product->imageshow_cover;
                } else {
                    $image = env('SHEEP_URL').'static/images/lazy.gif';
                }



                if(!empty($product->name_product_sale)){
                    $product_sale = $product->name_product_sale;
                } else {
                    $product_sale = $product->name_product;
                }

                $product_data['best_seller_'.$i][] = array(
                    'saleoff' => $saleoff,
                    'url' => env('SHEEP_URL').'product/detail/'.$product->id.'/'.url_clean($product->name_product).'.html',
                    'image' => $image,
                    'no' => $no,
                    'product_sale' => $product_sale,
                    'price' => $prict_product[0],
                    'price_old' => $prict_product[5],
                    'cashback' => floor($prict_product[2]),
                );

                $no++;

                if ($run == 3) {
                    $run = 1;
                    $i++;
                }else{
                    $run++;
                } 
            }
            $datareturn = response()->json($product_data);
            Cache::put( 'ShowBestsellerProduct', $datareturn, 90 );
            return $datareturn;

            
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getBestsellerProduct  cannot be found'
            ], 400);
        }

    }
        
    }

    
    public function ShowSearchProduct(Request $request){

        $GetModel = new Product();
        $data = $GetModel->getSearchProduct($request->route('keyword'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getSearchProduct  cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowProductTradeDate(Request $request){

        $GetModel = new Product();
        $data = $GetModel->getProductTradeDate($request->route('datestart'),$request->route('dateend'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function GetDataRandomProductLastView($data) {

        if(isset($_COOKIE["lastview"])) {

            $productrecommended_data_array[] = '';
            $listlastarray = array_reverse(explode(',',$_COOKIE["lastview"]));

            for ($i=0; $i < count($listlastarray); $i++) {

                   /* $productlastview = $this->db->table('product')->value('id,name_product,categoryid,category_subid,name_product_sale,nat_price,snat_price,sku_id,sku_item')->where('`show_hidden` = \'1\' AND `id` = \''.$listlastarray[$i].'\'')->limit('1')->find();
*/
                    /* $productlastview =  DB::table('product')->select('id','name_product','categoryid','category_subid','name_product_sale','nat_price','snat_price','sku_id','sku_item')
                    ->where('show_hidden','1')->limit('1')->get();*/

                    

                    foreach ($productlastview as $productrands_s) {

                        if($productrands_s->category_subid > 1) {
                            $where_category = ' AND `category_subid` = \''.$productrands_s->category_subid.'\'';
                        } else {
                            $where_category = ' AND `categoryid` = \''.$productrands_s->categoryid.'\'';
                        }
                        
                        /*  $productrecommendeds= $this->db->table('product')->value('id')->where('`show_hidden` = \'1\' AND `id` <> \''.$productrands_s->id.'\' AND `sku_id` <> \''.$productrands_s->sku_id.'\' AND `sku_item` <> \''.$productrands_s->sku_item.'\''.$where_category.'')->group_by('sku_id,sku_item')->limit('100')->find();*/

                        /* $productrecommendeds =   DB::select(DB::raw('SELECT  id FROM product WHERE `show_hidden` = \'1\' AND `id` != \''.$productrands_s->id.'\' AND `sku_id` <> \''.$productrands_s->sku_id.'\' AND `sku_item` <> \''.$productrands_s->sku_item.'\''.$where_category.' GROUP BY  sku_id,sku_item  LIMIT 100'));*/

                        $productrecommendeds = $GetProductModel->getProductRecommendedsSelectId($productrands_s->id,$productrands_s->sku_id,$productrands_s->sku_item,$where_category);

                        foreach ($productrecommendeds as $productrecommended) {
                            $productrecommended_data_array[] = $productrecommended->id;
                        }
                    }
                }

                $productrecommended_unique = array_unique($productrecommended_data_array);
                $productrecommended_splice = array_splice($productrecommended_unique, 0, 24);

                for ($i=0; $i < count($productrecommended_splice); $i++) {
                    /*$productrecommendeds= $this->db->table('product')->value('id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand')->where('`show_hidden` = \'1\' AND `id` = \''.$productrecommended_unique[$i].'\'')->limit('1')->find();*/

                    /*$productrecommendeds =   DB::select(DB::raw('SELECT  id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand FROM product WHERE `show_hidden` = \'1\' AND `id` = \''.$productrecommended_unique[$i].'\' LIMIT 1'));*/

                    $GetProductModel = new Product();
                    $productrecommendeds = $GetProductModel->getProductRecommendedsUnique($productrecommended_unique[$i]);
                    

                    foreach ($productrecommendeds as $productrecommended) {

                        if($productrecommended->promotion_id != '0'){

                            $GetPromotionModel = new Promotion();

                            $promotion_price = $GetPromotionModel->getCheckPromotion($productrecommended->promotion_id,$productrecommended->id);
                            
                            if(isset($promotion_price)){
                                if ($promotion_price->fake_price > 0) {
                                    $productrecommended->snat_price = $promotion_price->fake_price;
                                }
                                $productrecommended->nat_price = $promotion_price->pr_price;
                                $productrecommended->qty_sale = $productrecommended->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                                if($productrecommended->qty_sale <= 0){
                                    $productrecommended->qty_sale = 0;
                                }
                                if($promotion_price->product_image != ''){
                                    $productrecommended->imageshow_cover = $promotion_price->product_image;
                                }
                                if(strip_tags($promotion_price->detail_product_mobile) != ''){
                                    $productrecommended->details = $promotion_price->detail_product_mobile;
                                }
                                if(strip_tags($promotion_price->detail_product) != ''){
                                    $productrecommended->details_web = $promotion_price->detail_product;
                                }
                                if($promotion_price->cashback != '-1'){
                                    $productrecommended->cashback_percent = $promotion_price->cashback;
                                }
                                if($promotion_price->cashback_aff != '-1'){
                                    $productrecommended->aff_percent = $promotion_price->cashback_aff;
                                }
                            }
                        }else{
                            $promotion_price = false;
                        }

                        $prict_product = $thispriceproduct_arr($productrecommended);

                        if(isset($prict_product[3]) && $prict_product[3] !=''){ 
                            $saleoff = $prict_product[3];
                        } else {
                            $saleoff = '';
                        }




                        if(!empty($value_r->imageshow_cover)) {  
                            $image = env('CDN_URL').'static/imgs/products/medium/'.$value_r->imageshow_cover;
                        } else {
                            $image = env('SHEEP_URL').'static/images/lazy.gif';
                        }


                        if($productrecommended->name_product_sale != ''){
                            $product_sale = $productrecommended->name_product_sale;
                        } else {
                            $product_sale = $productrecommended->name_product;
                        }

                        $price_strip_tag = strip_tags($prict_product[1]);
                        $price_explode = explode('฿',$price_strip_tag);
                        if (!isset($price_explode[2])) {
                            $price_explode[2] = $price_explode[1];
                            $price_explode[1] = '';

                        }
                        
                        $productrecommended_data[] = array(
                            'saleoff' => $saleoff,
                            'url' => env('SHEEP_URL').'product/detail/'.$productrecommended->id.'/'.url_clean($productrecommended->name_product).'.html',
                            'image' => $image,
                            'product_sale' => $product_sale,
                            'price' => $price_explode[1],
                            'price_old' => $price_explode[2],
                            'cashback' => floor($prict_product[2]),
                        );
                    }
                }
            } else {
                $limit = 24;

                $cat = '';

                /*  $productrand = $this->db->table('product')->value('id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand')->where('`show_hidden` = \'1\' '.$cat.' ')->order_by('RAND()')->limit($limit)->find();*/

                /* $productrand =   DB::select(DB::raw('SELECT  id,cashback_percent,imageshow_cover,name_product,name_product_sale,nat_price,snat_price,aff_percent,qty,qty_sale,promotion_id,gift,brand FROM product WHERE `show_hidden` = "1"  ORDER BY RAND() limit 24 '));*/

                $GetProductModel = new Product();
                $productrand = $GetProductModel->getProductRandomLastView();



                foreach ($productrand as $key_r => $value_r) {

                    $GetPromotionModel = new Promotion();
                    $promotion_price = $GetPromotionModel->getCheckPromotion($value_r->promotion_id,$value_r->id);
                    if($promotion_price){
                        if ($promotion_price->fake_price > 0) {
                            $value_r->snat_price = $promotion_price->fake_price;
                        }
                        $value_r->nat_price = $promotion_price->pr_price;
                        $value_r->qty_sale = $value_r->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                        if($value_r->qty_sale <= 0){
                            $value_r->qty_sale = 0;
                        }
                        if($promotion_price->product_image != ''){
                            $value_r->imageshow_cover = $promotion_price->product_image;
                        }
                        if(strip_tags($promotion_price->detail_product_mobile) != ''){
                            $value_r->details = $promotion_price->detail_product_mobile;
                        }
                        
                        if(strip_tags($promotion_price->detail_product) != ''){
                            $value_r->details_web = $promotion_price->detail_product;
                        }
                        if($promotion_price->cashback != '-1'){
                            $value_r->cashback_percent = $promotion_price->cashback;
                        }
                        if($promotion_price->cashback_aff != '-1'){
                            $value_r->aff_percent = $promotion_price->cashback_aff;
                        }
                    }


                    $prict_product = $GetProductModel->priceproduct_arr($value_r);

                    if(isset($prict_product[3]) && $prict_product[3] !=''){ 
                        $saleoff = $prict_product[3];
                    } else {
                        $saleoff = '';
                    }


                    if(!empty($value_r->imageshow_cover)) {  
                        $image = env('CDN_URL').'static/imgs/products/medium/'.$value_r->imageshow_cover;
                    } else {
                        $image = env('SHEEP_URL').'static/images/lazy.gif';
                    }




                    if($value_r->name_product_sale != ''){
                        $product_sale = $value_r->name_product_sale;
                    } else {
                        $product_sale = $value_r->name_product;
                    }

                    $price_strip_tag = strip_tags($prict_product[1]);
                    $price_explode = explode('฿',$price_strip_tag);

                    if (!isset($price_explode[2])) {
                        $price_explode[2] = $price_explode[1];
                        $price_explode[1] = '';

                    }
                    $productrecommended_data[] = array(
                        'saleoff' => $saleoff,
                        'url' => env('SHEEP_URL').'product/detail/'.$value_r->id.'/'.url_clean($value_r->name_product).'.html',
                        'image' => $image,
                        'product_sale' => $product_sale,
                        'price' => $price_explode[1],
                        'price_old' => $price_explode[2],
                        'cashback' => floor($prict_product[2]),
                    );

                }
            }
            echo json_encode($productrecommended_data,JSON_PRETTY_PRINT);
        }
        public function ShowRandomProductLastView(){

            $GetProductModel = new Product();

       /* if(isset($_POST['limit'])){

            $limit = $_POST['limit'];

        }else{

            $limit = '24';
        }*/

        $data = $GetProductModel->getRandomProductLastView();
        //return $data;
        if (count($data)>0){


            return $this->GetDataRandomProductLastView($data);

        }else{
            return response()->json([
                'success' => false,
                'message' => 'Sorry, getDataRandomProductLastView  cannot be found'
            ], 400);
        }


    }

    
    public function ShowPromotionProductInArray(){

        $GetModel = new Product();

        if(isset($_POST['PromotionProduct']) AND isset($_POST['limit'])){

            $PromotionProduct =$_POST['PromotionProduct'];
            $explode_array = (explode(",",$PromotionProduct));
            $limit =$_POST['limit'];

        }else{

            $PromotionProduct =$_POST['PromotionProduct'];
            $explode_array = (explode(",",$PromotionProduct));
            $limit ='';

        }
        
        $data = $GetModel->getPromotionProductInArray($explode_array,$limit);
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowProductTradeId(Request $request){

        $GetModel = new Product();
        $data = $GetModel->getProductTradeId($request->route('pid'));
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowProductId(){

        if(!empty($_POST['pid'])){

            $pid = $_POST['pid'];

        }

        $GetModel = new Product();
        $data = $GetModel->getProductId($pid);
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowTagsbyCategory(){

        if(!empty($_POST['categoryid']) AND !empty($_POST['limit'])){

            $categoryid = $_POST['categoryid'];
            $limit = $_POST['limit'];

        }else{

            $categoryid = $_POST['categoryid'];
            $limit = '4';
        }

        $GetModel = new Product();
        $data = $GetModel->getTagsbyCategory($categoryid,$limit);
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowProductTrade(){

        $GetModel = new Product();
        $data = $GetModel->getProductTrade();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }



    public function ShowProductFavourite(){

        $GetModel = new Product();
        $data = $GetModel->getProductFavourite();
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }

        return $data;
    }
    public function ShowProductFavouriteID(){

       $GetModel = new Product();
       if(isset($_POST['pid']) AND isset($_POST['id_user'])){

        $pid = $_POST['pid'];
        $id_user = $_POST['id_user'];

    }else if(isset($_POST['id_user'])){

       $pid = '';
       $id_user = $_POST['id_user'];
   }


   $data = $GetModel->getProductFavouriteId($pid,$id_user);
   if (!$data) {
    return response()->json([
        'success' => false,
        'message' => 'Sorry, product  cannot be found'
    ], 400);
}

return $data;
}
public function ShowProductReview(){

    $GetModel = new Product();
    $data = $GetModel->getProductReview();
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function CountProductReview(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getCountProductReview($request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function ShowProductRankstar(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductRankstar($request->route('pid'),$request->route('uid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function ShowProductRating(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductRating($request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function CountProductRating(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getCountProductRating($request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function ShowProductStarRating(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductStarRating($request->route('pid'),$request->route('star'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}
public function ShowProductSku(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductSku($request->route('sku_id'),$request->route('sku_item'),$request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowCommentProgress(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getCommentProgress($request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowProductRandom(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductRandom($request->route('pid'),$request->route('limit'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowProductCategory(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductCategory($request->route('pid'),$request->route('categoryid'),$request->route('categorysubid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowProductReviewMaxId(Request $request){

    $GetModel = new Product();
    $data = $GetModel->getProductReviewMaxId($request->route('pid'));
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowMainSearch(){

    $GetModel = new Product();
    $data = $GetModel->getMainSearch();
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}

public function ShowProductColumn(){

    $GetModel = new Product();
    $data = $GetModel->getProductColumn();
    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product  cannot be found'
        ], 400);
    }

    return $data;
}


public function ShowProductCountReviewByUseridInv(){

    $GetModel = new Product();


     if(isset($_POST['uid']) AND isset($_POST['oid'])){

        $uid = $_POST['uid'];
        $inv = $_POST['oid'];

    }

    $data = $GetModel->getProductCountReviewByUseridInv($uid, $inv);

    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, getProductCountReviewByUseridInv  cannot be found'
        ], 400);
    }

    return $data;
}

    /*public function ShowProductFavouriteApp($userid){

        $GetModel = new Product();
        $data = $GetModel->getProductFavouriteApp($userid);
        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product  cannot be found'
            ], 400);
        }
        
        return $data;
    }*/

    public function ResponeProduct(Request $request){

        return response($request->all());

    }

    public function getProductPaginate() {
        $limit = 24;

        $GetModel = new Product();

        $data_results = $GetModel->getProductProductPagination($_POST);

        $response = [
            'pagination' => [
                'total' => $data_results->total(),
                'per_page' => $data_results->perPage(),
                'current_page' => $data_results->currentPage(),
                'last_page' => $data_results->lastPage(),
                'from' => $data_results->firstItem(),
                'to' => $data_results->lastItem()
            ],
            'data' => $data_results
        ];

        return response()->json($response);
    }


    public function GetDataProductFlashDealAll(){

        $GetProductModel = new Product();
        $GetPromotionModel = new Promotion();

        $date = date('Y-m-d');
        $time = date('H:i:s');

        
        $promotion_one = $GetPromotionModel->getPromotionFlashStartEndByOne($date,$time);

        $promotion = json_decode($promotion_one, true);

        $promotion_all = $GetPromotionModel->getPromotionFlashEnd($date,$time);


        //return $promotion_all;
        if ($promotion) {

            $data = $GetProductModel->getProductFlashByPromtionId2($promotion['PromtionId']);
            
        }else{

            $data = $GetProductModel->getProductFlashByPromtionId2($promotion->PromtionId);
        }

        
        foreach ($data as $vp) { 

            if($vp->promotion_id != '0'){
                $promotion_price = $GetPromotionModel->getCheckPromotion($vp->promotion_id,$vp->id);

                //return $vp->id.' '.$vp->promotion_id;
                //return $promotion_price->pr_price;
              
                if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                    $vp->nat_price = $promotion_price->pr_price;
                    $vp->qty_sale = $vp->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;

                      $count_order = $GetPromotionModel->getCountOrderBuyPromotion($vp->promotion_id,$vp->id,$promotion_price->qty_limit);
                     $count_order_percen = $GetPromotionModel->getCountOrderBuyPromotionPercen($vp->promotion_id,$vp->id,$promotion_price->qty_limit);

                    

                    if($vp->qty_sale <= 0){
                        $vp->qty_sale = 0;
                    }
                    

                    if($promotion_price->product_image != ''){
                        $vp->imageshow_cover = $promotion_price->product_image;
                    }
                    if(strip_tags($promotion_price->detail_product_mobile) != ''){
                        $vp->details = $promotion_price->detail_product_mobile;
                    }
                    if(strip_tags($promotion_price->detail_product) != ''){
                        $vp->details_web = $promotion_price->detail_product;
                    }
                    if($promotion_price->cashback != '-1'){
                        $vp->cashback_percent = $promotion_price->cashback;
                    }
                    if($promotion_price->cashback_aff != '-1'){
                        $vp->aff_percent = $promotion_price->cashback_aff;
                    }
                     
                  
                }
            }else{



               $promotion_price = false;
           }

      // return $promotion_price;

        $prict_product = $GetProductModel->priceproduct_arr($vp);

        if(isset($prict_product[3]) && $prict_product[3] !=''){ 
            $saleoff = $prict_product[3];
        } else {
            $saleoff = '';
        }

        
        if(!empty($vp->imageshow_cover)) {  
                        $image = env('CDN_URL').'static/imgs/products/medium/'.$vp->imageshow_cover;
                    } else {
                        $image = env('SHEEP_URL').'static/images/lazy.gif';
                    }

        if($vp->name_product_sale != ''){

                $product_sale = $vp->name_product_sale;

         } else {

                $product_sale = $vp->name_product;
         }


        $price_strip_tag = strip_tags($prict_product[1]);
        $price_explode = explode('฿',$price_strip_tag);

        if (!isset($price_explode[2])) {
            $price_explode[2] = $price_explode[1];
            $price_explode[1] = '';

        }    

           $productflashall_data[] = array(
                        'id' => $vp->id,
                        'saleoff' => $saleoff,
                        'url' => env('SHEEP_URL').'product/detail/'.$vp->id.'/'.url_clean($vp->name_product).'.html',
                        'image' => $image,
                        'product_sale' => $product_sale,
                        'price' => $price_explode[1],
                        'price_old' => $price_explode[2],
                        'cashback' => floor($prict_product[2]),
                        'count_order' => $count_order,
                        'count_order_percen' => $count_order_percen,
                       
          );
    }

    return json_encode($productflashall_data);


}
  

public function ShowDataProductFlashDealAll(){

    $data = $this->GetDataProductFlashDealAll();

    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'GetDataProductFlashDealAll, product  cannot be found'
        ], 400);
    }

    return $data;


}

public function ShowProductReviewPidUserid(){

    if(isset($_POST['pid']) && isset($_POST['user_id']) && isset($_POST['inv'])){

        $pid = $_POST['pid'];
        $user_id = $_POST['user_id'];
        $inv = $_POST['inv'];

         $explode_array = (explode(",",$pid));

    }
   // return  $_POST['user_id'];
    $GetProductModel = new Product();
    $data = $GetProductModel->getProductReviewPidUserid($explode_array,$user_id,$inv);

    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'getProductReviewPidUserid, product  cannot be found'
        ], 400);
    }

    return $data;


}

/*public function ShowProductReviewInvUserid(Request $request){


    $GetProductModel = new Product();
    $data = $GetProductModel->getProductReviewInvUserid($request->route('inv'),$request->route('user_id'));

    if (!$data) {
        return response()->json([
            'success' => false,
            'message' => 'getProductReviewInvUserid, product  cannot be found'
        ], 400);
    }

    return $data;


}*/


public function store(Request $request)
{
    $this->validate($request, [
        'name' => 'required',
        'price' => 'required|integer',
        'quantity' => 'required|integer'
    ]);
    $product = new Product();
    $product->name = $request->name;
    $product->price = $request->price;
    $product->quantity = $request->quantity;
    if ($this->user()->products()->save($product))
        return response()->json([
            'success' => true,
            'product' => $product
        ]);
    else
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product could not be added'
        ], 500);
}
public function update(Request $request, $id)
{
    $product = $this->user()->products()->find($id);
    if (!$product) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product with id ' . $id . ' cannot be found'
        ], 400);
    }
    $updated = $product->fill($request->all())
    ->save();
    if ($updated) {
        return response()->json([
            'success' => true
        ]);
    } else {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product could not be updated'
        ], 500);
    }
}
public function destroy($id)
{
    $product = $this->user()->products()->find($id);
    if (!$product) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, product with id ' . $id . ' cannot be found'
        ], 400);
    }
    if ($product->delete()) {
        return response()->json([
            'success' => true
        ]);
    } else {
        return response()->json([
            'success' => false,
            'message' => 'Product could not be deleted'
        ], 500);
    }
}
}