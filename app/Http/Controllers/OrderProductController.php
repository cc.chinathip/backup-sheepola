<?php

namespace App\Http\Controllers;
use App\OrderProduct;
use Illuminate\Http\Request;

class OrderProductController extends Controller
{
    //
    public function ShowOrderProduct()
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getOrderProduct();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }

   


   public function ShowCountOrderProduct(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getCountOrderProduct($request->route('userid'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getCountOrderProduct product cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowStatusLoadOrder(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getStatusLoadOrder($request->route('userid'),$request->route('status'),$request->route('date'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getStatusLoadOrder product cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowOrderProductStatus(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getOrderProductStatus($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowOrderProductInv(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getOrderProductId($request->route('id'),$request->route('value'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowOrderProductId(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getOrderProductId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowOrderProductId2(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getOrderProductId($request->route('id'),$request->route('value'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPendingCash(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getPendingCash($request->route('userid'),$request->route('lastdate'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowPendingCashBack(Request $request)
    {
        $GetModel = new OrderProduct();

        $data = $GetModel->getPendingCashBack($request->route('piuseridd'),$request->route('lastdate'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order product cannot be found'
            ], 400);
        }

        return $data;
    }

   
}
