<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ShowProvince()
    {
        $GetMoel = new Address();

        $data = $GetMoel->getProvince();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'address cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowProvinceId(Request $request)
    {
        $GetMoel = new Address();

        $data = $GetMoel->getProvinceId($request->route('value'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'address cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowAmphurId(Request $request)
    {
        $GetMoel = new Address();

        $data = $GetMoel->getAmphurId($request->route('value'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'address cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowTambonId(Request $request)
    {
        $GetMoel = new Address();

        $data = $GetMoel->getTambonId($request->route('value'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'address cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        //
    }
}
