<?php

namespace App\Http\Controllers;

use App\MyCoupon;
use Illuminate\Http\Request;

class MyCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ShowMyCoupon()
    {
        $GetMoel = new MyCoupon();

        $data = $GetMoel->getMyCoupon();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'my coupon cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowCountMyCouponId(Request $request)
    {
        $GetMoel = new MyCoupon();

        $data = $GetMoel->CountMyCouponId($request->route('userid'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'CountMyCouponId cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowMyCouponId(Request $request)
    {
        $GetMoel = new MyCoupon();

        $data = $GetMoel->getMyCouponId($request->route('userid'),$request->route('date_start'),$request->route('date_end'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'my coupon cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowMyCouponId2(Request $request)
    {
        $GetMoel = new MyCoupon();

        $data = $GetMoel->getMyCouponId2($request->route('userid')$request->route('codeid'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'my coupon cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MyCoupon  $myCoupon
     * @return \Illuminate\Http\Response
     */
    public function show(MyCoupon $myCoupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MyCoupon  $myCoupon
     * @return \Illuminate\Http\Response
     */
    public function edit(MyCoupon $myCoupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MyCoupon  $myCoupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MyCoupon $myCoupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MyCoupon  $myCoupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(MyCoupon $myCoupon)
    {
        //
    }
}
