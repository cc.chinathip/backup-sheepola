<?php

namespace App\Http\Controllers;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;

class OrderDetailController extends Controller
{
    //

    public function ShowOrderDetail()
    {
        $GetModel = new OrderDetail();

        $data = $GetModel->getOrderDetail();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order detail cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowOrderDetailInv(Request $request)
    {
        $GetModel = new OrderDetail();

        $data = $GetModel->getOrderDetailInv($request->route('userid'),$request->route('inv'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'order detail cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowOrderDetailJoinProductReview(Request $request){

        $GetModel = new OrderDetail();

        if(isset($_POST['userid']) && isset($_POST['inv'])){

            $userid = $_POST['userid'];
            $inv = $_POST['inv'];

        }
        $data = $GetModel->getOrderDetailJoinProductReview($userid,$inv);

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getOrderDetailJoinProductReview  cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowCountOrderDetailUseridInv(){

        $GetModel = new OrderDetail();

        if(isset($_POST['userid']) && isset($_POST['inv'])){

            $userid = $_POST['userid'];
            $inv = $_POST['inv'];

        }
        $data = $GetModel->getCountOrderDetailUseridInv($userid,$inv);

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getCountOrderDetailUseridInv  cannot be found'
            ], 400);
        }

        return $data;
    }


    public function GetDataOrderDetailJoinOrderProduct($datas){


        $GetProductModel = new Product();
       $review_config =  $GetProductModel->getReviewConfig();

        //$review_config =  DB::table('review_config')->first();

        foreach ($datas as $key => $data_value) {

            //$data_value->pid
        /*$product_review =  DB::table('product_review')->select('message','edit','tag')->where('pid', $data_value->pid)->where('user_id', $data_value->uid)->where('oid', $data_value->oid)->where('status', '1')->groupBy('pid')->first();*/

        $product_review = $GetProductModel->getProductReviewByTag($data_value->pid,$data_value->uid,$data_value->oid);

       // return  $product_review->tag;
        
       // foreach ($product_review as $product_review_value) {
        if(isset($product_review->tag)){
             $array_a = explode(",",$product_review->tag);

        }else{
            $array_a ='';
        }
       

            //return $product_review_value->tag;
        
        //$array_b = ['สินค้าดีเยี่ยม','สินค้าถูกมาก','สินค้าส่งเร็วมาก','แพ็คของดีมาก','ชอบมาก'];
        $array_b = unserialize($review_config->text_review);
    
        $tag_string = array();
        for($i=0; $i<count($array_b); $i++) {
            $string_check = '';
            $tag_data[] = array();
            if(is_array($array_b) && is_array($array_a)){

                if(in_array($array_b[$i], $array_a)) {
                $string_check = '1';
                    //return $string_check;
                } else {
                    $string_check = '0';
                }

            }
            
            $tag_data[$i] = array(
                'tags'=>$array_b[$i],
                'checkeds' => $string_check
            ); 
            $tag_string[] =  $tag_data[$i];
        }

        //$checked = array_intersect($array_a, $array_b); 
        //$result = array_values($checked); 
        //return $checked;

        $rankstar = $GetProductModel->getRankstarByReview($data_value->pid,$data_value->uid);
      

                if(!empty($product_review)){

                    $action_review =  1;

                }else{
                    $action_review = 0;
                }

                if(!empty($rankstar)){

                    $rankstar;

                }else{
                    $rankstar =  '';
                }

            
                    $Product_order_detail[] = array(
                            'oid' => $data_value->oid,
                            'product_review' => $action_review,
                            'rankstar' => $rankstar,
                            'review_select' => $product_review,
                            'review_config' => $array_b,
                            'tags_string' => $tag_string,
                            'sku_item' => $data_value->sku_item,
                            'pid' => $data_value->pid,
                            'imageshow_cover' => $data_value->imageshow_cover,
                            'name_product' => $data_value->name_product,
                            'name_product_sale' => $data_value->name_product_sale,
                            'priceperpcs' => $data_value->priceperpcs,
                            'userid' => $data_value->uid,
                            'nat_price' => $data_value->nat_price,
                            'snat_price' => $data_value->snat_price,
                            'qty' => $data_value->qty,
                            
                     );
          
        }
        return $Product_order_detail;
     //}

     

    }

    public function ShowOrderDetailJoinOrderProduct(Request $request){

        $GetModel = new OrderDetail();

        if(isset($_POST['userid']) && isset($_POST['inv'])){

            $userid = $_POST['userid'];
            $inv = $_POST['inv'];
           // $pid = $_POST['pid'];


        }

        $data = $GetModel->getOrderDetailJoinOrderProduct($userid,$inv);

        
        if (count($data) >0) {

            return $this->GetDataOrderDetailJoinOrderProduct($data);

        }else{
            return response()->json([
                'success' => false,
                'message' => 'getOrderDetailJoinOrderProduct cannot be found'
            ], 400);
        }


        //return $data;
    }
}
