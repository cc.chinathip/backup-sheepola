<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ShowTransactionsId(Request $request)
    {
        $GetModel = new Transactions();

        $data = $GetModel->getTransactionsId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'transaction cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowPaymentGatewayId(Request $request)
    {
        $GetModel = new Transactions();

        $data = $GetModel->getPaymentGatewayId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'transaction cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowTotalHistoryUnRead(Request $request)
    {
        $GetModel = new Transactions();

        $data = $GetModel->getTotalHistoryUnRead($request->route('userid'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'getTotalHistoryUnRead cannot be found'
            ], 400);
        }

        return $data;
    }


    public function ShowWithDrawalHistory(Request $request)
    {
        $GetModel = new Transactions();

        $data = $GetModel->getWithDrawalHistory($request->route('userid'),$request->route('lastdate'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'transaction cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transactions $transactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transactions)
    {
        //
    }
}
