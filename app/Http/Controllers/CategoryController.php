<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use App\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
class CategoryController extends Controller
{

    public function index()
    {
        $GetModel = new Category();

        $Cate= $GetModel->getCategoryAll();

        return json_encode($Cate);
    }

    public function ShowCategory()
    {
        $GetModel = new Category();

        $Cate= $GetModel->getCategoryByID($_POST);

        return json_encode($Cate);
    }

    public function ShowBannerCategory()
    {
        $GetModel = new Category();

        $Cate= $GetModel->getBannerCategoryByID($_POST);

        return json_encode($Cate);
    }

    public function ShowNameCategory()
    {
        $GetModel = new Category();

        $Cate= $GetModel->getNameCategoryByID($_POST);

        return json_encode($Cate);
    }

    public function ShowCouponCategory()
    {
        $GetModel = new Category();

        $Cate= $GetModel->getCouponCategoryByID($_POST);

        return json_encode($Cate);
    }

    public function MenuMobile()
    {
        if( Cache::has( 'MenuMobile' ) ) {
            return json_encode(Cache::get( 'MenuMobile' ));
        }else{

            $GetModel = new Category();

            $Cate= $GetModel->getCategoryMobile();
            Cache::put( 'MenuMobile', $Cate, 300 );

            return json_encode($Cate);

        }

    }

    public function MenuCateLeft()
    {
        $GetModel = new Category();

        $cate= $GetModel->menucateleft();


        return $cate;
    }

       public function MenuCateRootLeft()
    {
        $GetModel = new Category();

        $cate= $GetModel->menucaterootleft();


        return $cate;
    }

     public function GetDataCategoryProduct($categorys,$limit) {

        $GetProductModel = new Product();
        $GetPromotionModel = new Promotion();
        $resp['categorys'] = $categorys;
        
        foreach ($resp['categorys'] as $vc) {

            $tags = $GetProductModel->getTagsbyCategory($vc->id,$limit);
            $i=0;
 
             foreach ($tags as $vt) {
                $taglable = explode(',',$vt->taglable);
                foreach ($taglable as $v) {

                    if(!empty($v)){
                        
                        if($i<=7){
                            $resp['tags'][$vc->id][$i] = strtolower($v);   
                            
                        } 
                    } 
                    $i++;
                } 
                
            }
          
            $products = $GetProductModel->getProductByCategoryid($vc->id,$limit);
            foreach ($products as $vl) {

                 if($vl->promotion_id != '0'){
                    $promotion_price = $GetPromotionModel->getCheckPromotion($vl->id,$vl->promotion_id);
                    if(isset($promotion_price) && !empty($promotion_price) && !empty($promotion_price->pr_price)){
                        if ($promotion_price->fake_price > 0) {
                            $vl->snat_price = $promotion_price->fake_price;
                        }
                        $vl->nat_price = $promotion_price->pr_price;
                        $vl->qty_sale = $vl->qty_sale + $promotion_price->qty_limit - $promotion_price->qty_sale;
                        if($vl->qty_sale <= 0){
                            $vl->qty_sale = 0;
                        }
                    }
                }else{
                    $promotion_price = false;
                }

                $prict_product = $GetProductModel->priceproduct_arr($vl,'');

                if(isset($prict_product[3]) && !empty($prict_product[3])){ 
                    $saleoff = $prict_product[3];
                     
                } else {
                    $saleoff = '';

                }

                
                    if(!empty($vl->imageshow_cover)) {  
                        $image = env('CDN_URL').'static/imgs/products/medium/'.$vl->imageshow_cover;
                    } else {
                        $image = env('SHEEP_URL').'static/images/lazy.gif';
                    }

                if($vl->name_product_sale != ''){
                    $product_sale = $vl->name_product_sale;
                } else {
                    $product_sale = $vl->name_product;
                }

                $price_strip_tag = strip_tags($prict_product[1]);
                $price_explode = explode('฿',$price_strip_tag);

                if (!isset($price_explode[2])) {
                    $price_explode[2] = $price_explode[1];
                    $price_explode[1] = '';
                   
                }
                $resp['products'][$vc->id][] = array(
                    'saleoff' => $saleoff,
                    'url' => env('SHEEP_URL').'product/detail/'.$vl->id.'/'.url_clean($vl->name_product).'.html',
                    'image' => $image,
                    'product_sale' => $product_sale,
                    'price' => $price_explode[2],
                    'price_old' =>  $price_explode[1],
                    'cashback' => floor($prict_product[2]),
                );


            }


        } 
        return json_encode($resp);

    }
    public function ShowCategoryProducts(){


        if(!empty($_POST['limit'])){

            $limit = $_POST['limit'];

        }else{

             $limit = '6';
        }

        if( Cache::has( 'ShowCategoryProducts'.$limit ) ) {
            return Cache::get( 'ShowCategoryProducts'.$limit );
        }else{
            $GetMoel = new Category();

            $data = $GetMoel->getCategoryProducts($limit);


            if (count($data) > 0) {
               $datareturn = $this->GetDataCategoryProduct($data,$limit);
                Cache::put( 'ShowCategoryProducts'.$limit, $datareturn, 90 );
                return $datareturn;
            }
            else {
                return response()->json([
                    'success' => false,
                    'message' => 'GetCategoryPrice cannot be found'
                ], 400);
            }

        }
    }



}
