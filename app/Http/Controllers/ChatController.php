<?php

namespace App\Http\Controllers;

use App\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function ShowChatRoom()
    {
        $GetMoel = new Chat();

        $data = $GetMoel->getChatRoom();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'chat user cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowChatDetail()
    {
        $GetMoel = new Chat();

        $data = $GetMoel->getChatDetail();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'chat user cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowChatRoomId(Request $request)
    {
        $GetMoel = new Chat();

        $data = $GetMoel->getChatRoomId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'chat user cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowChatDetailId(Request $request)
    {
        $GetMoel = new Chat();

        $data = $GetMoel->getChatDetailId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'chat user cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $chat)
    {
        //
    }
}
