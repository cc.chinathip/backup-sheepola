<?php

namespace App\Http\Controllers;

use App\AdminBank;
use Illuminate\Http\Request;

class AdminBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function ShowAdminBank()
    {
        $GetMoel = new AdminBank();

        $data = $GetMoel->getAdminBank();

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'admin bank cannot be found'
            ], 400);
        }

        return $data;
    }

    public function ShowAdminBankId(Request $request)
    {
        $GetMoel = new AdminBank();

        $data = $GetMoel->getAdminBankId($request->route('id'));

        if (!$data) {
            return response()->json([
                'success' => false,
                'message' => 'admin bank cannot be found'
            ], 400);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminBank  $adminBank
     * @return \Illuminate\Http\Response
     */
    public function show(AdminBank $adminBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminBank  $adminBank
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminBank $adminBank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminBank  $adminBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminBank $adminBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminBank  $adminBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminBank $adminBank)
    {
        //
    }
}
