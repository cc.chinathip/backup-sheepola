<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Route; // call Route

class Promotion extends Model
{
    //
	public function getPromotion(){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'promotionall'){

			$datas =  DB::table('promotion')->get();
			return $datas;

		}

		return $datas;
	}
	public function getPromotionSlide(){

		$resp[0] = array(
			'link' => env('SHEEP_URL').'coupon',
			'image' => env('CDN_URL').'static/imgs/promotion/banner-promo/giftpromo/040.jpg'
		);

		$resp[1] = array(
			'link' => env('SHEEP_URL').'product/clearance/',
			'image' => env('CDN_URL').'static/imgs/promotion/banner-promo/giftpromo/050.jpg'
		);

		$resp[2] = array(
			'link' => 'https://goo.gl/piRW6W',
			'image' => env('CDN_URL').'static/imgs/promotion/banner-promo/giftpromo/030.jpg'
		);

		$resp[3] = array(
			'link' => 'https://goo.gl/TzskRp',
			'image' => env('CDN_URL').'static/imgs/promotion/banner-promo/giftpromo/010.jpg'
		);

		$resp[4] = array(
			'link' => 'https://goo.gl/yg521D',
			'image' => env('CDN_URL').'static/imgs/promotion/banner-promo/giftpromo/020.jpg'
		);


		return json_encode($resp);
	}

	public function getPromotionNotification(){

		$RouteName = Route::currentRouteName();

		if($RouteName == 'ShowPromotionNotification1'){

			$datas =  DB::table('promotion')->where('notification' ,'1')->where('status' ,'1')->get();

		}else if($RouteName == 'ShowPromotionNotification2'){

			$datas =  DB::table('promotion')->where('notification' ,'1')->where('type_notification' ,'1')->where('status' ,'1')->get();

		}else if($RouteName == 'ShowPromotionNotification3'){

			$datas =  DB::table('promotion')->where('notification' ,'1')->where('type_notification' ,'2')->where('status' ,'1')->get();

		}else if($RouteName == 'ShowPromotionNotification4'){

			$datas =  DB::table('promotion')->where('status' ,'1')->get();

		}else if($RouteName == 'UseWithNotification'){

			$datas =   DB::select(DB::raw('SELECT gender,age_from,age_to,id FROM `promotion` WHERE `notification` = \'1\' ORDER BY id DESC'));

		}

		return $datas;
	}

	public function getPromotionid($id){

		$datas =  DB::table('promotion')->where('id' ,$id)->get();

		return $datas;
	}

	public function getPromotionCateStatus($status){

		$datas =  DB::table('promotioncate')->where('status' ,$status)->get();

		return $datas;
	}

	public function getPromotionImageFull(){

		$datas =  DB::table('promotion')->select('ImageFull','htmledit')->where('id' ,'99')->get();

		return $datas;
	}



	public function getPromotionEnd($status,$status_admin,$ProductType,$dateEnd){

		$datas =  DB::table('promotion')->where('PromotionEnd' ,'>=' ,$dateEnd)->where('status' ,$status)->where('status_admin' ,$status_admin)
		->where('ProductType' , $ProductType)->where('id' , '<>' ,'114')->where('id' , '<>' ,'90')->orderBy('id', 'desc')->get();

		
		return $datas;
	}

	public function getPromotionCreateEndCat($dateCreate , $dateEnd , $PromotionCat){

		
		$datas =  DB::table('promotion')->where('PromotionCreate' ,'<=' ,$dateCreate)->where('PromotionEnd' ,'>=' ,$dateEnd)->where('status' ,'1')->where('status_admin' ,'1')
		->where('PromotionCat' , $PromotionCat)->orderBy('id', 'desc')->get();


		
		return $datas;
	}


	/*function getPromotionCodeId($dateAdd,$dateEnd,$id){

		
		$datas =  DB::table('promocode')->where('status' ,'1')->where('date_add' ,'<=' ,$dateAdd)->where('date_end' ,'>=' ,$dateEnd)->where('user_id' ,'00')
			->where('status' , '1')->where('template', '1')->where('category', '1')->where('category', '1')->where('id', $id)->get();

		return $datas;
	}*/

	public function getImgProductTypeLinkApp(){

		/*$main_product['promotion_last'] = $this->db->raw('SELECT id,ImageMeduim,ProductType,linkApp FROM `promotion` WHERE `status` = \'1\' AND `status_admin` = \'1\' AND `id` != \'95\' AND `PromotionEnd` <= \''.$date_time.'\'  ORDER BY id DESC');*/

		$datetime = date('Y-m-d H:i:s',strtotime("+16 day"));
		$datas =   DB::select(DB::raw('SELECT id,ImageMeduim,ProductType,linkApp FROM promotion WHERE `status` = 1 AND `status_admin` = 1 AND `id` != 95 AND `PromotionEnd` 
			<= '.$datetime.'  ORDER BY id DESC'));

		return $datas;

	}


	
	public function getCountOrderBuyPromotion($promoid,$pid='',$qtysale=''){
		
		 $datas = DB::table('order_detail')->select(DB::raw('count(order_detail.id) as total'))->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->where('order_detail.promotion_id', $promoid)->where('order_detail.pid', $pid)->where('order_product.status', '<>' ,'7')->first();

		if(intval($datas->total) < $qtysale) {

			return $datas->total;

		}else{
			return false;
		}



	}

	public function getCountOrderBuyPromotionPercen($promoid,$pid='',$qtysale=''){
		
		$datas = DB::table('order_detail')->select(DB::raw('count(order_detail.id) as total'))->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->where('order_detail.promotion_id', $promoid)->where('order_detail.pid', $pid)->where('order_product.status', '<>' ,'7')->first();

		return (intval($datas->total) * 100) / intval($qtysale);

	}

	public function getCheckPromotion($promoid,$id=''){
		$promotion = DB::table('promotion')->where('PromtionId', $promoid)->first();
		if(!empty($promotion)) {
			if($promotion->status == '1' && $promotion->status_admin == '1'){
				if($promotion->PromotionStart.' '.$promotion->time_start <= date('Y-m-d H:i:s') && $promotion->PromotionEnd.' '.$promotion->time_end >= date('Y-m-d H:i:s')){
					$promotion_detail = DB::table('promotion_detail')->where('PromtionId' ,$promoid)->where('pid' ,$id)->get();
					foreach ($promotion_detail as $promotion_detail_value) {
						
						if($promotion_detail_value->show_hidden == '1'){

							if($promotion_detail_value->qty_limit == 0){
								return 'false1';
							} else {
								//$datacount =   DB::select(DB::raw('SELECT COUNT(order_detail.id) as total FROM `order_detail` as od JOIN `order_product` as op ON op.oid = od.oid WHERE od.pid = \''.$id.'\' AND op.status != "7" '));

									$datacount = DB::table('order_detail')->select(DB::raw('COUNT(order_detail.id) as total'))->join('order_product', 'order_product.oid', '=', 'order_detail.oid')->where('order_detail.promotion_id', $promoid)->where('order_detail.pid', $id)->where('order_product.status', '!=', '7')->get();

									foreach ($datacount as $datacount_value) {
										if($promotion_detail_value->show_hidden == '1'){
											if (intval($datacount_value->total) < $promotion_detail_value->qty_limit) {
												return $promotion_detail_value;
											} else {
												return 'pro_mod';
											}
									}
								
								}
							}

						} else {
							return 'false2';
						}
					}
						//return 'df22';
				} else {
					return 'false3';
				}
				//return 'df';
			} else {
				return 'false4';
			}
		} else {
			return 'false5';
		}
	}


	public function getPromotionFlashdeal(){

		$date = date('Y-m-d');
		$time = date('H:i:s');


		$main_product['promotion'] =   DB::select(DB::raw('SELECT * FROM promotion WHERE `ProductType` = "3" AND `status` = "1" AND `status_admin` = "1" 
			AND `PromotionStart` <= "'.$date.'" AND `time_start` <= "'.$time.'" AND `PromotionEnd` >= "'.$date.'" AND `time_end` >= "'.$time.'" '));

			//return $main_product['promotion'];
		if ($main_product['promotion'][0]->id == '') {

			$main_product['promotion'] = false;
			
			$main_product['flashdeal'] =  DB::table('product')->where('show_hidden' ,'1')->where('flashdeal' ,'1')->groupBy('sku_id','sku_item')->orderBy('id', 'desc')->limit('10')->get();

			return 'dgg';
			
		}else{
				//return 'dgg222';

			foreach ($main_product['promotion'] as $main_product_key => $main_product_value) {

				$main_product['flashdeal'] =  DB::table('product')->where('promotion_id' ,$main_product_value->PromtionId)->where('show_hidden' ,'1')
				->orderBy('id', 'desc')->limit('12')->get();


				foreach ($main_product['flashdeal'] as $vp) { 
					if($vp->promotion_id != '0'){
							//$promotion_price = $this->getCheckPromotion($vp->promotion_id,$vp->id);

						$promotion_price = array();
						$promotion_price = $this->getCheckPromotion($vp->promotion_id,$vp->id);
							//return $promotion_price;

								//return 'dddgdgg';
						if(is_array($promotion_price) && count($promotion_price)>0){

							foreach ($promotion_price as $promotion_price_value) { 
										//return 'sfsf';

								$vp->nat_price = $promotion_price_value->pr_price;

								$vp->qty_sale = $vp->qty_sale + $promotion_price_value->qty_limit - $promotion_price_value->qty_sale;

								if($vp->qty_sale <= 0){
									$vp->qty_sale = 0;
								}

								$count_order = $this->getCountOrderBuyPromotion($vp->promotion_id,$promotion_price_value->qty_limit,$vp->id);

								if (!$count_order) {
									$vp->pro_mod = false;
								}else{
									$vp->pro_mod = true;
								}


								if($promotion_price_value->product_image != ''){
									$vp->imageshow_cover = $promotion_price_value->product_image;
								}
								if(strip_tags($promotion_price_value->detail_product_mobile) != ''){
									$vp->details = $promotion_price_value->detail_product_mobile;
								}
								if(strip_tags($promotion_price_value->detail_product) != ''){
									$vp->details_web = $promotion_price_value->detail_product;
								}
								if($promotion_price_value->cashback != '-1'){
									$vp->cashback_percent = $promotion_price_value->cashback;
								}
								if($promotion_price_value->cashback_aff != '-1'){
									$vp->aff_percent = $promotion_price_value->cashback_aff;
								}
							}
						}


					}else{
						$promotion_price = false;
					}
				}
			}
			
		}

		return  json_encode($main_product);
		
	}


	public function getPromotionFlash(){

        $date = date('Y-m-d');
        $time = date('H:i:s');

        $datas =  DB::table('promotion')->where('ProductType','3')->where('status','1')->where('status_admin','1')->where('PromotionStart','<=', $date)->where('time_start','<=', $time)->where('PromotionEnd','>=', $date)->where('time_end','>=', $time)->orderBy('id','DESC')->get();

        return $datas;

    }


    public function getPromotionFlashStartEndByOne($date,$time){

    
        $datas =  DB::table('promotion')->where('ProductType','3')->where('status','1')->where('status_admin','1')->where('PromotionStart','<=', $date)->where('time_start','<=', $time)->where('PromotionEnd','>=', $date)->where('time_end','>=', $time)->orderBy('id','DESC')->first();

        return json_encode($datas);

    }

    public function getPromotionFlashEnd($date,$time){

    
        $datas =  DB::table('promotion')->where('ProductType','3')->where('status','1')->where('status_admin','1')->where('PromotionEnd','>=', $date)->where('time_end','>=', $time)->orderBy('id','DESC')->get();

        return $datas;

    }


	public function getPromotionTemplate(){
		
		//$data = DB::table('promocode_template')->select('bgcolor','bg','id')->where('id',$id)->get();
		$data = DB::table('promocode_template')->select('bgcolor','bg')->get();
		return $data;
	}

	public function getPromotionCodeDate($datenow){

		$PromotionTemplate = $this->getPromotionTemplate();

		$GetCategorysModel = new Category();

		$CategorysModel = $GetCategorysModel->getCategorys();

	    foreach($PromotionTemplate as $PromotionTemplate_key => $PromotionTemplate_value){

	    	foreach($CategorysModel as $CategorysModel_key => $CategorysModel_value){

	    		$datas =  DB::table('promocode')->where('status', '1')->where('date_add', '<=' ,$datenow)->where('date_end', '>=' ,$datenow)->where('user_id','0')
	    	->where('template', $PromotionTemplate_key)->where('category_product',$CategorysModel_key)->get();

		    	foreach($datas as  $datas_key => $datas_values) {

		    		$datasss[] = array( 

		    			'Promocode_detail' => $datas_values,
		    			'Template_detail' => $PromotionTemplate_value,
		    			'Categorys_detail' => $CategorysModel_value

		    		); 
		    	}

	    	}
	    	
	    }

	    return $datasss;

	}



	public function getCoupon($limit){


		//$CategorysModel = $GetCategorysModel->getCouponCateActive($id);
		$datas =  DB::table('promocode')->where('status_admin','1')->where('status','1')->where('date_add','<=',date('Y-m-d'))->where('date_end','>=',date('Y-m-d'))->where('used','0')->where('show_in_coupon','1')->inRandomOrder()
			->limit($limit)->get();

		return  $datas;

		
	

}
		


	




}
